package Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class AllLikeModel implements Parcelable {
    public int user_id;
    public String name;
    public String mutual;
    public String thumb;


    @Override
    public int describeContents() {
        return 0;
    }

    public AllLikeModel(JSONObject jobj) throws JSONException {
        user_id = jobj.getInt("user_id");
        name = jobj.getString("name");
        int m = jobj.getInt("mutual");
        mutual = m + " mutual friend"+(m>1?"":"s");
        thumb = jobj.getString("user_image_thumb");
    }

    public AllLikeModel(Parcel in) {

    }


    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Creator CREATOR = new Creator() {
        public AllLikeModel createFromParcel(Parcel in) {
            return new AllLikeModel(in);
        }

        public AllLikeModel[] newArray(int size) {
            return new AllLikeModel[size];
        }
    };



}
