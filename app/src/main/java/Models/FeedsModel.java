package Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FeedsModel implements Parcelable {
    public int _id;
    public int user_id;
    public int owner_id=0;
    public String content;
    public String added_on;
    public ArrayList<MediaModel> media;
    public int like_counts;
    public int comment_counts;
    public String owner_display_name;
    public String user_display_name;
    public String user_image_thumb;
    public String user_image_main;
    public String owner_image_main;
    public String owner_image_thumb;
    public int allow_delete;
    public int isShared;
    public int self_liked;
    public ArrayList<FeedLikeModel> likes;
    public ArrayList<FeedCommentModel> comments;
    public String tags;


    @Override
    public int describeContents() {
        return 0;
    }

    public FeedsModel(JSONObject jobj) throws JSONException {
        _id = jobj.getInt("_id");
        user_id = jobj.getInt("user_id");
        content = jobj.getString("content");
        added_on = jobj.getString("added_on");
        media = new ArrayList<>();
        JSONArray jsonArray = jobj.getJSONArray("media");
        for(int i = 0; i<jsonArray.length(); i++){
            JSONObject jo = jsonArray.getJSONObject(i);
            media.add(new MediaModel(jo));
        }
        like_counts = jobj.getInt("like_counts");
        comment_counts = jobj.getInt("comment_counts");
        owner_display_name = jobj.getString("owner_display_name");
        user_display_name = jobj.getString("user_display_name");
        user_image_thumb = jobj.getString("user_image_thumb");
        user_image_main = jobj.getString("user_image_main");
        owner_image_main = jobj.getString("owner_image_main");
        owner_image_thumb = jobj.getString("owner_image_thumb");
        allow_delete = jobj.getInt("allow_delete");
        isShared = jobj.getInt("isShared");
        self_liked = jobj.getInt("self_liked");
        JSONArray likes_data = jobj.getJSONArray("post_likes");
        JSONArray comm_data = jobj.getJSONArray("post_comments");
        likes = new ArrayList<>();
        comments = new ArrayList<>();
        for(int i=0; i<likes_data.length();i++){
            likes.add(new FeedLikeModel(likes_data.getJSONObject(i)));
        }
        for(int i=0; i<comm_data.length();i++){
            comments.add(new FeedCommentModel(comm_data.getJSONObject(i)));
        }
        if(isShared==1){
            owner_id=jobj.getInt("owner_id");
        }
        tags=jobj.getString("tags");
    }

    public FeedsModel(Parcel in) {

    }


    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Creator CREATOR = new Creator() {
        public FeedsModel createFromParcel(Parcel in) {
            return new FeedsModel(in);
        }

        public FeedsModel[] newArray(int size) {
            return new FeedsModel[size];
        }
    };



}
