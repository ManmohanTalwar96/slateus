package Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sheetal on 27/01/2017.
 */
public class ScheduleDpModel implements Parcelable {
    public String schedule_id;
    public String schedule_at;
    public MediaModel media;


    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    public ScheduleDpModel(JSONObject jobj) throws JSONException {
        schedule_id= jobj.getString("schedule_id");
        schedule_at= jobj.getString("schedule_at");
        media= new MediaModel(jobj.getJSONObject("media"));
    }



    public ScheduleDpModel(Parcel in) {
        this.schedule_id= in.readString();
        this.schedule_at= in.readString();
        //this.image=in.readString();
    }


    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(schedule_id);
        dest.writeString(schedule_at);
        //dest.writeString(image);
    }

    public static final Creator CREATOR = new Creator() {
        public ScheduleDpModel createFromParcel(Parcel in) {
            return new ScheduleDpModel(in);
        }
        public ScheduleDpModel[] newArray(int size) {
            return new ScheduleDpModel[size];
        }
    };


}
