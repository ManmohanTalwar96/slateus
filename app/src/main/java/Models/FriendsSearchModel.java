package Models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FriendsSearchModel implements Parcelable {



    public int id;
    public String mutual_count;
    public String display_name;
    public String first_name;
    public String last_name;
    public String friend_status;
    public String user_image_main="";
    public String user_image_thumb="";


    @Override
    public int describeContents() {
        return 0;
    }

    protected FriendsSearchModel(Parcel in) {

        this.id=in.readInt();
        this.display_name=in.readString();
        this.first_name=in.readString();
        this.last_name=in.readString();
        this.friend_status=in.readString();
        this.user_image_main=in.readString();
        this.user_image_thumb=in.readString();
        this.mutual_count = in.readString();

    }

    public FriendsSearchModel (JSONObject jobj) throws JSONException {
        id = jobj.getInt("_id");
        display_name = jobj.getString("display_name");
        first_name = jobj.getString("first_name");
        last_name = jobj.getString("last_name");
        friend_status = jobj.getString("friend_status");
        mutual_count = jobj.getString("mutual_count");

        if(!jobj.getString("user_image_main").isEmpty()){
            user_image_main = jobj.getString("user_image_main");
            user_image_thumb = jobj.getString("user_image_thumb");
        }
    }


    @Override
    public void writeToParcel(Parcel dest, int i) {

        dest.writeInt(id);
        dest.writeString(display_name);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(user_image_main);
        dest.writeString(user_image_thumb);
        dest.writeString(friend_status);
        dest.writeString(mutual_count);

    }

    public static final  Creator<FriendsSearchModel> CREATOR = new Creator<FriendsSearchModel>() {
        @Override
        public FriendsSearchModel createFromParcel(Parcel in) {
            return new FriendsSearchModel(in);
        }

        @Override
        public FriendsSearchModel[] newArray(int size) {
            return new FriendsSearchModel[size];
        }
    };
}
