package Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class NotificationModel implements Parcelable {
    public int _id;
    public int user_id;
    public int ref_user_id;
    public String name;
    public String type;
    public String thumb;
    public String message;
    public String added_on;
    public int post_id=0;


    @Override
    public int describeContents() {
        return 0;
    }

    public NotificationModel(JSONObject jobj) throws JSONException {
        _id = jobj.getInt("_id");
        user_id = jobj.getInt("user_id");
        type = jobj.getString("type");
        if(type.equalsIgnoreCase("friend_request")){
            ref_user_id = jobj.getInt("friend_id");
            message = "has send you friend request";
        }else{
            ref_user_id = jobj.getInt("ref_user_id");
            message = jobj.getString("message");
            if(jobj.getString("post_id").isEmpty()==false)
                post_id = jobj.getInt("post_id");

        }
        name = jobj.getString("name");
        thumb = jobj.getString("user_image_thumb");

        added_on = jobj.getString("added_on");
    }

    public NotificationModel(Parcel in) {

    }


    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Creator CREATOR = new Creator() {
        public NotificationModel createFromParcel(Parcel in) {
            return new NotificationModel(in);
        }

        public NotificationModel[] newArray(int size) {
            return new NotificationModel[size];
        }
    };



}
