package Models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MediaModel implements Parcelable {
    public int _id;
    public String name;
    public String real_name;
    public String type;
    public String mime;
    public int size;
    public int width=0;
    public int height=0;
    public String path;
    public String thumb;
    public String extension;
    public String file_type;


    @Override
    public int describeContents() {
        return 0;
    }

    public MediaModel(JSONObject jobj) throws JSONException {
        _id = jobj.getInt("_id");
        name = jobj.getString("name");
        real_name = jobj.getString("real_name");
        type = jobj.getString("type");
        if(type.equalsIgnoreCase("image")){
            try{
                width = jobj.getInt("width");
                height = jobj.getInt("height");
            }catch (Exception e){
                width = 0;
                height = 0;
            }

        }
        mime = jobj.getString("mime");
        size = jobj.getInt("_id");

        JSONObject imagepath = jobj.getJSONObject("imagepath");
        path = imagepath.getString("path");
        thumb = imagepath.getString("thumb");

        extension = name.substring(name.lastIndexOf("."));
        Log.e("EXTENSION",extension);
        if(extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg") ||
                extension.equalsIgnoreCase(".gif") || extension.equalsIgnoreCase(".png")){
            file_type = "image";
            if(thumb.isEmpty()){
                thumb = path;
            }
        }else if(extension.equalsIgnoreCase(".mp3") || extension.equalsIgnoreCase(".wav")){
            file_type = "audio";
            if(thumb.isEmpty()){
                thumb = "file:///android_asset/audio.jpg";
            }
        }else if(extension.equalsIgnoreCase(".mp4") || extension.equalsIgnoreCase(".avi")||
                extension.equalsIgnoreCase(".3gp") || extension.equalsIgnoreCase(".mov")  ||
                extension.equalsIgnoreCase(".mkv")){
            file_type = "video";
            if(thumb.isEmpty()){
                thumb = path;
            }
        }else{
            file_type = "file";
        }

    }

    public MediaModel(Parcel in) {

    }


    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Creator CREATOR = new Creator() {
        public MediaModel createFromParcel(Parcel in) {
            return new MediaModel(in);
        }

        public MediaModel[] newArray(int size) {
            return new MediaModel[size];
        }
    };



}
