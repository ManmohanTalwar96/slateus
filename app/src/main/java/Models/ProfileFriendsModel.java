package Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileFriendsModel implements Parcelable {


    public int _id;
    public String friend_status;
    public String user_image_main;
    public String user_image_thumb;
    public String display_name;
    public String mutual_count;

    protected ProfileFriendsModel(Parcel in) {

        this._id = in.readInt();
        this.display_name = in.readString();
//        this.friend_status = in.readString();
        this.mutual_count = in.readString();
        this.user_image_main = in.readString();
        this.user_image_thumb = in.readString();
    }

    public ProfileFriendsModel(JSONObject jobj) throws JSONException{

        _id = jobj.getInt("_id");
        display_name = jobj.getString("display_name");
        friend_status = jobj.getString("friend_status");
        user_image_thumb = jobj.getString("user_image_thumb");
        user_image_main = jobj.getString("user_image_main");
        mutual_count = jobj.getString("mutual_count");

    }


    public static final Creator<ProfileFriendsModel> CREATOR = new Creator<ProfileFriendsModel>() {
        @Override
        public ProfileFriendsModel createFromParcel(Parcel in) {
            return new ProfileFriendsModel(in);
        }

        @Override
        public ProfileFriendsModel[] newArray(int size) {
            return new ProfileFriendsModel[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(_id);
        dest.writeString(display_name);
        dest.writeString(friend_status);
        dest.writeString(user_image_main);
        dest.writeString(user_image_thumb);
        dest.writeString(mutual_count);
    }


}
