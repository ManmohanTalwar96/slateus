package Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import in.evergreen.socialapp.MyApplication;

public class FeedCommentModel implements Parcelable {
    public int _id;
    public int user_id;
    public String name;
    public String comment;
    public String added_on;
    public String user_image_thumb;
    public int allow_delete=0;

//    "_id": 3,
//            "user_id": 3,
//            "post_id": 29,
//            "comment": "awesome post",
//            "updated_on": "2018-06-27T09:08:24.136Z",
//            "added_on": "2018-06-27T09:08:24.136Z",
//
//
//            "_id": 2,
//            "comment": "awesome post",
//            "post_id": 19,
//            "added_on": "2018-06-25T12:46:30.000Z",
//            "updated_on": "2018-06-25T12:46:30.000Z",
//            "name": "Sheetal Kumar",
//            "user_image_main": "http://api.slateus.com/uploads/main/65b7781f83afd9a35c04951710f135678e08feab.jpeg",
//            "user_image_thumb": "http://api.slateus.com/uploads/thumb/65b7781f83afd9a35c04951710f135678e08feab.jpeg"

    @Override
    public int describeContents() {
        return 0;
    }

    public FeedCommentModel(JSONObject jobj) throws JSONException {
        _id = jobj.getInt("_id");
        if(jobj.has("user_id")){
            user_id = jobj.getInt("user_id");
        }else{
            user_id = MyApplication._id;
        }
        if(jobj.has("name")){
            name = jobj.getString("name");
        }else{
            name = MyApplication.display_name;
        }
        if(jobj.has("user_image_thumb")){
            user_image_thumb = jobj.getString("user_image_thumb");
        }else{
            user_image_thumb = MyApplication.image;
        }
        comment = jobj.getString("comment");
        added_on = jobj.getString("added_on");
        if(jobj.has("allow_delete")){
            allow_delete = jobj.getInt("allow_delete");
        }

    }

    public FeedCommentModel(Parcel in) {

    }


    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Creator CREATOR = new Creator() {
        public FeedCommentModel createFromParcel(Parcel in) {
            return new FeedCommentModel(in);
        }

        public FeedCommentModel[] newArray(int size) {
            return new FeedCommentModel[size];
        }
    };



}
