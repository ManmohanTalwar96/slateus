package Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class StateModel implements Parcelable {

    public String name;
    public int state_id;

    protected StateModel(Parcel in) {

        this.state_id = in.readInt();
        this.name = in.readString();

    }

    public StateModel (JSONObject jsonObject) throws JSONException{

        name = jsonObject.getString("name");
        state_id = jsonObject.getInt("_id");

    }

    public static final Creator<StateModel> CREATOR = new Creator<StateModel>() {
        @Override
        public StateModel createFromParcel(Parcel in) {
            return new StateModel(in);
        }

        @Override
        public StateModel[] newArray(int size) {
            return new StateModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

            dest.writeString(name);
            dest.writeInt(state_id);
    }
}
