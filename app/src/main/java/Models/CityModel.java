package Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class CityModel implements Parcelable {

    public String city_name;
    public int state_id;
    public int city_id;

    protected CityModel(Parcel in) {

        this.city_id = in.readInt();
        this.state_id = in.readInt();
        this.city_name = in.readString();
    }

    public CityModel(JSONObject jsonObject) throws JSONException{

        city_name = jsonObject.getString("name");
        city_id = jsonObject.getInt("_id");
        state_id = jsonObject.getInt("state_id");

    }

    public static final Creator<CityModel> CREATOR = new Creator<CityModel>() {
        @Override
        public CityModel createFromParcel(Parcel in) {
            return new CityModel(in);
        }

        @Override
        public CityModel[] newArray(int size) {
            return new CityModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(city_id);
        dest.writeInt(state_id);
        dest.writeString(city_name);
    }
}
