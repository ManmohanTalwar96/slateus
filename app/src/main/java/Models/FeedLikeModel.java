package Models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class FeedLikeModel implements Parcelable {
    public int user_id;
    public String name;


    @Override
    public int describeContents() {
        return 0;
    }

    public FeedLikeModel(JSONObject jobj) throws JSONException {
        user_id = jobj.getInt("user_id");
        name = jobj.getString("name");
    }

    public FeedLikeModel(Parcel in) {

    }


    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Creator CREATOR = new Creator() {
        public FeedLikeModel createFromParcel(Parcel in) {
            return new FeedLikeModel(in);
        }

        public FeedLikeModel[] newArray(int size) {
            return new FeedLikeModel[size];
        }
    };



}
