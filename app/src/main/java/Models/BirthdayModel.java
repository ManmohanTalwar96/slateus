package Models;

import android.os.Parcel;
import android.os.Parcelable;

public class BirthdayModel implements Parcelable {

    protected BirthdayModel(Parcel in) {
    }

    public static final Creator<BirthdayModel> CREATOR = new Creator<BirthdayModel>() {
        @Override
        public BirthdayModel createFromParcel(Parcel in) {
            return new BirthdayModel(in);
        }

        @Override
        public BirthdayModel[] newArray(int size) {
            return new BirthdayModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
