package in.evergreen.socialapp;

import android.Manifest;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ads.AdsMediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.tokenautocomplete.FilteredArrayAdapter;
import com.tokenautocomplete.TokenCompleteTextView;
import com.yanzhenjie.album.Action;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumConfig;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.AlbumLoader;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Apis.ApiClient;
import Apis.AuthInterface;
import Apis.PostInterface;
import Apis.ProgressRequestBody;
import Apis.UserInterface;
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import in.evergreen.socialapp.Adapters.AddPostAdapter;
import in.evergreen.socialapp.Auth.LoginActivity;
import in.evergreen.socialapp.Utility.FriendsCompletionView;
import in.evergreen.socialapp.Utility.ImageViewer;
import in.evergreen.socialapp.Utility.Person;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;

import static in.evergreen.socialapp.R.drawable.inage_placeholder;

public class AddPostActivity extends AppCompatActivity implements TokenCompleteTextView.TokenListener<Person>{

    View footerMenu;
    TLHelper hlp;
    ArrayList<AlbumFile> checkedVideo=new ArrayList<>();
    ArrayList<AlbumFile> checkedImage=new ArrayList<>();
    ImageViewer iviewer;
    PlayerView playerView;
    ExoPlayer player;
    boolean playWhenReady = false;
    private  DataSource.Factory manifestDataSourceFactory;
    private  DataSource.Factory mediaDataSourceFactory;
    boolean menuOpen = false;
    EditText txtContent;
    View progressWrapper;
    ProgressBar progressBar;
    TextView progressBarValue;
    String audioPath = "";
    String audioPathUpload = "";
    PostInterface postInterface;
    UserInterface userInterface;
    TLStorage sto;
    ArrayList<uploadStatus> uploadStatuses=new ArrayList<>();
    boolean isPosting = false;
    boolean attached_media_view = false;
    private RecyclerView mRecyclerView;
    private AddPostAdapter adapter;
    ArrayList<MediaPath> paths = new ArrayList<>();

    ArrayList<String> sb=new ArrayList<>();
    FriendsCompletionView completionView;
    ArrayList<Person> people;
    ArrayAdapter<Person> personadapter;
    String tag_user_id="";



    public class uploadStatus{
        public int serial=0;
        public int progress=0;
        public boolean uploadcomplete = false;
        public boolean uploadsuccess = true;
        public int id=0;
        public uploadStatus(int index){
            this.serial = index;
        }
    }

    public class MediaPath{
        public int index = 0;
        public String type = "";
        public String path="";
        public String path_thumb="";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        Bundle b= getIntent().getExtras();
        hlp = new TLHelper(this);
        ViewGroup vg = (ViewGroup) findViewById(R.id.imageContainer);
        iviewer = new ImageViewer(this,vg);
        getSupportActionBar().setElevation(0);
        footerMenu = findViewById(R.id.footerMenu);
        footerMenu.setTranslationY(hlp.pxFromDp(150));
        txtContent = (EditText) findViewById(R.id.txtContent);
        progressWrapper = findViewById(R.id.progressWrapper);
        progressWrapper.setVisibility(View.GONE);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBarValue = (TextView) findViewById(R.id.txtProgressValue);
        Album.initialize(AlbumConfig.newBuilder(this)
                .setAlbumLoader(new MediaLoader())
                .build());
        playerView = findViewById(R.id.video_view);
        mediaDataSourceFactory = new DefaultDataSourceFactory(
                this,
                Util.getUserAgent(this, getString(R.string.app_name)),
                new DefaultBandwidthMeter());
        manifestDataSourceFactory =
                new DefaultDataSourceFactory(
                        this, Util.getUserAgent(this, this.getString(R.string.app_name)));
        FrameLayout touchInterceptor = (FrameLayout)findViewById(R.id.frame);
        touchInterceptor.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (txtContent.isFocused()) {
                        Rect outRect = new Rect();
                        txtContent.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                            txtContent.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                    Rect outRect = new Rect();
                    findViewById(R.id.footerMenuButton).getGlobalVisibleRect(outRect);
                    if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                        if(menuOpen){
                            openBottomMenu(v);
                        }
                    }

                }
                return false;
            }
        });
        postInterface= ApiClient.getClient().create(PostInterface.class);
        userInterface= ApiClient.getClient().create(UserInterface.class);
        sto=new TLStorage(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AddPostAdapter(this, paths);
        mRecyclerView.setAdapter(adapter);
        if(b!=null && b.getString("tag_user_id")!=null){
            tag_user_id=b.getString("tag_user_id");
        }
        showUserDetail();
        initACFriendList();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_post_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.itemSave) {
            if(isPosting==false)
                uploadFiles();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public void openBottomMenu(View v){
        View myYellowView = findViewById(R.id.footerMenu);
        if(menuOpen == false){
            createBottomUpAnimation(myYellowView, null, myYellowView.getHeight()).start();
            menuOpen =true;
        }else{
            createTopDownAnimation(myYellowView, null, myYellowView.getHeight()).start();
            menuOpen =false;
        }

    }

    private static ObjectAnimator createBottomUpAnimation(View view,
                                                          AnimatorListenerAdapter listener, float distance) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "translationY", 0);
//        animator.setDuration(???)
        animator.removeAllListeners();
        if (listener != null) {
            animator.addListener(listener);
        }
        return animator;
    }

    public static ObjectAnimator createTopDownAnimation(View view, AnimatorListenerAdapter listener,
                                                        float distance) {
        //view.setTranslationY(-distance);
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "translationY", distance);
        animator.removeAllListeners();
        if (listener != null) {
            animator.addListener(listener);
        }
        return animator;
    }

    public void addPhotos(View v){
        if(menuOpen){
            openBottomMenu(v);
        }
        Album.image(this) // Image and video mix options.
                .multipleChoice() // Multi-Mode, Single-Mode: singleChoice().
                .columnCount(2) // The number of columns in the page list.
                .selectCount(10)  // Choose up to a few images.
                .camera(true) // Whether the camera appears in the Item.
                .checkedList(checkedImage)
                //.filterSize() // Filter the file size.
                //.filterMimeType() // Filter file format.
                //.filterDuration() // Filter video duration.
                //.afterFilterVisibility() // Show the filtered files, but they are not available.
                .onResult(new Action<ArrayList<AlbumFile>>() {
                    @Override
                    public void onAction(@NonNull ArrayList<AlbumFile> result) {
                        checkedImage = result;
                        Log.e("IMAGE GOT","SIZE:"+result.size());
                        displayThumbnail();
                    }
                })
                .onCancel(new Action<String>() {
                    @Override
                    public void onAction(@NonNull String result) {
                        // The user canceled the operation.
                    }
                })
                .start();
    }

    public void displayThumbnail(){
        paths.clear();
        for(int i=0; i<checkedImage.size(); i++){
            MediaPath mp=new MediaPath();
            mp.index = i;
            mp.type = "image";
            mp.path = checkedImage.get(i).getPath();
            mp.path_thumb = checkedImage.get(i).getThumbPath();
            paths.add(mp);
        }
        for(int i=0; i<checkedVideo.size(); i++){
            MediaPath mp=new MediaPath();
            mp.index = i;
            mp.type = "video";
            mp.path = checkedVideo.get(i).getPath();
            mp.path_thumb = checkedVideo.get(i).getThumbPath();
            paths.add(mp);
        }
        if(audioPathUpload.isEmpty()==false){
            MediaPath mp=new MediaPath();
            mp.index = 0;
            mp.type = "audio";
            mp.path = audioPathUpload;
            mp.path_thumb = "file:///android_asset/mic.png";
            paths.add(mp);
        }
        iviewer.showImages(paths);
    }

    public class MediaLoader implements AlbumLoader {

        @Override
        public void load(ImageView imageView, AlbumFile albumFile) {
            load(imageView, albumFile.getPath());
        }

        @Override
        public void load(ImageView imageView, String url) {
            Glide.with(imageView.getContext())
                    .load(url)
                    //.error(R.drawable.inage_placeholder)
                    //.placeholder(inage_placeholder)
                    //.crossFade()
                    .into(imageView);
        }
    }

    public void openRecorder(View v){
        if(menuOpen){
            openBottomMenu(v);
        }

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.RECORD_AUDIO)
                .withListener(new PermissionListener() {
                    @Override public void onPermissionGranted(PermissionGrantedResponse response) {
                        audioPath = Environment.getExternalStorageDirectory() + "/"+"audio" +".wav";
                        int color = getResources().getColor(R.color.colorPrimaryDark);
                        int requestCode = 0;
                        AndroidAudioRecorder rec = AndroidAudioRecorder.with(AddPostActivity.this)
                                // Required
                                .setFilePath(audioPath)
                                .setColor(color)
                                .setRequestCode(requestCode)

                                // Optional
                                .setSource(AudioSource.MIC)
                                .setChannel(AudioChannel.STEREO)
                                .setSampleRate(AudioSampleRate.HZ_48000)
                                .setAutoStart(true)
                                .setKeepDisplayOn(true)

                                ;
                        Log.e("AUSIO RECORD","RECORD");

                        rec.record();
                    }
                    @Override public void onPermissionDenied(PermissionDeniedResponse response) {/* ... */}
                    @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
                }).check();


    }

    public void addVideo(View v){
        if(menuOpen){
            openBottomMenu(v);
        }
        Album.video(this) // Video selection.
                .multipleChoice()
                .camera(true)
                .columnCount(2)
                .selectCount(1)
                .checkedList(checkedVideo)
//                .filterSize()
//                .filterMimeType()
//                .filterDuration()
//                .afterFilterVisibility() // Show the filtered files, but they are not available.
                .onResult(new Action<ArrayList<AlbumFile>>() {
                    @Override
                    public void onAction(@NonNull ArrayList<AlbumFile> result) {
                        checkedVideo = result;
                        displayThumbnail();
                    }
                })
                .onCancel(new Action<String>() {
                    @Override
                    public void onAction(@NonNull String result) {
                    }
                })
                .start();
    }


    public void showAttachedMedia(View v){
        findViewById(R.id.contentView).setVisibility(View.VISIBLE);
        attached_media_view=true;
        adapter.notifyDataSetChanged();
    }




    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            //releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    private void releasePlayer() {
        if (player != null) {
//            playbackPosition = player.getCurrentPosition();
//            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
            player.release();
            player = null;
        }
    }

    @Override
    public void onBackPressed() {
        if(menuOpen==true){
            openBottomMenu(new View(this));
            return;

        }
        if(attached_media_view==true){
            findViewById(R.id.contentView).setVisibility(View.GONE);
            adapter.notifyDataSetChanged();
            attached_media_view =false;
            adapter.releasePlayer();
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                audioPathUpload = audioPath;
                //createMediaView(audioPath);
                displayThumbnail();
            } else if (resultCode == RESULT_CANCELED) {
                // Oops! User has canceled the recording
            }
        }
    }

    public void uploadFiles(){
        isPosting = true;
        uploadStatuses.clear();
        if(checkedImage.size()>0 || checkedVideo.size()>0 || audioPathUpload.isEmpty()==false){
            progressWrapper.setVisibility(View.VISIBLE);
            updateProgressBar();
            int index = 0;
            for(int i= 0; i<checkedImage.size();i++){
                uploadFile(new File(checkedImage.get(i).getPath()),index,true);
                index++;
            }
            for(int i= 0; i<checkedVideo.size();i++){
                uploadFile(new File(checkedVideo.get(i).getPath()),index,false);
                index++;
            }
            if(audioPathUpload.isEmpty()==false){
                uploadFile(new File(audioPathUpload),index,false);
                index++;
            }
        }else{
            createPost();
        }


    }

    public void uploadFile(File file, final int index, boolean isImage){
        uploadStatuses.add(new uploadStatus(index));
        ProgressRequestBody fileBody = new ProgressRequestBody(file, new ProgressRequestBody.UploadCallbacks() {
            @Override
            public void onProgressUpdate(int percentage) {
                Log.e("PROGRESS","INDEX-"+index+"--"+percentage);
                uploadStatuses.get(index).progress = percentage;
                updateProgressBar();
            }

            @Override
            public void onError() {

            }

            @Override
            public void onFinish() {

            }
        });
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(((isImage)?"image":"file"), file.getName(), fileBody);
        Call<JsonElement> call;
        if(isImage){
            call = postInterface.uploadImage(sto.getValueString("token"),filePart);
        }else{
            call = postInterface.uploadFile(sto.getValueString("token"),filePart);
        }

        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement>call, retrofit2.Response<JsonElement> response) {

                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();
                Log.e("RES", "Body: "+body);
                try {
                    JSONObject res = new JSONObject(body);
                    if(res.getString("type").equalsIgnoreCase("success")){
                        JSONObject data = res.getJSONObject("data");
                        uploadStatuses.get(index).uploadcomplete=true;
                        uploadStatuses.get(index).uploadsuccess=true;
                        uploadStatuses.get(index).id=data.getInt("_id");
                        createPost();
                    }else{
                        uploadStatuses.get(index).uploadcomplete=true;
                        uploadStatuses.get(index).uploadsuccess=false;
                        createPost();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                uploadStatuses.get(index).uploadcomplete=true;
                uploadStatuses.get(index).uploadsuccess=false;
                createPost();
            }
        });
    }

    public void updateProgressBar(){
        int value = 0;
        for(int i = 0; i<uploadStatuses.size(); i++){
            value += uploadStatuses.get(i).progress;
        }
        if(uploadStatuses.size()>0){
            value = value/uploadStatuses.size();
        }
        progressBar.setProgress(value);
        progressBarValue.setText(value+"%");

    }

    public void createPost(){
        for(int i=0; i<uploadStatuses.size();i++){
            if(uploadStatuses.get(i).uploadcomplete==false){
                return;
            }
        }
        progressWrapper.setVisibility(View.GONE);
        JSONArray media_ids = new JSONArray();
        JSONArray tag_ids = new JSONArray();
        for(int i=0; i<uploadStatuses.size();i++){
            if(uploadStatuses.get(i).uploadcomplete==true && uploadStatuses.get(i).uploadsuccess==true){
                media_ids.put(uploadStatuses.get(i).id);
            }
        }

        for(int i=0; i<sb.size();i++){
            tag_ids.put(sb.get(i));
        }

        JSONObject jo = new JSONObject();
        try {
            jo.put("content", txtContent.getText().toString());
            jo.put("media", media_ids);
            jo.put("tags", tag_ids);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = (JsonObject) jsonParser.parse(jo.toString());
        Call<JsonElement> call = postInterface.createPost(sto.getValueString("token"),jsonObject);

        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement> call, retrofit2.Response<JsonElement> response) {
                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();
                Log.e("RESPONSE", body);
                try {
                    JSONObject res = new JSONObject(body);
                    if(res.getString("status").equalsIgnoreCase("success")){
                        finishActivity(true);
                    }else{

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.noConnection();
            }
        });
    }

    public void showUserDetail() {
        TextView username = (TextView) findViewById(R.id.txtUserName);
        username.setText(MyApplication.display_name);
        ImageView userimage = (ImageView) findViewById(R.id.userImage);
        TextView txtAvatarText = (TextView) findViewById(R.id.txtNameChar);

        String avatar = MyApplication.image;
        if (avatar.isEmpty() == false) {
            userimage.setVisibility(View.VISIBLE);
            txtAvatarText.setVisibility(View.GONE);
            Glide.with(this).load(sto.getValueString("image")).into(userimage);
        } else {
            userimage.setVisibility(View.GONE);
            txtAvatarText.setVisibility(View.VISIBLE);
            txtAvatarText.setText(hlp.getShortName(sto.getValueString("display_name")));
        }
    }

    public void deleteMedia(MediaPath path){
        if(path.type.equalsIgnoreCase("image")){
            checkedImage.remove(path.index);
        }
        if(path.type.equalsIgnoreCase("video")){
            checkedVideo.remove(path.index);
        }
        if(path.type.equalsIgnoreCase("audio")){
            audioPathUpload = "";
        }
        displayThumbnail();
        adapter.notifyDataSetChanged();
    }

    public void finishActivity(boolean status){
        if(status == true){
            Intent intent=new Intent();
            intent.putExtra("status",status);
            setResult(RESULT_OK,intent);
            finish();
        }
    }



    public void createFriendList(JSONArray jdata) throws JSONException{
        people = new ArrayList<>();
        for(int i=0; i<jdata.length(); i++){
            JSONObject jo =jdata.getJSONObject(i);

            people.add(new Person(hlp.getCapsSentences(jo.getString("display_name")),jo.getString("_id"),jo.getString("image")));

            if(tag_user_id.equalsIgnoreCase(jo.getString("_id"))){
                findViewById(R.id.tagContainer).setVisibility(View.VISIBLE);
                completionView.addObject(people.get(i));
            }
            personadapter = new FilteredArrayAdapter<Person>(this, R.layout.autocomplete_person_layout, people) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    if (convertView == null) {

                        LayoutInflater l = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                        convertView = l.inflate(R.layout.autocomplete_person_layout, parent, false);
                    }

                    Person p = getItem(position);
                    ((TextView)convertView.findViewById(R.id.name)).setText(p.getName());
                    ImageView iv=(ImageView)convertView.findViewById(R.id.imgUser);
                    Glide.with(AddPostActivity.this).load(p.getImage()).into(iv);
                    return convertView;
                }

                @Override
                protected boolean keepObject(Person person, String mask) {
                    mask = mask.toLowerCase();
                    return person.getName().toLowerCase().startsWith(mask);
                }
            };

            completionView = (FriendsCompletionView) findViewById(R.id.searchView);
            completionView.setAdapter(personadapter);
            completionView.setTokenListener(AddPostActivity.this);
            completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);

        }
    }

    public void initACFriendList(){
        Call<JsonElement> call = userInterface.allFriendList(MyApplication.token);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement>call, retrofit2.Response<JsonElement> response) {
                if(response.isSuccessful()){
                    JsonElement jsonElement = response.body();
                    String body = jsonElement.toString();
                    Log.e("RESPONSE", body);
                    try {
                        JSONObject res = new JSONObject(body);
                        JSONArray data = res.getJSONArray("data");
                        createFriendList(data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
            }
        });
    }

    @Override
    public void onTokenAdded(Person token) {
        sb.add(token.getId());
    }

    @Override
    public void onTokenRemoved(Person token) {
        String id=token.getId();
        for(int i=0; i<sb.size(); i++){
            if(sb.get(i).equalsIgnoreCase(id)){
                sb.remove(i);
            }
        }
    }

}
