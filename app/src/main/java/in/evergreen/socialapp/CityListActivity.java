package in.evergreen.socialapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Apis.ApiClient;
import Apis.UserInterface;
import Models.CityModel;
import Models.StateModel;
import in.evergreen.socialapp.Adapters.CityListAdapter;
import in.evergreen.socialapp.Utility.TLHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CityListActivity extends AppCompatActivity {

    private Context context;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private CityListAdapter adapter;
    private TLHelper hlp;
    private UserInterface userInterface;
    private ArrayList<CityModel> items = new ArrayList<>();
    private int state_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_list);

        userInterface = ApiClient.getClient().create(UserInterface.class);

        state_id = getIntent().getIntExtra("state_id",0);

        recyclerView = (RecyclerView) findViewById(R.id.cityRecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CityListAdapter(items,CityListActivity.this);
        recyclerView.setAdapter(adapter);

        if (state_id >= 0) {
            cityList(state_id);
        }
    }

    public void cityList(int state_id){

        Call<JsonElement> call = userInterface.cityList(MyApplication.token, state_id);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();

                try {
                    JSONObject res = new JSONObject(body);

                    if (res.getString("status").equalsIgnoreCase("success")){

                        JSONArray data = res.getJSONArray("data");

                        createListing(data);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });
    }

    public void createListing(JSONArray data) throws JSONException{
        items.clear();
        for (int i=0; i<data.length(); i++) {
            try {
                items.add(new CityModel(data.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        adapter.notifyDataSetChanged();

    }
}
