package in.evergreen.socialapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Apis.ApiClient;
import Apis.AuthInterface;
import Apis.PostInterface;
import Apis.UserInterface;
import Models.FeedsModel;
import Models.MediaModel;

import cn.jzvd.JZMediaManager;
import cn.jzvd.JZUtils;
import cn.jzvd.JZVideoPlayer;
import cn.jzvd.JZVideoPlayerManager;
import in.evergreen.socialapp.Adapters.FeedsHomeAdapter;
import in.evergreen.socialapp.Auth.LoginActivity;
import in.evergreen.socialapp.Utility.EndlessRecyclerOnScrollListener;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppCompatActivity implements  NavigationView.OnNavigationItemSelectedListener {

    UserInterface userService;
    JsonParser jsonParser;
    JsonObject jsonObject;
    TLStorage sto;
    TLHelper hlp;
    ImageView userimage;
    View nav_header;
    TextView badgeView;
    int REQUEST_CREATE_POST = 99;

    private FeedsHomeAdapter adapter;
    public static ArrayList<FeedsModel> itemiList=new ArrayList<FeedsModel>();
    private RecyclerView mRecyclerView;
    View  reloader;
    PostInterface postInterface;
    SwipeRefreshLayout swipeRefreshLayout;
    public int page=1;
    public boolean pageend =false;
    boolean is_loading =false;
    public TextView noRecord;
    EndlessRecyclerOnScrollListener esl;

    String TAG = DashboardActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        sto = new TLStorage(this);
        hlp = new TLHelper(this);
        MyApplication.initUserSession(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        userService = ApiClient.getClient().create(UserInterface.class);
        jsonParser = new JsonParser();
        jsonObject = new JsonObject();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);
        //  View nav_header = LayoutInflater.from(this).inflate(R.layout.nav_header_dashboard, null);
        nav_header = navigationView.getHeaderView(0);
        updateSidebarContent();
        toolbar.setNavigationIcon(R.drawable.ic_menu);



        noRecord = (TextView) findViewById(R.id.txtNoRecord);
        reloader = findViewById(R.id.buttonReload);
        postInterface = ApiClient.getClient().create(PostInterface.class);
        reloader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchRecords();
            }
        });
        esl = new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                Log.e("SCROLL ", pageend+"==="+is_loading);
                if(pageend==false && is_loading == false){
                    page++;
                    fetchRecords();
                }
            }
        };
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.dark_blue, R.color.dark_grey, R.color.light_grey);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                itemiList.clear();
                page=1;
                pageend = false;
                fetchRecords();
                esl.reset();
            }
        });
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FeedsHomeAdapter(this, itemiList);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(View view) {

            }

            @Override
            public void onChildViewDetachedFromWindow(View view) {
                JZVideoPlayer jzvd = view.findViewById(R.id.videoplayer);
                if (jzvd != null && JZUtils.dataSourceObjectsContainsUri(jzvd.dataSourceObjects, JZMediaManager.getCurrentDataSource())) {
                    JZVideoPlayer currentJzvd = JZVideoPlayerManager.getCurrentJzvd();
                    if (currentJzvd != null && currentJzvd.currentScreen != JZVideoPlayer.SCREEN_WINDOW_FULLSCREEN) {
                        JZVideoPlayer.releaseAllVideos();
                    }
                }
            }
        });

        mRecyclerView.addOnScrollListener(esl);





        fetchRecords();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btnAddPost);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createPost(view);
            }
        });

        // TODO: Move this to where you establish a user session
        logUser();

    }

    public void updateSidebarContent() {
        TextView username = (TextView) nav_header.findViewById(R.id.dashboard_txtName);
        username.setText(sto.getValueString("display_name"));
        userimage = (ImageView) nav_header.findViewById(R.id.dashboard_imageView);
        RelativeLayout avatarView = nav_header.findViewById(R.id.dashboard_avatarView);
        TextView txtAvatarText = (TextView) nav_header.findViewById(R.id.dashboard_txtNameChar);
        ImageView editProfile = (ImageView) nav_header.findViewById(R.id.editPtofile);
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DashboardActivity.this, EditProfile.class);
                startActivity(i);
            }
        });

        String avatar = sto.getValueString("image");
        if (avatar.isEmpty() == false) {
            userimage.setVisibility(View.VISIBLE);
            avatarView.setVisibility(View.GONE);
            Glide.with(this).load(sto.getValueString("image")).into(userimage);
        } else {
            userimage.setVisibility(View.GONE);
            avatarView.setVisibility(View.VISIBLE);
            txtAvatarText.setText(hlp.getShortName(sto.getValueString("display_name")));
        }

        Log.e("DISPLAY NAME:","==="+sto.getValueString("display_name"));
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        if (JZVideoPlayer.backPress()) {
            return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
//        MenuItem itemMessages = menu.findItem(R.id.action_notification);
//
//        MenuItem item = menu.findItem(R.id.action_notification);
//        MenuItemCompat.setActionView(item, R.layout.badge_layout);
//        View badgeLayout = (RelativeLayout) itemMessages.getActionView();
//
//        badgeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(DashboardActivity.this, NotificationsActivity.class);
//                startActivity(intent);
//            }
//        });
//        badgeView = badgeLayout.findViewById(R.id.badgeView);
//        updateNotificationCount();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
//        if (id == R.id.action_notification) {
//            return true;
//        }
        if (id == R.id.dashboard_search) {
            Intent i = new Intent(getApplicationContext(), FriendSearchActivity.class);
            startActivity(i);
            return true;
        }
        if (id == R.id.action_notification) {
            Intent i = new Intent(getApplicationContext(), NotificationActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profession) {

        } else if (id == R.id.nav_scheduled) {
//            Intent intent = new Intent(this, SettingsActivity.class);
//            startActivity(intent);
        } else if (id == R.id.nav_schedule_dp) {
            Intent dp = new Intent(DashboardActivity.this, ScheduleDpActivity.class);
            startActivity(dp);
        } else if (id == R.id.nav_birthday) {
            Intent birthday = new Intent(DashboardActivity.this, BirthdayActivity.class);
            startActivity(birthday);
        } else if (id == R.id.nav_tracking) {
//            switchMenuIcon("agenda");
        } else if (id == R.id.nav_call) {
            shareApp();
        } else if (id == R.id.nav_rating) {
            Uri uri = Uri.parse("market://details?id=" + getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
            }
        } else if (id == R.id.nav_aboutus) {
            Intent i=new Intent(DashboardActivity.this,MissionActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_logout) {
            logout();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void logout() {
        final String token = sto.getValueString("token");
//        sto.clearStorageData();

        hlp.showLoader("Logging Out ...");
        Call<JsonElement> call = userService.logout(token);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                hlp.hideLoader();

                if(response.isSuccessful()) {
                    JsonElement jsonElement = response.body();
                    if (jsonElement != null) {


                        String body = jsonElement.toString();
                        Log.e(TAG, "entering try");

                        try {
                            JSONObject res = new JSONObject(body);
                            if (res.getString("status").equalsIgnoreCase("success")) {
                                Log.e(TAG, "Setting value String");



                            } else if (res.getString("status").equalsIgnoreCase("failed")) {
                                //                            ((TextView) findViewById(R.id.reg_errorCommon)).setText(res.getString("message").toString());
                                //                            findViewById(R.id.reg_errorCommon).setVisibility(View.VISIBLE);
                                Toast.makeText(getApplicationContext(), "Error while logging out, Please try after some Time", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                sto.clearStorageData();

                Intent i = new Intent(DashboardActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.hideLoader();
                hlp.noConnection();
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CREATE_POST && resultCode == RESULT_OK){

            page=1;
            pageend = false;
            fetchRecords();
            esl.reset();
        }
    }

    public void createPost(View v){
        Intent intent = new Intent(this, AddPostActivity.class);
        startActivityForResult(intent,REQUEST_CREATE_POST);
    }





    public void fetchRecords(){
        noRecord.setVisibility(View.GONE);
        reloader.setVisibility(View.GONE);
        if(page==1){
            swipeRefreshLayout.setRefreshing(true);
        }

        is_loading = true;
        Call<JsonElement> call = postInterface.getFeed(sto.getValueString("token"),page,20);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                is_loading = false;
                swipeRefreshLayout.setRefreshing(false);
                if(response.isSuccessful()){
                    JsonElement jsonElement = response.body();
                    if(jsonElement!=null){
                        String body = jsonElement.toString();
                        Log.e("RESPONSE", body);
                        try {
                            JSONObject res = new JSONObject(body);
                            JSONArray data = res.getJSONArray("data");
                            createListing(data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }else{
                    hlp.showError(response.code());
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                is_loading=false;
                swipeRefreshLayout.setRefreshing(false);
                if(page==1){
                    reloader.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void createListing(JSONArray data) throws JSONException{
        if(page==1){
            itemiList.clear();
        }
        int last_size =itemiList.size();



        for (int i=0; i<data.length(); i++) {
            JSONObject item;
            try {
                item = data.getJSONObject(i);
                itemiList.add(new FeedsModel(item));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(data.length()==0){
            pageend = true;
        }
        adapter.updateItems(itemiList);
        if(itemiList.size()==0){
            noRecord.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        JZVideoPlayer.releaseAllVideos();
    }

    public static ArrayList<MediaModel> getPostMedia(int post_index){
        if(itemiList.size()>post_index){
            return itemiList.get(post_index).media;
        }else {
            return new ArrayList<MediaModel>();
        }
    }

    public void shareApp(){
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        String link="http://play.google.com/store/apps/details?id=" + appPackageName;
        String textMSG="Make me friend here "+"\n"+link;
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, textMSG);
        sendIntent.putExtra(Intent.EXTRA_TITLE, "Slateus");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(MyApplication._id+"");
        Crashlytics.setUserEmail(MyApplication.email);
        Crashlytics.setUserName(MyApplication.display_name);
    }




}