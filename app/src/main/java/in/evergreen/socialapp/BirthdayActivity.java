package in.evergreen.socialapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import Apis.ApiClient;
import Apis.UserInterface;
import Models.BirthdayModel;
import in.evergreen.socialapp.Adapters.BirthdayAdapter;
import in.evergreen.socialapp.Utility.EndlessRecyclerOnScrollListener;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;

public class BirthdayActivity extends AppCompatActivity {

    private TextView noRecord;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<BirthdayModel> items = new ArrayList<>();
    private BirthdayAdapter adapter;
    private UserInterface userInterface;
    private TLHelper hlp;
    private TLStorage sto;
    private View reloader;
    private int page = 1;
    private boolean pageend = false;
    private boolean is_loading = false;
    private EndlessRecyclerOnScrollListener esl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_birthday);

        noRecord = (TextView) findViewById(R.id.birthday_txtNoRecord);
        reloader = findViewById(R.id.birthday_buttonReload);
        userInterface = ApiClient.getClient().create(UserInterface.class);
        reloader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        hlp = new TLHelper(this);
        sto = new TLStorage(this);

        recyclerView = (RecyclerView) findViewById(R.id.birthday_recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new BirthdayAdapter(items, BirthdayActivity.this);
        recyclerView.setAdapter(adapter);
        esl = new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                if (pageend == false && is_loading == false) {
                    page++;

                }
            }
        };
        recyclerView.addOnScrollListener(esl);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    
}
