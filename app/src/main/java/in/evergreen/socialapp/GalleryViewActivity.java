package in.evergreen.socialapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;

import Models.MediaModel;
import cn.jzvd.JZVideoPlayer;
import cn.jzvd.JZVideoPlayerStandard;
import in.evergreen.socialapp.Fragments.ProfilePostFragment;

public class GalleryViewActivity extends AppCompatActivity {

    ArrayList<MediaModel> filelist = new ArrayList<>();
    int position=0;
    private GalleryPagerAdapter _adapter;
    ViewPager _pager;
    ImageView _closeButton;
    private  DataSource.Factory manifestDataSourceFactory;
    private  DataSource.Factory mediaDataSourceFactory;
    ExoPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_gallery_view);
        Bundle b=getIntent().getExtras();
        String source = b.getString("source");

        mediaDataSourceFactory = new DefaultDataSourceFactory(
                this,
                Util.getUserAgent(this, this.getString(R.string.app_name)),
                new DefaultBandwidthMeter());
        manifestDataSourceFactory =
                new DefaultDataSourceFactory(
                        this, Util.getUserAgent(this, this.getString(R.string.app_name)));


        int post_index=b.getInt("post_index");
        if(source.equalsIgnoreCase("dashboard"))
            filelist = DashboardActivity.getPostMedia(post_index);
        else if(source.equalsIgnoreCase("dpschedule"))
            filelist = ScheduleDpActivity.getPostMedia(post_index);
        else
            filelist = ProfilePostFragment.getPostMedia(post_index);
        position=b.getInt("position");
        position = position-1;
        _pager= (ViewPager) findViewById(R.id.pager);
        _closeButton = (ImageView) findViewById(R.id.btn_close);


        _adapter = new GalleryPagerAdapter(this);
        _pager.setAdapter(_adapter);
        _pager.setOffscreenPageLimit(3); // how many images to load into memory_pager
        _pager.setCurrentItem(position);

        _pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //Log.e("POS", "" + position);
               // setThumbnail(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        _closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Close", "Close clicked");
                finish();
            }
        });


    }



    class GalleryPagerAdapter extends PagerAdapter {

        Context _context;
        LayoutInflater _inflater;


        public GalleryPagerAdapter(Context context) {
            _context = context;
            _inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
//            Log.e("LIST SIZE",filelist.size()+"--");
            return filelist.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int pos) {
            MediaModel model = filelist.get(pos);
            if(model.file_type.equalsIgnoreCase("image")){
                View itemView = _inflater.inflate(R.layout.gallery_image_view, container, false);
                container.addView(itemView);

                final SubsamplingScaleImageView imageView =
                        (SubsamplingScaleImageView) itemView.findViewById(R.id.image);
                final ImageView _loader = (ImageView)  itemView.findViewById(R.id.loadingImage);
                _loader.setVisibility(View.VISIBLE);
                RotateAnimation rotate = new RotateAnimation(
                        0, 360,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f
                );

                rotate.setDuration(900);
                rotate.setRepeatCount(Animation.INFINITE);
                _loader.startAnimation(rotate);
                Glide.with(getApplicationContext())
                        .asBitmap()
                        .load(model.path)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                                _loader.setVisibility(View.GONE);
                                int w = bitmap.getWidth();
                                int h = bitmap.getHeight();
                                imageView.setImage(ImageSource.bitmap(bitmap));

                            }
                        });
                return itemView;
            }else if(model.file_type.equalsIgnoreCase("video")){
                View itemView = _inflater.inflate(R.layout.gallery_video_view, container, false);
                container.addView(itemView);
                final JZVideoPlayerStandard jzVideoPlayerStandard = (JZVideoPlayerStandard)itemView.findViewById(R.id.videoplayer);
                jzVideoPlayerStandard.setUp(model.path,JZVideoPlayerStandard.SCREEN_WINDOW_LIST,"");
                Glide.with(GalleryViewActivity.this).load(model.thumb).into(jzVideoPlayerStandard.thumbImageView);
                return itemView;
            }else if(model.file_type.equalsIgnoreCase("audio")){
                View itemView = _inflater.inflate(R.layout.gallery_audio_view, container, false);
                container.addView(itemView);


                boolean playWhenReady = false;
                PlayerView playerView = itemView.findViewById(R.id.audio_view);
                playerView.showController();
                Uri uri = Uri.parse(model.path);
                MediaSource mediaSource = buildMediaSource(uri);
                player = ExoPlayerFactory.newSimpleInstance(
                        new DefaultRenderersFactory(GalleryViewActivity.this),
                        new DefaultTrackSelector(), new DefaultLoadControl());
                playerView.setPlayer(player);
                player.setPlayWhenReady(playWhenReady);
                player.prepare(mediaSource, true, false);




//                final JZVideoPlayerStandard jzVideoPlayerStandard = (JZVideoPlayerStandard)itemView.findViewById(R.id.audio_view);
//                jzVideoPlayerStandard.setUp(model.path,JZVideoPlayerStandard.SCREEN_WINDOW_LIST,"");
                return itemView;
            }else {
                View itemView = new View(GalleryViewActivity.this);
                return itemView;
            }
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        JZVideoPlayer.releaseAllVideos();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }
    @Override
    public void onBackPressed() {
        if (JZVideoPlayer.backPress()) {
            return;
        }
        super.onBackPressed();
    }

    private MediaSource buildMediaSource(Uri uri) {
        @C.ContentType int type = Util.inferContentType(uri);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(
                        new DefaultDashChunkSource.Factory(mediaDataSourceFactory),
                        manifestDataSourceFactory)
                        .createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(
                        new DefaultSsChunkSource.Factory(mediaDataSourceFactory), manifestDataSourceFactory)
                        .createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }




    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    private void releasePlayer() {
        if (player != null) {
//            playbackPosition = player.getCurrentPosition();
//            currentWindow = player.getCurrentWindowIndex();
            //playWhenReady = player.getPlayWhenReady();
            player.release();
            player = null;
        }
    }
}
