package in.evergreen.socialapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import in.evergreen.socialapp.Auth.LoginActivity;
import in.evergreen.socialapp.Utility.TLStorage;

public class SplashActivity extends AppCompatActivity {
    TLStorage sto;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 34;
    int frame=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
//        animateCircle();

        sto = new TLStorage(this);
        final String token11 = sto.getValueString("token");
        final  String id = sto.getValueString("_id");
        Log.e("id is:",id);
        Log.e("token",token11);
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            File folder = new File(this.getFilesDir()+ File.separator + "slateus");
            if (!folder.exists()) {  folder.mkdirs();   }
            File folder2 = new File(Environment.getExternalStorageDirectory()+ File.separator + "Download");
            if (!folder2.exists()) {  folder2.mkdirs();   }
        }


        Thread background = new Thread() {
            public void run() {

                try {
                    // Thread will sleep for 5 seconds
                    sleep(3000);

                    if(sto.getValueString("token").isEmpty()){
                        Intent i=new Intent(getBaseContext(),LoginActivity.class);
                        startActivity(i);
                    }else{
                        openDashboard();
                    }
                    //openDashboard();
                    //Remove activity
                    finish();

                } catch(InterruptedException e){
                    e.printStackTrace();

                }
            }
        };

        // start thread
        background.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }


    public void openDashboard(){
        int ACCESSCOARSELOCATION = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int ACCESSFINELOCATION = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionWriteStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int readPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionWriteStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }

        if (readPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }


        if (ACCESSCOARSELOCATION != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (ACCESSFINELOCATION != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            Log.e("Splash","no permission");
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
        }else{
            Log.e("Splash","show dashboard");
            Intent i=new Intent(getBaseContext(),DashboardActivity.class);
            startActivity(i);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                openDashboard();
            }
            break;
        }
    }

//    public void animateCircle(){
//        setNewFrame();
//    }
//
//    public void setNewFrame(){
//        frame++;
//        if(frame==5)
//            frame=0;
//        switch (frame){
//            case 0:
//                ((ImageView) findViewById(R.id.image)).setImageResource(R.drawable.circle_animation);
//                break;
//            case 1:
//                ((ImageView) findViewById(R.id.image)).setImageResource(R.drawable.circle_animation4);
//                break;
//            case 2:
//                ((ImageView) findViewById(R.id.image)).setImageResource(R.drawable.circle_animation3);
//                break;
//            case 3:
//                ((ImageView) findViewById(R.id.image)).setImageResource(R.drawable.circle_animation2);
//                break;
//            case 4:
//                ((ImageView) findViewById(R.id.image)).setImageResource(R.drawable.circle_animation1);
//                break;
//        }
//
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                setNewFrame();
//            }
//        },200);
//    }
}
