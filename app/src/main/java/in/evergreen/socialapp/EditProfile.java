package in.evergreen.socialapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;


import Apis.ApiClient;
import Apis.UserInterface;
import Models.FriendsSearchModel;
import in.evergreen.socialapp.Utility.TLCallback;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLImageCompressor;
import in.evergreen.socialapp.Utility.TLStorage;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EditProfile extends AppCompatActivity {


    private static int StateList = 01;
    private Context context;
    private String updatedUserImageURL;
    private UserInterface userInterface;
    private JSONObject selfProfile = new JSONObject();
    private JsonParser jsonParser = new JsonParser();
    private String dob = new SimpleDateFormat("yyyy-dd-mm").format(new Date());
    private String gender = "male";
    private String next_action;
    private String title;
    private TLHelper hlp;
    private Effectstype effect;
    private ImageView updateUserImage;
    private String[] profession_list;
    private int[] profession_id;
    private String[] relationship_list;
    private int[] relationship_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        userInterface = ApiClient.getClient().create(UserInterface.class);
        ViewCompat.setTransitionName(findViewById(R.id.edit_appBarLayout), "Edit Profile");
        supportPostponeEnterTransition();
        setSupportActionBar((Toolbar) findViewById(R.id.editProfiletoolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        hlp = new TLHelper(this);
        userInterface = ApiClient.getClient().create(UserInterface.class);
        fetchProfile(MyApplication._id);
        updateUserImage = (ImageView) findViewById(R.id.select_userImage);
    }

    private void start() {
        Log.e("CALL", "OPEn CROPPER");

        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setFlipHorizontally(false)
                .setFlipHorizontally(false)
                .setAutoZoomEnabled(true)
                .setShowCropOverlay(true)
                .setActivityTitle("Image")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setFixAspectRatio(true)
                .setCropMenuCropButtonTitle("Done")
                .setRequestedSize(500, 500)
                .start(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                updateUserImage.setImageURI(result.getUri());
                Uri resultUri = result.getUri();
                uploadImage(result.getUri());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        CropImage.ActivityResult result = CropImage.getActivityResult(data);

        if (requestCode == StateList && resultCode == RESULT_OK) {
            try {
                selfProfile.put("state_id", data.getIntExtra("state_id", 1));
                selfProfile.put("city_id", data.getIntExtra("city_id", 1));
                updateProfile(selfProfile);
                fetchProfile(MyApplication._id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void fetchProfile(int user_id) {
        Call<JsonElement> call = userInterface.getUserProfile(MyApplication.token, user_id);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();

                try {
                    JSONObject res = new JSONObject(body);
                    if (res.getString("status").equalsIgnoreCase("success")) {
                        selfProfile = res.getJSONObject("data");
                        RelativeLayout avatarView = findViewById(R.id.avatarView);
                        TextView txtAvatarText = (TextView) findViewById(R.id.txtNameChar);
                        ImageView img = (ImageView) findViewById(R.id.select_userImage);
                        String avatar = selfProfile.getString("user_image_main");
                        if (!avatar.isEmpty()) {
                            avatarView.setVisibility(View.GONE);
                            img.setVisibility(View.VISIBLE);
                            Glide.with(EditProfile.this).load(selfProfile.getString("user_image_main")).into(img);
                        } else {
                            img.setVisibility(View.GONE);
                            avatarView.setVisibility(View.VISIBLE);
                            txtAvatarText.setText(hlp.getShortName(selfProfile.getString("display_name")));
                        }
                        Glide.with(EditProfile.this).load(selfProfile.getString("user_image_main")).into(img);
                        Log.d("res", String.valueOf(selfProfile));
                        ((TextView) findViewById(R.id.user_fname)).setText(selfProfile.getString("first_name"));
                        ((TextView) findViewById(R.id.user_lname)).setText(selfProfile.getString("last_name"));
                        ((TextView) findViewById(R.id.profile_aboutme_text)).setText(selfProfile.getString("about"));
                        ((TextView) findViewById(R.id.edit_user_state)).setText(selfProfile.getString("state"));
                        ((TextView) findViewById(R.id.user_city)).setText(selfProfile.getString("city"));
                        ((TextView) findViewById(R.id.user_mobile)).setText(selfProfile.getString("mobile"));
                        ((TextView) findViewById(R.id.user_relationship_status)).setText(selfProfile.getString("relationship"));
                        ((TextView) findViewById(R.id.user_interest)).setText(selfProfile.getString("interest"));
                        ((TextView) findViewById(R.id.user_profession)).setText(selfProfile.getString("profession"));
                        ((TextView) findViewById(R.id.user_company)).setText(selfProfile.getString("company"));
                        ((TextView) findViewById(R.id.user_hobbies)).setText(selfProfile.getString("hobbies"));
                        ((TextView) findViewById(R.id.user_website)).setText(selfProfile.getString("website"));
                        ((TextView) findViewById(R.id.user_nickname)).setText(selfProfile.getString("nickname"));
                        ((TextView) findViewById(R.id.user_goal)).setText(selfProfile.getString("goal"));
                        ((TextView) findViewById(R.id.user_technology)).setText(selfProfile.getString("technology"));
                        ((TextView) findViewById(R.id.user_college)).setText(selfProfile.getString("college"));
                        ((TextView) findViewById(R.id.user_life)).setText(selfProfile.getString("life"));
                        ((TextView) findViewById(R.id.user_believe)).setText(selfProfile.getString("believe"));
                        ((TextView) findViewById(R.id.user_book)).setText(selfProfile.getString("book"));
                        ((TextView) findViewById(R.id.user_tagline)).setText(selfProfile.getString("tagline"));
                        ((TextView) findViewById(R.id.user_idol)).setText(selfProfile.getString("idol"));
                        ((TextView) findViewById(R.id.user_actor)).setText(selfProfile.getString("actor"));
                        ((TextView) findViewById(R.id.user_movie)).setText(selfProfile.getString("movie"));
                        ((TextView) findViewById(R.id.user_music)).setText(selfProfile.getString("music"));
                        ((TextView) findViewById(R.id.user_place)).setText(selfProfile.getString("place"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.hideLoader();
                hlp.noConnection();
            }
        });
    }

    public void actionEdit1(View view) {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(this);
        switch (view.getId()) {
            case R.id.edit_Fname:
                effect = Effectstype.Slideleft;
                next_action = "first_name";
                title = "First Name";
                break;
            case R.id.edit_Lname:
                effect = Effectstype.Slideleft;
                next_action = "last_name";
                title = "Last Name";
                break;
            case R.id.edit_about:
                effect = Effectstype.Slideleft;
                next_action = "about";
                title = "About";
                break;
            case R.id.edit_mobile:
                title = "Mobile";
                Log.e("edit_mobile selected", title);
                effect = Effectstype.Fall;
                next_action = "mobile";
                break;

            case R.id.edit_company:
                effect = Effectstype.Slidetop;
                next_action = "company";
                title = "Company";
                break;
            case R.id.edit_hobbies:
                effect = Effectstype.SlideBottom;
                next_action = "hobbies";
                title = "Hobbies";
                break;
            case R.id.edit_website:
                effect = Effectstype.Slideleft;
                next_action = "website";
                title = "Website";
                break;
            case R.id.edit_nickname:
                effect = Effectstype.Slideright;
                next_action = "nickname";
                title = "Nickname";
                break;
            case R.id.edit_goal:
                effect = Effectstype.Slidetop;
                next_action = "goal";
                title = "Goal";
                break;
            case R.id.edit_technology:
                effect = Effectstype.SlideBottom;
                next_action = "technology";
                title = "Technology";
                break;
            case R.id.edit_college:
                effect = Effectstype.Slideleft;
                next_action = "college";
                title = "College";
                break;
            case R.id.edit_about_life:
                effect = Effectstype.Slideright;
                next_action = "life";
                title = "Life";
                break;
            case R.id.edit_about_believe:
                effect = Effectstype.RotateBottom;
                next_action = "believe";
                title = "Believe";
                break;
            case R.id.edit_book:
                effect = Effectstype.Slidetop;
                next_action = "book";
                title = "Book";
                break;
            case R.id.edit_tagline:
                effect = Effectstype.Slideright;
                next_action = "tagline";
                title = "Tagline";
                break;
            case R.id.edit_idol:
                effect = Effectstype.Slideleft;
                next_action = "idol";
                title = "Idol";
                break;
            case R.id.edit_actor:
                effect = Effectstype.Slidetop;
                next_action = "actor";
                title = "Actor";
                break;
            case R.id.edit_movie:
                effect = Effectstype.SlideBottom;
                next_action = "movie";
                title = "Movie";
                break;
            case R.id.edit_place:
                effect = Effectstype.Slidetop;
                next_action = "place";
                title = "Place";
                break;
            case R.id.edit_music:
                effect = Effectstype.Slideright;
                next_action = "music";
                title = "Music";
                break;
        }
        LayoutInflater li = LayoutInflater.from(getApplicationContext());
        View promptsView = li.inflate(R.layout.custom_view, null);
        final EditText et = (EditText) promptsView.findViewById(R.id.editProfileData);
        try {
            et.setText(selfProfile.getString(next_action));
            et.setSelection(et.getText().length());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialogBuilder
                .withTitle(title)
                .withTitleColor(getResources().getColor(R.color.black))
                .withDividerColor(getResources().getColor(R.color.black))
                .withMessage(null)
                .withDialogColor("#FFFFFF")
                .withIcon(getResources().getDrawable(R.mipmap.ic_launcher))
                .isCancelableOnTouchOutside(true)
                .withDuration(400)
                .withButton1Text("UPDATE")
                .withButton2Text("CANCEL")
                .setCustomView(promptsView,EditProfile.this)
                .withEffect(Effectstype.Slideleft)
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String text = et.getText().toString().trim();
                        Log.e("updated", text);
                        try {
                            selfProfile.put(next_action, text);
                            Log.e("updated json", String.valueOf(selfProfile));
                            updateProfile(selfProfile);
                            fetchProfile(MyApplication._id);
                            dialogBuilder.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogBuilder.dismiss();
                    }
                })
                .show();
    }

//    public void actionEdit2(View view) {
//        title = "Gender";
//
//        final Dialog dialog = new Dialog(EditProfile.this);
//        dialog.setContentView(R.layout.custom_dialog_gender);
//        dialog.setTitle(title);
//
//        dialog.show();
//
//        TextView updateDob = (TextView) dialog.findViewById(R.id.updateDob);
//        TextView cancelUpdate = (TextView) dialog.findViewById(R.id.cancelUpdate);
//
//
//
//
//       RelativeLayout male =  ((RelativeLayout) dialog.findViewById(R.id.male_circle));
//       RelativeLayout female =  ((RelativeLayout) dialog.findViewById(R.id.female_circle));
//       RelativeLayout other =  ((RelativeLayout) dialog.findViewById(R.id.other_circle));
//
//       male.setOnClickListener(new View.OnClickListener() {
//           @Override
//           public void onClick(View v) {
//               gender = v.getTag().toString().trim();
//               Log.e("updated gender", gender);
//
//               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                   ((RelativeLayout) dialog.findViewById(R.id.male_circle)).setBackground(getDrawable(R.drawable.gender_unselected));
//                   ((RelativeLayout) dialog.findViewById(R.id.female_circle)).setBackground(getDrawable(R.drawable.gender_unselected));
//                   ((RelativeLayout) dialog.findViewById(R.id.other_circle)).setBackground(getDrawable(R.drawable.gender_unselected));
//               }
//
//               switch (gender){
//                   case "Male":
//                       Log.e("male case",gender);
//                       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                           ((RelativeLayout) dialog.findViewById(R.id.male_circle)).setBackground(getDrawable(R.drawable.gender_selected));
//                       }
//                       break;
//                   case "Female":
//                       Log.e("female case",gender);
//                       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                           ((RelativeLayout) dialog.findViewById(R.id.female_circle)).setBackground(getDrawable(R.drawable.gender_selected));
//                       }
//
//                       break;
//                   case "Other":
//                       Log.e("other case",gender);
//                       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                           ((RelativeLayout) dialog.findViewById(R.id.other_circle)).setBackground(getDrawable(R.drawable.gender_selected));
//                       }
//                       break;
//
//               }
//           }
//       });
//
//        female.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                gender = v.getTag().toString().trim();
//                Log.e("updated dender", gender);
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    ((RelativeLayout) dialog.findViewById(R.id.male_circle)).setBackground(getDrawable(R.drawable.gender_unselected));
//                    ((RelativeLayout) dialog.findViewById(R.id.female_circle)).setBackground(getDrawable(R.drawable.gender_unselected));
//                    ((RelativeLayout) dialog.findViewById(R.id.other_circle)).setBackground(getDrawable(R.drawable.gender_unselected));
//                }
//
//                switch (gender){
//                    case "Male":
//                        Log.e("male case",gender);
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            ((RelativeLayout) dialog.findViewById(R.id.male_circle)).setBackground(getDrawable(R.drawable.gender_selected));
//                        }
//                        break;
//                    case "Female":
//                        Log.e("female case",gender);
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            ((RelativeLayout) dialog.findViewById(R.id.female_circle)).setBackground(getDrawable(R.drawable.gender_selected));
//                        }
//
//                        break;
//                    case "Other":
//                        Log.e("other case",gender);
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            ((RelativeLayout) dialog.findViewById(R.id.other_circle)).setBackground(getDrawable(R.drawable.gender_selected));
//                        }
//                        break;
//
//                }
//            }
//        });
//
//        other.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                gender = v.getTag().toString().trim();
//                Log.e("updated dender", gender);
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    ((RelativeLayout) dialog.findViewById(R.id.male_circle)).setBackground(getDrawable(R.drawable.gender_unselected));
//                    ((RelativeLayout) dialog.findViewById(R.id.female_circle)).setBackground(getDrawable(R.drawable.gender_unselected));
//                    ((RelativeLayout) dialog.findViewById(R.id.other_circle)).setBackground(getDrawable(R.drawable.gender_unselected));
//                }
//
//                switch (gender){
//                    case "Male":
//                        Log.e("male case",gender);
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            ((RelativeLayout) dialog.findViewById(R.id.male_circle)).setBackground(getDrawable(R.drawable.gender_selected));
//                        }
//                        break;
//                    case "Female":
//                        Log.e("female case",gender);
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            ((RelativeLayout) dialog.findViewById(R.id.female_circle)).setBackground(getDrawable(R.drawable.gender_selected));
//                        }
//
//                        break;
//                    case "Other":
//                        Log.e("other case",gender);
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            ((RelativeLayout) dialog.findViewById(R.id.other_circle)).setBackground(getDrawable(R.drawable.gender_selected));
//                        }
//                        break;
//
//                }
//
//
//            }
//        });
//
//
//
//        updateDob.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    selfProfile.put("gender",gender);
//                    updateProfile(selfProfile);
//                    dialog.dismiss();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        });
//        cancelUpdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                dialog.dismiss();
//            }
//        });
//
//    }
//
//    public void actionEdit3(View view) throws JSONException {
//
//        title = "Date Of Birth";
//
//        final Dialog dialog = new Dialog(EditProfile.this);
//        dialog.setContentView(R.layout.custom_dialog_dob);
//        dialog.setTitle(title);
//
//
//        dialog.show();
//
//        DatePicker dp = dialog.findViewById(R.id.update_datepicker);
//
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                dp.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
//                    @Override
//                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        dob = view.getYear() + " / " + (view.getMonth()+1) + " / " + view.getDayOfMonth();
//                    }
//                });
//            }
//
//            Log.e("dob", dob);
//
//            TextView updateDob = (TextView) dialog.findViewById(R.id.updateDob);
//        TextView cancelUpdate = (TextView) dialog.findViewById(R.id.cancelUpdate);
//
//        updateDob.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                try {
//                    selfProfile.put("dob",dob);
//                    updateProfile(selfProfile);
//                    dialog.dismiss();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        cancelUpdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                dialog.dismiss();
//            }
//        });
//    }


    public void actionEdit4(View view) {

        title = "State";

        Intent open = new Intent(this, StateCityListActivity.class);
        startActivityForResult(open, StateList);
    }


    public void relationship() {

        title = "Relationship";
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Relationship");
        builder.setItems(relationship_list, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                int relationship_id_sel = relationship_id[item];
                try {
                    selfProfile.put("relationship_id", relationship_id_sel);
                    updateProfile(selfProfile);
                    fetchProfile(MyApplication._id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

//        ListView list;
//        final Dialog listDialog;
//
//        listDialog = new Dialog(this);
//        listDialog.setContentView(R.layout.custom_view_list);
//        listDialog.setCancelable(true);
//
//
//        list = (ListView) listDialog.findViewById(R.id.dialog_listview);
//        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                int relationship_id_sel = relationship_id[position];
//                try {
//                    selfProfile.put("relationship", relationship_id_sel);
//                    updateProfile(selfProfile);
//                    fetchProfile(MyApplication._id);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//
//
//        list.setAdapter(new ArrayAdapter<String>(this, R.layout.list_relationship, R.id.relationshiplist_text, relationship_list));
//
//        listDialog.show();

    }

    public void actionEdit5(View view) {
        Call<JsonElement> call = userInterface.relationship(MyApplication.token);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                hlp.hideLoader();
                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();
                try {
                    JSONObject res = new JSONObject(body);
                    if (res.getString("status").equalsIgnoreCase("success")) {

                        JSONArray data = res.getJSONArray("data");
                        Log.e("relationship data", data.toString());
                        try {
                            relationship_list = new String[data.length()];
                            relationship_id = new int[data.length()];
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject = data.getJSONObject(i);
                                relationship_id[i] = jsonObject.getInt("_id");
                                relationship_list[i] = jsonObject.getString("name");
                            }
                            relationship();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });

    }

    public void actionEdit6(View view) {

        title = "Interest";
        String[] val = {"Men", "Women", "Both"};
        ListView list;
        final Dialog listDialog;
        listDialog = new Dialog(this);
        listDialog.setTitle("Select Item");
        listDialog.setContentView(R.layout.custom_view_list);
        listDialog.setCancelable(true);
        list = (ListView) listDialog.findViewById(R.id.dialog_listview);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listDialog.dismiss();
                String interest = ((TextView) view).getText().toString();
                try {
                    selfProfile.put("interest", interest);
                    updateProfile(selfProfile);
                    fetchProfile(MyApplication._id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        list.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, val));
        listDialog.show();
    }

    public void actionEdit7(View view) {
        Call<JsonElement> call = userInterface.profession(MyApplication.token);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                hlp.hideLoader();
                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();
                try {
                    JSONObject res = new JSONObject(body);
                    if (res.getString("status").equalsIgnoreCase("success")) {
                        JSONArray data = res.getJSONArray("data");
                        try {
                            profession_list = new String[data.length()];
                            profession_id = new int[data.length()];
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject = data.getJSONObject(i);
                                Log.e("jobj", jsonObject.toString());
                                profession_id[i] = jsonObject.getInt("_id");
                                profession_list[i] = jsonObject.getString("profession");
                            }
                            profession();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.noConnection();
                hlp.hideLoader();
            }
        });

    }

    public void profession() {

        title = "Profession";
//        ListView list;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Profession");
        builder.setItems(profession_list, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                int profession_id_sel = profession_id[item];
                try {
                    selfProfile.put("profession_id", profession_id_sel);
                    updateProfile(selfProfile);
                    fetchProfile(MyApplication._id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
//        final AlertDialog listDialog = new AlertDialog(this);
//        listDialog.setTitle("Select Profession");
//        listDialog.setContentView(R.layout.custom_view_list);
//        listDialog.setCancelable(true);
//        listDialog.
//        list = (ListView) listDialog.findViewById(R.id.dialog_listview);
//
//        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//        });
//        list.setAdapter(new ArrayAdapter<String>(this, R.layout.list_relationship, R.id.relationshiplist_text, profession_list));
//        listDialog.show();
    }

    public void actionChooseImage(View view) {
        start();
    }

    public void uploadImage(Uri path) {
        hlp.showLoader("Please wait...");
        title = "Image";
        File file = new File(path.getPath());

        final RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload_test");

//        jsonParser.parse(selfProfile.toString());
//        JsonObject jsonObject = (JsonObject) jsonParser.parse(selfProfile.toString());
        Call<JsonElement> call = userInterface.updateProfileImage(MyApplication.token, image, name);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                hlp.hideLoader();
                if (response.isSuccessful()) {
                    JsonElement jsonElement = response.body();
                    String body = jsonElement.toString();
                    Log.e("RESPONSE", body);
                    try {
                        JSONObject res = new JSONObject(body);
                        if (res.getString("status").equalsIgnoreCase("success")) {

                            String imagepath = res.getString("data");

                            TLStorage sto = new TLStorage(EditProfile.this);
                            sto.setValueString("image", imagepath);
                            MyApplication.image = imagepath;
                            Toast.makeText(EditProfile.this, title + " Updated", Toast.LENGTH_LONG).show();
                            fetchProfile(MyApplication._id);
                        } else {
                            hlp.showToast(res.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    hlp.showError(response.code());
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.hideLoader();
                hlp.noConnection();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void updateProfile(JSONObject obj) {

        Log.e("updated job", String.valueOf(selfProfile));
        jsonParser.parse(selfProfile.toString());
        JsonObject jsonObject = (JsonObject) jsonParser.parse(selfProfile.toString());
        hlp.showLoader("Please wait ...");
        Call<JsonElement> call = userInterface.updateProfile(MyApplication.token, jsonObject);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                hlp.hideLoader();

                Toast.makeText(EditProfile.this, title + " Updated", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.noConnection();
            }
        });
    }

}
