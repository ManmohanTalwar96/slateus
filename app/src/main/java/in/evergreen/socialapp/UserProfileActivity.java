package in.evergreen.socialapp;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Apis.ApiClient;
import Apis.UserInterface;
import Models.ProfileFriendsModel;
import in.evergreen.socialapp.Adapters.ProfilePagerAdapter;
import in.evergreen.socialapp.Fragments.ProfileFriendsFragment;
import in.evergreen.socialapp.Fragments.ProfilePostFragment;
import in.evergreen.socialapp.Fragments.ProfileProfileFragment;
import in.evergreen.socialapp.Utility.CircularImageView;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileActivity extends AppCompatActivity {

    private TLStorage sto;
    private TLHelper hlp;
    private int user_id;
    private String name;
    private ImageView userImage;
    private RelativeLayout addFriend;
    private RelativeLayout cancelFriend;
    private RelativeLayout acceptFriendR;
    private RelativeLayout denyFriendR;
    private LinearLayout acceptFriendRequest;
    public String current_status = "";
    private ArrayList<ProfileFriendsModel> items = new ArrayList<ProfileFriendsModel>();
    private UserInterface userInterface;
    private ProfileProfileFragment profileFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        sto = new TLStorage(this);
        hlp = new TLHelper(this);

        Bundle b  = getIntent().getExtras();

        user_id = b.getInt("user_id");
        name = b.getString("user_name");

        profileFragment = ProfileProfileFragment.getInstance(this,this, user_id);

        ViewCompat.setTransitionName(findViewById(R.id.profile_appBarLayout), "User Profile");
        supportPostponeEnterTransition();

        setSupportActionBar((Toolbar) findViewById(R.id.ctoolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);


        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbarr);
        //collapsingToolbarLayout.setTitle(name);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        userInterface = ApiClient.getClient().create(UserInterface.class);

        ProfilePagerAdapter profilePagerAdapter = new ProfilePagerAdapter(getSupportFragmentManager());

        ViewPager profileViewPager = (ViewPager) findViewById(R.id.profile_viewpager);
        setupViewPager(profileViewPager);

        TextView userName = (TextView) findViewById(R.id.display_name);
        userName.setText(name);


        TabLayout profileTabs = (TabLayout) findViewById(R.id.profile_tabs);
        profileTabs.setupWithViewPager(profileViewPager);

        fetchProfile(user_id);

        RelativeLayout sendMessage = (RelativeLayout) findViewById(R.id.sendMessage_button);
        addFriend = (RelativeLayout) findViewById(R.id.addFriend_button);
        cancelFriend = (RelativeLayout) findViewById(R.id.cancelRequest_button);
        acceptFriendR = (RelativeLayout) findViewById(R.id.acceptFriend_button);
        denyFriendR = (RelativeLayout) findViewById(R.id.denyFriend_button);

        acceptFriendRequest = (LinearLayout) findViewById(R.id.acceptDeny_container);


        sendMessage.setVisibility(View.GONE);
        addFriend.setVisibility(View.GONE);
        cancelFriend.setVisibility(View.GONE);
        acceptFriendRequest.setVisibility(View.GONE);

       userImage = (ImageView) findViewById(R.id.user_profile_image);
    }

    private void fetchProfile(int user_id) {

        Call<JsonElement> call = userInterface.getUserProfile(MyApplication.token,user_id);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                hlp.hideLoader();
                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();

                try {

                    JSONObject res = new JSONObject(body);

                    if(res.getString("status").equalsIgnoreCase("success")){
                        JSONObject data = res.getJSONObject("data");
                        profileFragment.setProfileFields(data);
                        ((TextView) findViewById(R.id.userprofile_about_me)).setText(data.getString("about"));


                        RelativeLayout avatarView = findViewById(R.id.avatarView);
                        TextView txtAvatarText = (TextView) findViewById(R.id.txtNameChar);


                        String avatar = data.getString("user_image_main");

                        if(!avatar.isEmpty()){
                            avatarView.setVisibility(View.GONE);
                            userImage.setVisibility(View.VISIBLE);
                            Glide.with(getApplicationContext()).load(data.getString("user_image_main")).into(userImage);
                        }else{
                            userImage.setVisibility(View.GONE);
                            avatarView.setVisibility(View.VISIBLE);
                            txtAvatarText.setText(hlp.getShortName(data.getString("display_name")));
                        }
                        current_status = data.getString("friend_status");
                        actionRequestShown();
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.hideLoader();
                hlp.noConnection();
            }
        });

    }

    public void actionRequestShown(){

        addFriend.setVisibility(View.GONE);
        cancelFriend.setVisibility(View.GONE);
        acceptFriendRequest.setVisibility(View.GONE);

        if (current_status.equalsIgnoreCase("AD"))
            acceptFriendRequest.setVisibility(View.VISIBLE);
        if (current_status.equalsIgnoreCase("N"))
            addFriend.setVisibility(View.VISIBLE);
        if (current_status.equalsIgnoreCase("C"))
            cancelFriend.setVisibility(View.VISIBLE);


        if(user_id == MyApplication._id){
            addFriend.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public  void actionRequest(final String action_type, final int user_id, final String status){


        Call<JsonElement> call;

        if(action_type.equalsIgnoreCase("accept"))
            call = userInterface.acceptFriendRequest(MyApplication.token, user_id);
         else if (action_type.equalsIgnoreCase("deny"))
            call = userInterface.denyFriendRequest(MyApplication.token, user_id);
         else if (action_type.equalsIgnoreCase("request"))
            call = userInterface.sendFriendRequest(MyApplication.token, user_id);
         else if (action_type.equalsIgnoreCase("cancel"))
            call = userInterface.cancelFriendRequest(MyApplication.token, user_id);
        else
            call = userInterface.cancelFriendRequest(MyApplication.token, user_id);

        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                hlp.hideLoader();
                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();
                try {
                    JSONObject res = new JSONObject(body);

                    if (res.getString("status").equalsIgnoreCase("success")){

                        current_status = nextStatus(action_type,status);
                        Log.e("statuss_changed", current_status);
                        actionRequestShown();

                    } else
                        hlp.showAlert("", res.getString("message"));


                }catch (JSONException e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.hideLoader();
                hlp.noConnection();
            }
        });

    }

    private String nextStatus(String action_type, String status) {

        Log.e("see_changed_status", status);

        if(action_type.equalsIgnoreCase("accept") && status.equalsIgnoreCase("AD"))
            return "AF";
        if(action_type.equalsIgnoreCase("deny") && status.equalsIgnoreCase("AD"))
            return "N";
        if(action_type.equalsIgnoreCase("cancel") && status.equalsIgnoreCase("C"))
            return "N";
        if(action_type.equalsIgnoreCase("request") && status.equalsIgnoreCase("N"))
            return "C";

        Log.e("cancel API called", status);
        return "";
    }


    private void  setupViewPager(ViewPager viewPager){

    ProfilePagerAdapter adapter = new ProfilePagerAdapter(getSupportFragmentManager());

    adapter.addFragments( new ProfilePostFragment(this,user_id), "Post");
    adapter.addFragments( new ProfileFriendsFragment(items, this, user_id), "Friends");
    adapter.addFragments( profileFragment, "Profile");

    viewPager.setAdapter(adapter);


    }

    public void addFriend(View view) {

        actionRequest("request", getIntent().getIntExtra("user_id",0), current_status);

    }

    public void cancelFriend(View view) {

        actionRequest("cancel", getIntent().getIntExtra("user_id",0), current_status);
    }

    public void sendMessage(View view) {
    }

    public void acceptFriend(View view) {

        actionRequest("accept", getIntent().getIntExtra("user_id",0), current_status);

    }

    public void denyFriend(View view) {

        actionRequest("deny", getIntent().getIntExtra("user_id",0), current_status);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void closeActivity(View v){
        finish();
    }

}
