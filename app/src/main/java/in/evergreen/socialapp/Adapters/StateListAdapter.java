package in.evergreen.socialapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import Models.StateModel;
import in.evergreen.socialapp.CityListActivity;
import in.evergreen.socialapp.EditProfile;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.StateCityListActivity;
import in.evergreen.socialapp.Utility.TLHelper;

public class StateListAdapter extends RecyclerView.Adapter<StateListAdapter.StateViewHolder> {

    private ArrayList<StateModel> items;
    private Context context;
    private int STATE = 11;
    private TLHelper hlp;

    public StateListAdapter(ArrayList<StateModel> items, Context context) {
        this.items = items;
        this.context = context;
        hlp = new TLHelper(context);
    }

    @Override
    public StateViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_state_city, parent, false);
        StateListAdapter.StateViewHolder viewHolder = new StateListAdapter.StateViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(StateViewHolder holder, int position) {

            final StateModel model = items.get(position);
            final View v = holder.v;

        RelativeLayout state = (RelativeLayout) v.findViewById(R.id.state_city);

     TextView state_text = (TextView) v.findViewById(R.id.state_city_text);
           state_text.setText(model.name);
        state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent city = new Intent(context, CityListActivity.class);
                city.putExtra("state_id", model.state_id);
                ((StateCityListActivity)context).startActivityForResult(city, STATE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class StateViewHolder extends RecyclerView.ViewHolder {
        View v;
        public StateViewHolder(View itemView) {
            super(itemView);
            this.v = itemView;
        }
    }
}
