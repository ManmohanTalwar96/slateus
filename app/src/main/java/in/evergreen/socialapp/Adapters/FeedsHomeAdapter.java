package in.evergreen.socialapp.Adapters;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.ArrayList;

import Apis.ApiClient;
import Apis.PostInterface;
import Models.FeedCommentModel;
import Models.FeedLikeModel;
import Models.FeedsModel;
import in.evergreen.socialapp.CommentsActivity;
import in.evergreen.socialapp.DashboardActivity;
import in.evergreen.socialapp.LikesActivity;
import in.evergreen.socialapp.MyApplication;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.UserProfileActivity;
import in.evergreen.socialapp.Utility.DateFormatter;
import in.evergreen.socialapp.Utility.DownloadCallback;
import in.evergreen.socialapp.Utility.DownloadTask;
import in.evergreen.socialapp.Utility.PostMediaViewer;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedsHomeAdapter extends RecyclerView.Adapter {
    private ArrayList<FeedsModel> items;
    private Context mContext;
    TLHelper hlp;
    DateFormatter df;

    private float x1,x2;
    static final int MIN_DISTANCE = 150;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    TLStorage sto ;
    PostInterface postInterface;


    public FeedsHomeAdapter(Context context, ArrayList<FeedsModel> items) {
        hlp = new TLHelper(context);
        this.items = items;
        this.mContext = context;
        sto = new TLStorage(mContext);
        df = new DateFormatter();
        postInterface = ApiClient.getClient().create(PostInterface.class);
    }

    public void updateItems(ArrayList<FeedsModel> newItems) {
        ArrayList<FeedsModel> updatedItems = new ArrayList<>();
        updatedItems = newItems;
        items = updatedItems;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_header_layout, parent, false);
            return new HeaderViewHolder(layoutView);
        } else if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_feed, null);
            return new CustomViewHolder(view);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    public void showUserDetail(String name, String avatar, View v) {
        TextView username = (TextView) v.findViewById(R.id.txtUserName);
        username.setText(hlp.getCapsSentences(name));
        ImageView userimage = (ImageView) v.findViewById(R.id.userImage);
        TextView txtAvatarText = (TextView) v.findViewById(R.id.txtNameChar);
        if (avatar.isEmpty() == false) {
            userimage.setVisibility(View.VISIBLE);
            txtAvatarText.setVisibility(View.GONE);
            Glide.with(mContext).load(avatar).into(userimage);
        } else {
            userimage.setVisibility(View.GONE);
            txtAvatarText.setVisibility(View.VISIBLE);
            txtAvatarText.setText(hlp.getShortName(name));
        }
    }

    public void showUserImage(String name, String avatar, View v) {
        ImageView userimage = (ImageView) v.findViewById(R.id.userImage);
        TextView txtAvatarText = (TextView) v.findViewById(R.id.txtNameChar);
        if (avatar.isEmpty() == false) {
            userimage.setVisibility(View.VISIBLE);
            txtAvatarText.setVisibility(View.GONE);
            Glide.with(mContext).load(avatar).into(userimage);
        } else {
            userimage.setVisibility(View.GONE);
            txtAvatarText.setVisibility(View.VISIBLE);
            txtAvatarText.setText(hlp.getShortName(name));
        }
    }
    public void showSelfImage(View v) {
        ImageView userimage = (ImageView) v.findViewById(R.id.userImageComment);
        TextView txtAvatarText = (TextView) v.findViewById(R.id.txtNameCharComment);
        if (MyApplication.image.isEmpty() == false) {
            userimage.setVisibility(View.VISIBLE);
            txtAvatarText.setVisibility(View.GONE);
            Glide.with(mContext).load(MyApplication.image).into(userimage);
        } else {
            userimage.setVisibility(View.GONE);
            txtAvatarText.setVisibility(View.VISIBLE);
            txtAvatarText.setText(hlp.getShortName(MyApplication.display_name));
        }
    }



    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int index ) {


        if (holder instanceof HeaderViewHolder) {
            View v = ((HeaderViewHolder)holder).v;
            if(mContext instanceof DashboardActivity){
                showUserImage(MyApplication.display_name,MyApplication.image,v);
                v.findViewById(R.id.btnCreatePost).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((DashboardActivity) mContext).createPost(view);
                    }
                });
            }else {
                v.setVisibility(View.GONE);
            }

        }else if (holder instanceof CustomViewHolder) {
            final int i = index ;
            final View v = ((CustomViewHolder)holder).v;
            v.findViewById(R.id.btnAction).setVisibility(View.GONE);
            showSelfImage(v);
            final FeedsModel model = items.get(i);
            View postHeader = v.findViewById(R.id.postHeader);
            postHeader.setVisibility(View.GONE);
            if(model.isShared==1){
                showUserDetail(model.owner_display_name,model.owner_image_thumb, v);
                postHeader.setVisibility(View.VISIBLE);
                ((TextView)v.findViewById(R.id.postHeader_user)).setText(hlp.getCapsSentences(model.user_display_name));
                ((TextView)v.findViewById(R.id.postHeader_action)).setText(" shared ");
                ((TextView)v.findViewById(R.id.postHeader_feed)).setText("this post");
                v.findViewById(R.id.postHeader_user).setClickable(true);
                v.findViewById(R.id.postHeader_user).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, UserProfileActivity.class);
                        intent.putExtra("user_id",model.user_id);
                        intent.putExtra("user_name",model.user_display_name);
                        mContext.startActivity(intent);
                    }
                });
                v.findViewById(R.id.btnShowProfile).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, UserProfileActivity.class);
                        intent.putExtra("user_id",model.owner_id);
                        intent.putExtra("user_name",model.owner_display_name);
                        mContext.startActivity(intent);
                    }
                });

            }else {
                showUserDetail(model.user_display_name,model.user_image_thumb, v);
                v.findViewById(R.id.btnShowProfile).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, UserProfileActivity.class);
                        intent.putExtra("user_id",model.user_id);
                        intent.putExtra("user_name",model.user_display_name);
                        mContext.startActivity(intent);
                    }
                });

            }



            TextView tagView = (TextView) v.findViewById(R.id.txtTags);
            SpannableStringBuilder spanTxt = new SpannableStringBuilder("with- ");
            SpannableStringBuilder spanTxtLess = new SpannableStringBuilder("with- ");
            try {
                JSONArray jarr=new JSONArray(model.tags);
                for(int j=0; j<jarr.length(); j++){

                    JSONObject jo=jarr.getJSONObject(j);
                    final int id = jo.getInt("_id");
                    final String name = jo.getString("display_name");

                    String tagname=hlp.getCapsSentences(jo.getString("display_name"));
                    int namelength=tagname.length();
                    if(j<(jarr.length()-2)){
                        tagname=tagname+", ";
                    }else if(j<(jarr.length()-1)){
                        tagname=tagname+" and ";
                    }

                    spanTxt.append(tagname);
                    spanTxt.setSpan(new ClickableSpan() {
                        @Override
                        public void onClick(View widget) {
                            Intent in=new Intent(mContext,UserProfileActivity.class);
                            in.putExtra("user_id",id);
                            in.putExtra("user_name",name);
                            mContext.startActivity(in);

                        }
                    }, spanTxt.length() - tagname.length(), spanTxt.length()- (tagname.length()-namelength), 0);
                    final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
                    spanTxt.setSpan(bss, spanTxt.length() - tagname.length(), spanTxt.length()- (tagname.length()-namelength), 0);

                }

                if(jarr.length()>0){
                    tagView.setVisibility(View.VISIBLE);
                    tagView.setMovementMethod(LinkMovementMethod.getInstance());
                    tagView.setText(spanTxt, TextView.BufferType.SPANNABLE);
                }else {
                    tagView.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }



            if(model.allow_delete==1){
                v.findViewById(R.id.btnAction).setVisibility(View.VISIBLE);
                v.findViewById(R.id.btnAction).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popup = new PopupMenu(mContext, v.findViewById(R.id.btnAction));
                        popup.inflate(R.menu.menu_post);
                        Menu m=popup.getMenu();
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                if(item.getItemId()==R.id.deletepost){
                                    deletePost(model._id,i);
                                }
                                return false;
                            }
                        });

                        try{
                            Method me = m.getClass().getDeclaredMethod(
                                    "setOptionalIconsVisible", Boolean.TYPE);
                            me.setAccessible(true);
                            me.invoke(m, true);
                        }
                        catch(NoSuchMethodException e){

                        }
                        catch(Exception e){
                            throw new RuntimeException(e);
                        }
                        popup.show();
                    }
                });
            }

            if(model.content.isEmpty()){
                v.findViewById(R.id.txtContent).setVisibility(View.GONE);
            }else{
                v.findViewById(R.id.txtContent).setVisibility(View.VISIBLE);
                ((TextView)v.findViewById(R.id.txtContent)).setText(model.content);
            }

            try {
                String added_on = df.convertDate(model.added_on);
                ((TextView)v.findViewById(R.id.txtPostDate)).setText(added_on);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            final ImageView heart = (ImageView) v.findViewById(R.id.imgLike);
            if(model.self_liked==0){
                heart.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_hearts));
            }else {
                heart.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_filled));
            }
            v.findViewById(R.id.btnLike).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    likePost(v,model,i);
                }
            });


            ((TextView)v.findViewById(R.id.txtLikeCount)).setText(model.like_counts>0?((model.like_counts>1)?model.like_counts+" Likes":"1 Like"):"Like");
            ((TextView)v.findViewById(R.id.txtCommentCount)).setText(model.comment_counts>0?((model.comment_counts>1)?model.comment_counts+" Comments":"1 Comment"):"Comments");
            ViewGroup vg = (ViewGroup) v.findViewById(R.id.imageContainer);
            PostMediaViewer pv = new PostMediaViewer(mContext,vg,i);
            pv.showImages(model.media);

            String like_sentance = "";
            if(model.likes.size()>0){
                for(int j=0; j<model.likes.size();j++){
                    FeedLikeModel flm = model.likes.get(j);
                    if(flm.user_id== MyApplication._id){
                        like_sentance = "You";
                    }else {
                        like_sentance = hlp.getCapsSentences(flm.name);
                    }
                    if(j<model.likes.size()-1){
                        like_sentance = like_sentance+", ";
                    }
                }
            }
            if(like_sentance.isEmpty()==false){
                int remaining = model.like_counts - model.likes.size();
                if(remaining>0){
                    like_sentance = like_sentance +" and "+remaining+" others liked this post";
                }else{
                    like_sentance = like_sentance +" liked this post";
                }
                ((TextView) v.findViewById(R.id.likeSentance)).setText(like_sentance);
                v.findViewById(R.id.likeSentanceCon).setVisibility(View.VISIBLE);
                v.findViewById(R.id.likeSentanceCon).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }else{
                v.findViewById(R.id.likeSentanceCon).setVisibility(View.GONE);
            }
            ViewGroup cvg = (ViewGroup) v.findViewById(R.id.commentContainer);
            cvg.removeAllViews();
            for(int j=0; j<model.comments.size();j++){
                FeedCommentModel fcm = model.comments.get(j);
                LayoutInflater li = LayoutInflater.from(mContext);
                View view = li.inflate(R.layout.list_comments_in_feed, null);
                showUserDetail(fcm.name,fcm.user_image_thumb, view);
                ((TextView) view.findViewById(R.id.txtComment)).setText(fcm.comment);
                cvg.addView(view);
            }

            v.findViewById(R.id.btnPostComment).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vv) {
                    EditText txtComment = (EditText) v.findViewById(R.id.txtPostComment);
                    String comment = txtComment.getText().toString();
                    txtComment.setText("");
                    if(comment.isEmpty()==false){
                        postComment(comment, model._id, i,txtComment);
                    }
                }
            });

            v.findViewById(R.id.btnComments).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vv) {
                    Intent intent =new Intent(mContext, CommentsActivity.class);
                    intent.putExtra("post_id",model._id);
                    intent.putExtra("index",index);
                    mContext.startActivity(intent);
                }
            });

            v.findViewById(R.id.btnShare).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vv) {
                    String media="";
                    if(model.media.size()>0){
                        media=model.media.get(0).path;
                    }
                    sharePost(model._id,model.content,media);
                }
            });
            v.findViewById(R.id.likeSentanceCon).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, LikesActivity.class);
                    intent.putExtra("post_id", model._id);
                    mContext.startActivity(intent);
                    ((Activity)mContext).overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );
                }
            });



        }
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        //return (null != items ? items.size()+1 : 0);
        return (null != items ? items.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        //if (isPositionHeader(position))
        //    return TYPE_HEADER;
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder{
        private View v;
        public CustomViewHolder(View view) {
            super(view);
            this.v = view;
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder{
        private View v;
        public HeaderViewHolder(View itemView) {
            super(itemView);
            this.v = itemView;
        }
    }

    public void likePost(final View v, final FeedsModel model, final int index){
        final int main_status = model.self_liked;
        final int main_count = model.like_counts;
        Call<JsonElement> call;
        final ImageView iv = (ImageView) v.findViewById(R.id.imgLike);
        if(model.self_liked==0){
            iv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_filled));
            ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(iv,
                    PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                    PropertyValuesHolder.ofFloat("scaleY", 1.2f));
            scaleDown.setDuration(300);
            scaleDown.setRepeatCount(3);
            scaleDown.setRepeatMode(ObjectAnimator.REVERSE);
            scaleDown.start();
            model.like_counts++;
            model.self_liked=1;

            call = postInterface.setLike(sto.getValueString("token"),model._id, "post");
        }else{
            iv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_hearts));
            model.like_counts--;
            model.self_liked=0;
            call = postInterface.setUnlike(sto.getValueString("token"),model._id, "post");
        }
        ((TextView)v.findViewById(R.id.txtLikeCount)).setText(model.like_counts>0?((model.like_counts>1)?model.like_counts+" Likes":"1 Like"):"Like");
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                model.like_counts=main_count;
                model.self_liked=main_status;
                ((TextView)v.findViewById(R.id.txtLikeCount)).setText(model.like_counts>0?((model.like_counts>1)?model.like_counts+" Likes":"1 Like"):"Like");
                if(model.self_liked==0){
                    iv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_hearts));
                }else {
                    iv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_filled));
                }
            }
        });
    }

    public void postComment(final String comment, int post_id, final int index, final EditText txtComment){

        JSONObject jo = new JSONObject();
        try {
            jo.put("comment", comment);
            jo.put("post_id", post_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = (JsonObject) jsonParser.parse(jo.toString());
        Call<JsonElement>  call = postInterface.createComment(MyApplication.token,jsonObject);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();
                Log.e("RESPONSE", body);
                try {
                    JSONObject res = new JSONObject(body);
                    if(res.getString("status").equalsIgnoreCase("success")){
                        Log.e("STATUS", "SUCCESS");
                        items.get(index).comments.add(new FeedCommentModel(res.getJSONObject("data")));
                        notifyItemChanged(index);
                    }else{

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                txtComment.setText(comment);
                hlp.noConnection();
            }
        });
    }


    public void deletePost(final int post_id, final int index){
        hlp.confirmDialog("Delete Post", "Are you sure to delete this post?", "Delete" , new TLHelper.MyCallback() {
            @Override
            public void callbackCall() {
                Call<JsonElement>  call = postInterface.deletePost(MyApplication.token,post_id);
                call.enqueue(new Callback<JsonElement>() {

                    @Override
                    public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                        JsonElement jsonElement = response.body();
                        String body = jsonElement.toString();
                        Log.e("RESPONSE", body);
                        try {
                            JSONObject res = new JSONObject(body);
                            if(res.getString("status").equalsIgnoreCase("success")){
                                items.remove(index);
                                notifyItemRemoved(index);
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        hlp.noConnection();
                    }
                });
            }
        });

    }

    public void sharePost(final int post_id, final String content, final String media){
        final CharSequence[] items = {"Share on Slateus", "Share on Other Apps"};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Share Post");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if(item==0){
                    Call<JsonElement> call = postInterface.sharePost(MyApplication.token,post_id);
                    call.enqueue(new Callback<JsonElement>() {

                        @Override
                        public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                            if(mContext instanceof DashboardActivity){
                                ((DashboardActivity)mContext).page =1;
                                ((DashboardActivity)mContext).pageend =false;
                                ((DashboardActivity)mContext).fetchRecords();
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonElement> call, Throwable t) {
                            hlp.noConnection();
                        }
                    });
                }
                if(item==1){
                    sharePostSocial(content,media);
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void sharePostSocial(String t, String image){
        t=t.replace("<br />","\n");
        // final String title = Jsoup.parse(t).text();
        String appPackageName = mContext.getPackageName();
        String link="http://play.google.com/store/apps/details?id=" + appPackageName;
        final String title=t+"\n"+link;
        if(image.isEmpty()){
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setType("text/plain");
            //whatsappIntent.setPackage("com.whatsapp");
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, title);
            try {
                mContext.startActivity(whatsappIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                hlp.showToast("Unable to share");
            }
            return;
        }
        DownloadTask dt=new DownloadTask(mContext,new DownloadCallback(){

            @Override
            public void downloadComplete(String filepath) {
                hlp.hideLoader();
                Log.i("DOWNLOAD COMPLETE","Path:"+filepath);
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, title);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(filepath)));
                //shareIntent.putExtra(Intent.EXTRA_STREAM, filepath);
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                mContext.startActivity(Intent.createChooser(shareIntent, "Share Post"));
            }

            @Override
            public void downloadStart() {
                hlp.showLoader("Preparing file to share");
                Log.i("DOWNLOAD START","Stared");
            }

            @Override
            public void downloadFailed() {
                hlp.hideLoader();
                hlp.showToast("Unable to share post");
                Log.i("DOWNLOAD FAILED","FAILED");
            }

            @Override
            public void downloadProgress(int progress) {
                Log.i("DOWNLOAD PROGRESS","Status:"+progress);
            }
        });
        dt.execute(image);
    }

}
