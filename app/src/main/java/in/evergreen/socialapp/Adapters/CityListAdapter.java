package in.evergreen.socialapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import Models.CityModel;
import in.evergreen.socialapp.CityListActivity;
import in.evergreen.socialapp.EditProfile;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.TLHelper;

public class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.CityViewHolder>{

    private ArrayList<CityModel> items = new ArrayList<>();
    private Context context;
    private int city_state = 1;
    private TLHelper hlp;

    public CityListAdapter(ArrayList<CityModel> items, Context context) {
        this.items = items;
        this.context = context;
        hlp = new TLHelper(context);
    }

    @Override
    public CityViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_state_city, parent, false);
        CityListAdapter.CityViewHolder viewHolder = new CityListAdapter.CityViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {

        final CityModel model = items.get(position);
        View v = holder.v;

        RelativeLayout city = (RelativeLayout) v.findViewById(R.id.state_city);

        TextView city_text = (TextView) v.findViewById(R.id.state_city_text);
        city_text.setText(model.city_name);
        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent() ;
                int state_id = (model.state_id);
                int city_id = (model.city_id);
                i.putExtra("state_id", state_id);
                i.putExtra("city_id", city_id);
                ((CityListActivity)context).setResult(Activity.RESULT_OK, i);
                ((CityListActivity) context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class CityViewHolder extends RecyclerView.ViewHolder {
        View v;
        public CityViewHolder(View itemView) {
            super(itemView);
            this.v = itemView;
        }
    }
}
