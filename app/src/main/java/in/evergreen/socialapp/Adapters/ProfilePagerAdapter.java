package in.evergreen.socialapp.Adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ProfilePagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> profileFragmentList = new ArrayList<>();
    private final List<String> profileFragmentTitle = new ArrayList<>();


        public void addFragments(Fragment fragments, String title){

            profileFragmentList.add(fragments);
            profileFragmentTitle.add(title);
        }


    public ProfilePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return profileFragmentTitle.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return profileFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return profileFragmentList.size();
    }
}
