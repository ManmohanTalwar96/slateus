package in.evergreen.socialapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import Apis.ApiClient;
import Apis.UserInterface;
import Models.BirthdayModel;
import Models.FriendsSearchModel;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.TLHelper;

public class BirthdayAdapter extends RecyclerView.Adapter<BirthdayAdapter.BirthdayViewHolder>{

    private ArrayList<BirthdayModel> items;
    private Context context;
    UserInterface userInterface;
    TLHelper hlp;

    public BirthdayAdapter(ArrayList<BirthdayModel> items, Context context) {
        this.items = items;
        this.context = context;
        hlp = new TLHelper(context);

        userInterface = ApiClient.getClient().create(UserInterface.class);
    }

    @Override
    public BirthdayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_birthday, parent, false);
        BirthdayAdapter.BirthdayViewHolder viewHolder = new  BirthdayAdapter.BirthdayViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BirthdayViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class BirthdayViewHolder extends RecyclerView.ViewHolder{
        private View v;
        public BirthdayViewHolder(View itemView) {
            super(itemView);
            this.v = itemView;
        }
    }
}
