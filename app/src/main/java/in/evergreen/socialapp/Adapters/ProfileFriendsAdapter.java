package in.evergreen.socialapp.Adapters;

import android.content.Context;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Apis.ApiClient;
import Apis.UserInterface;
import Models.ProfileFriendsModel;
import in.evergreen.socialapp.MyApplication;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.UserProfileActivity;
import in.evergreen.socialapp.Utility.TLHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFriendsAdapter extends RecyclerView.Adapter<ProfileFriendsAdapter.ProfileFriendsViewHolder>{


    private ArrayList<ProfileFriendsModel> items;
    private Context context;
    private TLHelper hlp;
    private UserInterface userInterface;

    public ProfileFriendsAdapter(ArrayList<ProfileFriendsModel> items, Context context) {
        this.items = items;
        this.context = context;

        userInterface = ApiClient.getClient().create(UserInterface.class);
       hlp = new TLHelper(context);
    }

    @Override
    public ProfileFriendsViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_profile_friends, parent, false);
        ProfileFriendsAdapter.ProfileFriendsViewHolder viewHolder = new ProfileFriendsAdapter.ProfileFriendsViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProfileFriendsAdapter.ProfileFriendsViewHolder holder, final int position) {

        final ProfileFriendsModel model = items.get(position);
        final View v = holder.v;

        RelativeLayout avatarView = v.findViewById(R.id.profile_avatarView);
        TextView txtAvatarText = (TextView) v.findViewById(R.id.profile_txtNameChar);
        ImageView img = (ImageView) v.findViewById(R.id.profile_user_image);
//       Glide.with(context).load(model.user_image_thumb).into(img);

        String avatar = model.user_image_thumb;
        if(avatar.isEmpty()==false){
            img.setVisibility(View.VISIBLE);
            avatarView.setVisibility(View.GONE);
            Glide.with(context).load(model.user_image_thumb).into(img);
        }else{
            img.setVisibility(View.GONE);
            avatarView.setVisibility(View.VISIBLE);
            txtAvatarText.setText(hlp.getShortName(model.display_name));
        }
//        Glide.with(this).load(sto.getValueString("image")).into(userimage);

        Log.e("name",model.display_name);
        Log.e("count",model.mutual_count);

        ((TextView)v.findViewById(R.id.friends_userName)).setText(model.display_name);
        ((TextView)v.findViewById(R.id.friends_mutualFriends)).setText(model.mutual_count);

        View userProfile = v.findViewById(R.id.profile_userProfile);
        View addFriend = v.findViewById(R.id.profile_addFriend);
        View acceptFriend = v.findViewById(R.id.profile_acceptFriend);
        View denyFriend = v.findViewById(R.id.profile_denyFriend);
        View accept_deny = v.findViewById(R.id.profile_acceptDeny_container);
        View cancelFriend = v.findViewById(R.id.profile_cancelFriend);

        addFriend.setVisibility(View.GONE);
        acceptFriend.setVisibility(View.GONE);
        denyFriend.setVisibility(View.GONE);
        cancelFriend.setVisibility(View.GONE);
        accept_deny.setVisibility(View.GONE);
        if(model.friend_status.equalsIgnoreCase("AD")){
            accept_deny.setVisibility(View.VISIBLE);

        }
        if(model.friend_status.equalsIgnoreCase("N")){
            addFriend.setVisibility(View.VISIBLE);
        }
        if(model.friend_status.equalsIgnoreCase("C")){
            cancelFriend.setVisibility(View.VISIBLE);
        }

        userProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UserProfileActivity.class);
                Gson gson = new Gson();
                intent.putExtra("user_id", model._id );
                intent.putExtra("user_name",model.display_name);
                context.startActivity(intent);
            }
        });

        addFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionFriend("request",model._id, position, v, model.friend_status);
            }
        });

        acceptFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionFriend("accept",model._id, position, v, model.friend_status);
            }
        });
        denyFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionFriend("deny",model._id, position, v, model.friend_status);
            }
        });
        cancelFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionFriend("cancel",model._id, position, v, model.friend_status);

            }
        });
    }



    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ProfileFriendsViewHolder extends RecyclerView.ViewHolder {

        private View v;
        public ProfileFriendsViewHolder(View itemView) {
            super(itemView);
            this.v = itemView;
        }
    }

    public void actionFriend(final String action_type, int user_id, final int index, View v, final String status) {
        hlp.showLoader("Please wait ...");
        Call<JsonElement> call;
        if(action_type.equalsIgnoreCase("accept")){
            call = userInterface.acceptFriendRequest(MyApplication.token,user_id);
        }else if(action_type.equalsIgnoreCase("deny")){
            call = userInterface.denyFriendRequest(MyApplication.token,user_id);
        }else if(action_type.equalsIgnoreCase("cancel")){
            call = userInterface.cancelFriendRequest(MyApplication.token,user_id);
        }else if(action_type.equalsIgnoreCase("request")){
            call = userInterface.sendFriendRequest(MyApplication.token,user_id);
        }else{
            call = userInterface.cancelFriendRequest(MyApplication.token,user_id);
        }
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                hlp.hideLoader();
                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();
                Log.e("RESPONSE", body);
                try {
                    JSONObject res = new JSONObject(body);
                    if(res.getString("status").equalsIgnoreCase("success")){
                        hlp.showToast(res.getString("message"));
                        String next_status = nextStatus(action_type, status);
                        items.get(index).friend_status = next_status;
                        notifyItemChanged(index);
                    }else{
                        hlp.showAlert("", res.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.hideLoader();
                hlp.noConnection();
            }
        });
    }

    public String nextStatus(String action, String status){
        if(action.equalsIgnoreCase("accept") && status.equalsIgnoreCase("AD")){
            return "AF";
        }
        if(action.equalsIgnoreCase("deny") && status.equalsIgnoreCase("AD")){
            return "N";
        }
        if(action.equalsIgnoreCase("cancel") && status.equalsIgnoreCase("C")){
            return "N";
        }
        if(action.equalsIgnoreCase("request") && status.equalsIgnoreCase("N")){
            return "C";
        }
        return "AF";
    }
}
