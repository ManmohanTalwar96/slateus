package in.evergreen.socialapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;

import Apis.ApiClient;
import Apis.PostInterface;
import Models.AllLikeModel;
import Models.FeedCommentModel;
import in.evergreen.socialapp.MyApplication;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.UserProfileActivity;
import in.evergreen.socialapp.Utility.DateFormatter;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;

public class LikesAdapter extends RecyclerView.Adapter<LikesAdapter.CustomViewHolder> {
    private ArrayList<AllLikeModel> items;
    private Context mContext;
    TLHelper hlp;
    DateFormatter df;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    TLStorage sto ;
    PostInterface postInterface;


    public LikesAdapter(Context context, ArrayList<AllLikeModel> items) {
        hlp = new TLHelper(context);
        this.items = items;
        this.mContext = context;
        sto = new TLStorage(mContext);
        df = new DateFormatter();
        postInterface = ApiClient.getClient().create(PostInterface.class);
    }

    public void updateItems(ArrayList<AllLikeModel> newItems) {
        ArrayList<AllLikeModel> updatedItems = new ArrayList<>();
        updatedItems = newItems;
        items = updatedItems;
        notifyDataSetChanged();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_likes, null);
            return new CustomViewHolder(view);
    }

    public void showUserDetail(String name, String avatar, View v) {
        ImageView userimage = (ImageView) v.findViewById(R.id.userImage);
        TextView txtAvatarText = (TextView) v.findViewById(R.id.txtNameChar);
        if (avatar.isEmpty() == false) {
            userimage.setVisibility(View.VISIBLE);
            txtAvatarText.setVisibility(View.GONE);
            Glide.with(mContext).load(avatar).into(userimage);
        } else {
            userimage.setVisibility(View.GONE);
            txtAvatarText.setVisibility(View.VISIBLE);
            txtAvatarText.setText(hlp.getShortName(name));
        }
    }


    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int index ) {
        final View v = holder.v;
        final AllLikeModel model = items.get(index);
        showUserDetail(model.name,model.thumb, v);
        ((TextView) v.findViewById(R.id.txtName)).setText(hlp.getCapsSentences(model.name));
        ((TextView) v.findViewById(R.id.txtMutual)).setText(model.mutual);
        v.findViewById(R.id.btnShowProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, UserProfileActivity.class);
                intent.putExtra("user_id",model.user_id);
                intent.putExtra("user_name",model.name);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return (null != items ? items.size() : 0);
    }



     class CustomViewHolder extends RecyclerView.ViewHolder{
        public View v;
        public CustomViewHolder(View view) {
            super(view);
            this.v = view;
        }
    }


}
