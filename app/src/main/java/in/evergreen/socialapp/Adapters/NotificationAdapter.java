package in.evergreen.socialapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;

import Apis.ApiClient;
import Apis.PostInterface;
import Apis.UserInterface;
import Models.AllLikeModel;
import Models.NotificationModel;
import in.evergreen.socialapp.MyApplication;
import in.evergreen.socialapp.PostActivity;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.UserProfileActivity;
import in.evergreen.socialapp.Utility.DateFormatter;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.CustomViewHolder> {
    private ArrayList<NotificationModel> items;
    private Context mContext;
    TLHelper hlp;
    DateFormatter df;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    TLStorage sto ;
    PostInterface postInterface;
    UserInterface userInterface;


    public NotificationAdapter(Context context, ArrayList<NotificationModel> items) {
        hlp = new TLHelper(context);
        this.items = items;
        this.mContext = context;
        sto = new TLStorage(mContext);
        df = new DateFormatter();
        postInterface = ApiClient.getClient().create(PostInterface.class);
        userInterface = ApiClient.getClient().create(UserInterface.class);
    }

    public void updateItems(ArrayList<NotificationModel> newItems) {
        ArrayList<NotificationModel> updatedItems = new ArrayList<>();
        updatedItems = newItems;
        items = updatedItems;
        notifyDataSetChanged();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_notification, null);
            return new CustomViewHolder(view);
    }

    public void showUserDetail(String name, String avatar, View v) {
        ImageView userimage = (ImageView) v.findViewById(R.id.userImage);
        TextView txtAvatarText = (TextView) v.findViewById(R.id.txtNameChar);
        if (avatar.isEmpty() == false) {
            userimage.setVisibility(View.VISIBLE);
            txtAvatarText.setVisibility(View.GONE);
            Glide.with(mContext).load(avatar).into(userimage);
        } else {
            userimage.setVisibility(View.GONE);
            txtAvatarText.setVisibility(View.VISIBLE);
            txtAvatarText.setText(hlp.getShortName(name));
        }
    }


    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int index ) {
        final View v = holder.v;
        final NotificationModel model = items.get(index);
        showUserDetail(model.name,model.thumb, v);
        ((TextView) v.findViewById(R.id.txtName)).setText(hlp.getCapsSentences(model.name));
        v.findViewById(R.id.acceptLayout).setVisibility(View.GONE);

        try {
            ((TextView) v.findViewById(R.id.txtDate)).setText(hlp.formatToYesterdayOrToday2(model.added_on));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ((TextView) v.findViewById(R.id.txtMessage)).setText(model.message);
        v.findViewById(R.id.btnAction).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(model.type.equalsIgnoreCase("friend_request")){
                    Intent intent = new Intent(mContext, UserProfileActivity.class);
                    intent.putExtra("user_id",model.user_id);
                    intent.putExtra("user_name",model.name);
                    mContext.startActivity(intent);
                }else{
                    if(model.post_id>0){
                        Intent intent = new Intent(mContext, PostActivity.class);
                        intent.putExtra("post_id",model.post_id);
                        mContext.startActivity(intent);
                    }else{
                        Intent intent = new Intent(mContext, UserProfileActivity.class);
                        intent.putExtra("user_id",model.user_id);
                        intent.putExtra("user_name",model.name);
                        mContext.startActivity(intent);
                    }
                }
            }
        });
        if(model.type.equalsIgnoreCase("friend_request")){
            v.findViewById(R.id.acceptLayout).setVisibility(View.VISIBLE);
            v.findViewById(R.id.acceptFriend).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionFriend("accept",model.user_id,index);
                }
            });
            v.findViewById(R.id.denyFriend).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionFriend("deny",model.user_id,index);
                }
            });
        }
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return (null != items ? items.size() : 0);
    }



     class CustomViewHolder extends RecyclerView.ViewHolder{
        public View v;
        public CustomViewHolder(View view) {
            super(view);
            this.v = view;
        }
    }

    public void actionFriend(final String action_type, int user_id, final int index) {
        hlp.showLoader("Please wait ...");
        Call<JsonElement> call;
        if(action_type.equalsIgnoreCase("accept")){
            call = userInterface.acceptFriendRequest(MyApplication.token,user_id);
        }else{
            call = userInterface.denyFriendRequest(MyApplication.token,user_id);
        }
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                hlp.hideLoader();
                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();
                Log.e("RESPONSE", body);
                try {
                    JSONObject res = new JSONObject(body);
                    if(res.getString("status").equalsIgnoreCase("success")){
                        hlp.showToast(res.getString("message"));
                        items.remove(index);
                        notifyItemRemoved(index);
                    }else{
                        hlp.showAlert("", res.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.hideLoader();
                hlp.noConnection();
            }
        });
    }

}
