package in.evergreen.socialapp.Adapters;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;


import in.evergreen.socialapp.AddPostActivity;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.TLHelper;

/**
 * Created by sheetal on 22-03-2017.
 */

public class AddPostAdapter extends RecyclerView.Adapter<AddPostAdapter.CustomViewHolder> {
    private ArrayList<AddPostActivity.MediaPath> items;
    private Context mContext;
    TLHelper hlp;
    private  DataSource.Factory manifestDataSourceFactory;
    private  DataSource.Factory mediaDataSourceFactory;
    ExoPlayer player;


    public AddPostAdapter(Context context, ArrayList<AddPostActivity.MediaPath> items) {
        hlp = new TLHelper(context);
        this.items = items;
        this.mContext = context;
        mediaDataSourceFactory = new DefaultDataSourceFactory(
                mContext,
                Util.getUserAgent(mContext, mContext.getString(R.string.app_name)),
                new DefaultBandwidthMeter());
        manifestDataSourceFactory =
                new DefaultDataSourceFactory(
                        mContext, Util.getUserAgent(mContext, mContext.getString(R.string.app_name)));

    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_add_post_media, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int i) {
        final AddPostActivity.MediaPath model = items.get(i);
        final View v = customViewHolder.v;
        if(model.type.equalsIgnoreCase("image")){
            v.findViewById(R.id.image_view).setVisibility(View.VISIBLE);
            v.findViewById(R.id.audio_view).setVisibility(View.GONE);
            v.findViewById(R.id.video_view).setVisibility(View.GONE);
            ImageView iv = (ImageView) v.findViewById(R.id.image_view);
            Glide.with(mContext).load(model.path_thumb).into(iv);
        }
        if(model.type.equalsIgnoreCase("video")){
            v.findViewById(R.id.image_view).setVisibility(View.GONE);
            v.findViewById(R.id.audio_view).setVisibility(View.GONE);
            v.findViewById(R.id.video_view).setVisibility(View.VISIBLE);
            boolean playWhenReady = false;
            PlayerView playerView = v.findViewById(R.id.video_view);
            Uri uri = Uri.parse(model.path);
            MediaSource mediaSource = buildMediaSource(uri);
             player = ExoPlayerFactory.newSimpleInstance(
                    new DefaultRenderersFactory(mContext),
                    new DefaultTrackSelector(), new DefaultLoadControl());
            playerView.setPlayer(player);
            player.setPlayWhenReady(playWhenReady);
            player.prepare(mediaSource, true, false);

        }
        if(model.type.equalsIgnoreCase("audio")){
            v.findViewById(R.id.image_view).setVisibility(View.GONE);
            v.findViewById(R.id.video_view).setVisibility(View.GONE);
            v.findViewById(R.id.audio_view).setVisibility(View.VISIBLE);
            boolean playWhenReady = false;
            PlayerView playerView = v.findViewById(R.id.audio_view);
            playerView.showController();
            Uri uri = Uri.parse(model.path);
            MediaSource mediaSource = buildMediaSource(uri);
            player = ExoPlayerFactory.newSimpleInstance(
                    new DefaultRenderersFactory(mContext),
                    new DefaultTrackSelector(), new DefaultLoadControl());
            playerView.setPlayer(player);
            player.setPlayWhenReady(playWhenReady);
            player.prepare(mediaSource, true, false);
        }

        View btnDelete = v.findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((AddPostActivity) mContext).deleteMedia(model);
            }
        });
    }

    public void releasePlayer() {
        if (player != null) {
//            playbackPosition = player.getCurrentPosition();
//            currentWindow = player.getCurrentWindowIndex();
            player.release();
            player = null;
        }
    }


    private MediaSource buildMediaSource(Uri uri) {
        @C.ContentType int type = Util.inferContentType(uri);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(
                        new DefaultDashChunkSource.Factory(mediaDataSourceFactory),
                        manifestDataSourceFactory)
                        .createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(
                        new DefaultSsChunkSource.Factory(mediaDataSourceFactory), manifestDataSourceFactory)
                        .createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }




    @Override
    public int getItemCount() {
        return (null != items ? items.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder{
        private View v;
        public CustomViewHolder(View view) {
            super(view);
            this.v = view;
        }
    }



}
