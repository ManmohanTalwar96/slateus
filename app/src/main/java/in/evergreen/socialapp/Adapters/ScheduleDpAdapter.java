package in.evergreen.socialapp.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;


import Models.ScheduleDpModel;
import in.evergreen.socialapp.DashboardActivity;
import in.evergreen.socialapp.GalleryViewActivity;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.TLHelper;


public class ScheduleDpAdapter extends BaseAdapter {
    ArrayList<ScheduleDpModel> items = new ArrayList<>();
    Context mContext;
    int height=0;
    public ScheduleDpAdapter(Context c, ArrayList<ScheduleDpModel> gs, int display_width){
        items=gs;
        mContext=c;
        float newFloat = (float) display_width;
        float width = dpFromPx(c,newFloat);
        width=width/2-16;
        float ht=width*6/5f;
        height=(int) pxFromDp(c,ht);
        height=(int) pxFromDp(c,width);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {

        final ScheduleDpModel g = items.get(position);
        View row = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.list_dp_schedule, parent, false);
        }
        row.findViewById(R.id.container).getLayoutParams().height = height;
        ImageView imageView = (ImageView) row.findViewById(R.id.imageView);
        Glide.with(mContext).load(g.media.path).into(imageView);

        ((TextView) row.findViewById(R.id.txtSchedule)).setText(g.schedule_at);

        View delete = row.findViewById(R.id.btnDelete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
                alertDialog.setMessage("Are you sure to delete this DP?");
                alertDialog.setPositiveButton("Delete",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                try {


                                    final TLHelper hlp=new TLHelper(mContext);
                                    hlp.showLoader("Please wait...");
//                                    HashMap<String, String> req_data=hlp.getDevice();
//                                    req_data.put("id",g.schedule_id);

                                }catch (Exception e){
                                    Toast.makeText(mContext,"Unable to delete", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                // on pressing cancel button
                alertDialog.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                // Showing Alert Message
                alertDialog.show();
            }
        });

        imageView.setClickable(true);

        imageView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {


                Intent intent = new Intent(mContext, GalleryViewActivity.class);
                intent.putExtra("post_index",0);
                intent.putExtra("position",position);
                intent.putExtra("source","dbschedule");
                mContext.startActivity(intent);


            }});

        return row;
    }

    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }
}
