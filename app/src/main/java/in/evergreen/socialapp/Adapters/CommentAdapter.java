package in.evergreen.socialapp.Adapters;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.ArrayList;

import Apis.ApiClient;
import Apis.PostInterface;
import Models.FeedCommentModel;
import Models.FeedLikeModel;
import Models.FeedsModel;
import in.evergreen.socialapp.CommentsActivity;
import in.evergreen.socialapp.DashboardActivity;
import in.evergreen.socialapp.MyApplication;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.UserProfileActivity;
import in.evergreen.socialapp.Utility.DateFormatter;
import in.evergreen.socialapp.Utility.PostMediaViewer;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CustomViewHolder> {
    private ArrayList<FeedCommentModel> items;
    private Context mContext;
    TLHelper hlp;
    DateFormatter df;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    TLStorage sto ;
    PostInterface postInterface;


    public CommentAdapter(Context context, ArrayList<FeedCommentModel> items) {
        hlp = new TLHelper(context);
        this.items = items;
        this.mContext = context;
        sto = new TLStorage(mContext);
        df = new DateFormatter();
        postInterface = ApiClient.getClient().create(PostInterface.class);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_comment, null);
            return new CustomViewHolder(view);
    }

    public void showUserDetail(String name, String avatar, View v) {
        TextView username = (TextView) v.findViewById(R.id.txtUserName);
        username.setText(name);
        ImageView userimage = (ImageView) v.findViewById(R.id.userImage);
        TextView txtAvatarText = (TextView) v.findViewById(R.id.txtNameChar);
        if (avatar.isEmpty() == false) {
            userimage.setVisibility(View.VISIBLE);
            txtAvatarText.setVisibility(View.GONE);
            Glide.with(mContext).load(avatar).into(userimage);
        } else {
            userimage.setVisibility(View.GONE);
            txtAvatarText.setVisibility(View.VISIBLE);
            txtAvatarText.setText(hlp.getShortName(name));
        }
    }

    public void showUserImage(String name, String avatar, View v) {
        ImageView userimage = (ImageView) v.findViewById(R.id.userImage);
        TextView txtAvatarText = (TextView) v.findViewById(R.id.txtNameChar);
        if (avatar.isEmpty() == false) {
            userimage.setVisibility(View.VISIBLE);
            txtAvatarText.setVisibility(View.GONE);
            Glide.with(mContext).load(avatar).into(userimage);
        } else {
            userimage.setVisibility(View.GONE);
            txtAvatarText.setVisibility(View.VISIBLE);
            txtAvatarText.setText(hlp.getShortName(name));
        }
    }
    public void showSelfImage(View v) {
        ImageView userimage = (ImageView) v.findViewById(R.id.userImageComment);
        TextView txtAvatarText = (TextView) v.findViewById(R.id.txtNameCharComment);
        if (MyApplication.image.isEmpty() == false) {
            userimage.setVisibility(View.VISIBLE);
            txtAvatarText.setVisibility(View.GONE);
            Glide.with(mContext).load(MyApplication.image).into(userimage);
        } else {
            userimage.setVisibility(View.GONE);
            txtAvatarText.setVisibility(View.VISIBLE);
            txtAvatarText.setText(hlp.getShortName(MyApplication.display_name));
        }
    }



    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int index ) {

            final View v = holder.v;
            final FeedCommentModel model = items.get(index);
            showUserDetail(model.name,model.user_image_thumb, v);

            v.findViewById(R.id.txtUserName).setClickable(true);
            v.findViewById(R.id.txtUserName).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in=new Intent(mContext,UserProfileActivity.class);
                    in.putExtra("user_id",model.user_id);
                    in.putExtra("user_name",model.name);
                    mContext.startActivity(in);
                }
            });


            try {
                String added_on = df.convertDate(model.added_on);
                ((TextView)v.findViewById(R.id.txtCommentDate)).setText(added_on);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            ((TextView) v.findViewById(R.id.txtComment)).setText(model.comment);
        v.findViewById(R.id.btnAction).setVisibility(View.GONE);
        if(model.allow_delete==1){
            v.findViewById(R.id.btnAction).setVisibility(View.VISIBLE);
            v.findViewById(R.id.btnAction).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(mContext, v.findViewById(R.id.btnAction));
                    popup.inflate(R.menu.menu_comment);
                    Menu m=popup.getMenu();
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            if(item.getItemId()==R.id.deletecomment){
                                deleteComment(model._id,index);
                            }
                            if(item.getItemId()==R.id.editcomment){
                                editComment(model._id, index, model.comment);
                            }
                            return false;
                        }
                    });

                    try{
                        Method me = m.getClass().getDeclaredMethod(
                                "setOptionalIconsVisible", Boolean.TYPE);
                        me.setAccessible(true);
                        me.invoke(m, true);
                    }
                    catch(NoSuchMethodException e){

                    }
                    catch(Exception e){
                        throw new RuntimeException(e);
                    }
                    popup.show();
                }
            });
        }

    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return (null != items ? items.size() : 0);
    }



     class CustomViewHolder extends RecyclerView.ViewHolder{
        public View v;
        public CustomViewHolder(View view) {
            super(view);
            this.v = view;
        }
    }

    public void deleteComment(final int id, final int index){
        hlp.confirmDialog("Delete Comment", "Are you sure to delete this comment?", "Delete", new TLHelper.MyCallback() {
            @Override
            public void callbackCall() {
                Call<JsonElement>  call = postInterface.deleteComment(MyApplication.token,id);
                call.enqueue(new Callback<JsonElement>() {

                    @Override
                    public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                        JsonElement jsonElement = response.body();
                        String body = jsonElement.toString();
                        Log.e("RESPONSE", body);
                        try {
                            JSONObject res = new JSONObject(body);
                            if(res.getString("status").equalsIgnoreCase("success")){
                                items.remove(index);
                                notifyItemRemoved(index);
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        hlp.noConnection();
                    }
                });
            }
        });
    }

    public void editComment(final int _id, final int index, String comment){
        LayoutInflater li = LayoutInflater.from(mContext);
        View promptsView = li.inflate(R.layout.prompt_edit, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle("Edit Comment");
        alertDialogBuilder.setView(promptsView);
        final EditText textbox =(EditText) promptsView.findViewById(R.id.textbox);
        textbox.setText(comment);
        textbox.setHint("Write your comment");
        alertDialogBuilder
                .setCancelable(true)
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                String txtData=textbox.getText().toString();
                                if(txtData.isEmpty()==false){
                                    updateComment(txtData,_id, index);
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void updateComment(final String txtData, int _id, final int index){

        JSONObject jo = new JSONObject();
        try {
            jo.put("comment", txtData);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = (JsonObject) jsonParser.parse(jo.toString());
        Call<JsonElement>  call = postInterface.updateComment(MyApplication.token,_id,jsonObject);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();
                Log.e("RESPONSE", body);
                try {
                    JSONObject res = new JSONObject(body);
                    if(res.getString("status").equalsIgnoreCase("success")){
                        items.get(index).comment = txtData;
                        notifyItemChanged(index);
                    }else{

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.noConnection();
            }
        });
    }

}
