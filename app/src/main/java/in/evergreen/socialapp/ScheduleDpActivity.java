package in.evergreen.socialapp;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Models.MediaModel;
import Models.ScheduleDpModel;
import in.evergreen.socialapp.Adapters.ScheduleDpAdapter;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;

public class ScheduleDpActivity extends AppCompatActivity {

    private FloatingActionButton add;
    public Context mContext;
    private TLStorage sto;
    private TLHelper hlp;
    public static ArrayList<ScheduleDpModel> list;
    private ScheduleDpAdapter adapter;
    public LinearLayout lo_con;
    public TextView noreport;
    public GridView gridView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_dp);

        gridView = (GridView) findViewById(R.id.dp_gridView);
        add = (FloatingActionButton) findViewById(R.id.dp_fab);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ScheduleDpActivity.this, WindowActivityDP.class);
                startActivity(i);
            }
        });

        mContext = getApplicationContext();
        sto = new TLStorage(mContext);
        hlp = new TLHelper(mContext);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

//        addImage();

        list = new ArrayList<ScheduleDpModel>();
        adapter = new ScheduleDpAdapter(ScheduleDpActivity.this, list, width);
        gridView.setAdapter(adapter);

        findViewById(R.id.serverDataReload).setVisibility(View.GONE);

        noreport = (TextView) findViewById(R.id.report_noreport);
        noreport.setText("No DP Scheduled");
        lo_con = (LinearLayout) findViewById(R.id.serverDataReload);
        lo_con.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                reloadContent();
            }
        });

        if (savedInstanceState == null) {
            LoadReport("new");
        } else {
            list = savedInstanceState.getParcelableArrayList("listdata");
            if (list.size() == 0) {
                LoadReport("new");
            }
        }
    }

//    public void addImage(){
//
//        Bundle b = getIntent().getExtras();
//        String schedule_date = b.getString("schedule_at");
//
//        ScheduleDpModel sch=new ScheduleDpModel(schedule_date, b.getString("picture"));
//        list.add(sch);
//        adapter.notifyDataSetChanged();
//    }

    @Override
    public void onSaveInstanceState(Bundle savedState) {
        super.onSaveInstanceState(savedState);
        savedState.putParcelableArrayList("listdata", list);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void LoadReport(final String ltype){

        lo_con.setVisibility(View.GONE);
        if(ltype.equalsIgnoreCase("new")){
            hlp.showLoader("Please wait...");
        }

        // Call API

    }
    public void createListView(JSONArray sarr, String ltype){
        list.clear();
        gridView.setVisibility(View.VISIBLE);
        Log.e("ARRAY SIZE",sarr.length()+"-aa");
        for (int i=0; i<sarr.length(); i++) {
            JSONObject item;
            try {
                item = sarr.getJSONObject(i);
                list.add(new ScheduleDpModel(item));  //Calling the Class to Assign Values
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        Log.e("AS",list.size()+ltype);

        adapter.notifyDataSetChanged();
        if(list.size() == 0){
            noreport.setVisibility(View.VISIBLE);

            gridView.setVisibility(View.GONE);
        }else {
            noreport.setVisibility(View.GONE);
            gridView.setVisibility(View.VISIBLE);
        }
    }
    public void reloadContent(){
        findViewById(R.id.serverDataReload).setVisibility(View.GONE);
        findViewById(R.id.serverDataContainer).setVisibility(View.VISIBLE);
        LoadReport("new");
    }

    public void showReload(){
        findViewById(R.id.serverDataContainer).setVisibility(View.GONE);
        findViewById(R.id.serverDataReload).setVisibility(View.VISIBLE);
    }


    public void closeActivity(View view) {
    }

    public static ArrayList<MediaModel> getPostMedia(int post_index){
        ArrayList<MediaModel> mm = new ArrayList<MediaModel>();
        for(int i=0; i<list.size(); i++){
            mm.add(list.get(i).media);
        }
        return mm;
    }
}

