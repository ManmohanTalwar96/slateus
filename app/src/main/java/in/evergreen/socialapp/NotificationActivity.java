package in.evergreen.socialapp;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Apis.ApiClient;
import Apis.NotificationInterface;
import Apis.PostInterface;
import Models.FeedsModel;
import Models.NotificationModel;
import cn.jzvd.JZMediaManager;
import cn.jzvd.JZUtils;
import cn.jzvd.JZVideoPlayer;
import cn.jzvd.JZVideoPlayerManager;
import in.evergreen.socialapp.Adapters.FeedsHomeAdapter;
import in.evergreen.socialapp.Adapters.NotificationAdapter;
import in.evergreen.socialapp.Utility.EndlessRecyclerOnScrollListener;
import in.evergreen.socialapp.Utility.TLHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {
    JsonParser jsonParser;
    JsonObject jsonObject;
    TLHelper hlp;
    private NotificationAdapter adapter;
    public static ArrayList<NotificationModel> itemiList=new ArrayList<NotificationModel>();
    private RecyclerView mRecyclerView;
    View  reloader;
    NotificationInterface notificationInterface;
    public TextView noRecord;
    SwipeRefreshLayout swipeRefreshLayout;
    public int page=1;
    public boolean pageend =false;
    boolean is_loading =false;
    EndlessRecyclerOnScrollListener esl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        hlp = new TLHelper(this);
        jsonParser = new JsonParser();
        jsonObject = new JsonObject();
        itemiList.clear();
        noRecord = (TextView) findViewById(R.id.txtNoRecord);
        reloader = findViewById(R.id.buttonReload);
        notificationInterface = ApiClient.getClient().create(NotificationInterface.class);
        reloader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchRecords();
            }
        });
        esl = new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                Log.e("SCROLL ", pageend+"==="+is_loading);
                if(pageend==false && is_loading == false){
                    page++;
                    fetchRecords();
                }
            }
        };
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.dark_blue, R.color.dark_grey, R.color.light_grey);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                itemiList.clear();
                page=1;
                pageend = false;
                fetchRecords();
                esl.reset();
            }
        });
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new NotificationAdapter(this, itemiList);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnScrollListener(esl);
        fetchRecords();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }



    public void fetchRecords(){
        noRecord.setVisibility(View.GONE);
        reloader.setVisibility(View.GONE);


        if(page==1)
            swipeRefreshLayout.setRefreshing(true);
        Call<JsonElement> call = notificationInterface.getNotification(MyApplication.token,page,20);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                swipeRefreshLayout.setRefreshing(false);
                if(response.isSuccessful()){
                    JsonElement jsonElement = response.body();
                    String body = jsonElement.toString();
                    Log.e("RESPONSE", body);
                    try {
                        JSONObject res = new JSONObject(body);
                        JSONArray data = res.getJSONArray("data");
                        createListing(data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    hlp.showError(response.code());
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                reloader.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void createListing(JSONArray data) throws JSONException{
        if(page==1)
            itemiList.clear();
        for (int i=0; i<data.length(); i++) {
            JSONObject item;
            try {
                item = data.getJSONObject(i);
                itemiList.add(new NotificationModel(item));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        adapter.updateItems(itemiList);
        if(itemiList.size()==0 && page==1){
            noRecord.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        JZVideoPlayer.releaseAllVideos();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
