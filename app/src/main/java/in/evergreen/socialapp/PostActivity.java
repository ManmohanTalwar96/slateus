package in.evergreen.socialapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Apis.ApiClient;
import Apis.PostInterface;
import Models.FeedsModel;
import cn.jzvd.JZMediaManager;
import cn.jzvd.JZUtils;
import cn.jzvd.JZVideoPlayer;
import cn.jzvd.JZVideoPlayerManager;
import in.evergreen.socialapp.Adapters.FeedsHomeAdapter;
import in.evergreen.socialapp.Utility.TLHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostActivity extends AppCompatActivity {
    int post_id=1;
    JsonParser jsonParser;
    JsonObject jsonObject;
    TLHelper hlp;
    private FeedsHomeAdapter adapter;
    public static ArrayList<FeedsModel> itemiList=new ArrayList<FeedsModel>();
    private RecyclerView mRecyclerView;
    View reloader;
    PostInterface postInterface;
    public TextView noRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        post_id = getIntent().getExtras().getInt("post_id");
        hlp = new TLHelper(this);
        jsonParser = new JsonParser();
        jsonObject = new JsonObject();
        noRecord = (TextView) findViewById(R.id.txtNoRecord);
        reloader = findViewById(R.id.buttonReload);
        postInterface = ApiClient.getClient().create(PostInterface.class);
        reloader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchRecords();
            }
        });
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FeedsHomeAdapter(this, itemiList);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(View view) {

            }

            @Override
            public void onChildViewDetachedFromWindow(View view) {
                JZVideoPlayer jzvd = view.findViewById(R.id.videoplayer);
                if (jzvd != null && JZUtils.dataSourceObjectsContainsUri(jzvd.dataSourceObjects, JZMediaManager.getCurrentDataSource())) {
                    JZVideoPlayer currentJzvd = JZVideoPlayerManager.getCurrentJzvd();
                    if (currentJzvd != null && currentJzvd.currentScreen != JZVideoPlayer.SCREEN_WINDOW_FULLSCREEN) {
                        JZVideoPlayer.releaseAllVideos();
                    }
                }
            }
        });
        fetchRecords();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }



    public void fetchRecords(){
        noRecord.setVisibility(View.GONE);
        reloader.setVisibility(View.GONE);



        Call<JsonElement> call = postInterface.getSinglePost(MyApplication.token,post_id);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {

                if(response.isSuccessful()){
                    JsonElement jsonElement = response.body();
                    String body = jsonElement.toString();
                    Log.e("RESPONSE", body);
                    try {
                        JSONObject res = new JSONObject(body);
                        JSONArray data = res.getJSONArray("data");
                        createListing(data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    hlp.showError(response.code());
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                reloader.setVisibility(View.VISIBLE);
            }
        });
    }

    public void createListing(JSONArray data) throws JSONException{
        itemiList.clear();
        for (int i=0; i<data.length(); i++) {
            JSONObject item;
            try {
                item = data.getJSONObject(i);
                itemiList.add(new FeedsModel(item));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        adapter.updateItems(itemiList);
        if(itemiList.size()==0){
            noRecord.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        JZVideoPlayer.releaseAllVideos();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
