package in.evergreen.socialapp;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFrame;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import io.github.sac.Ack;
import io.github.sac.BasicListener;
import io.github.sac.Emitter;
import io.github.sac.ReconnectStrategy;
import io.github.sac.Socket;

public class SocketTestActivity extends AppCompatActivity {

    Socket socket;
    Socket.Channel channel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_socket_test);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initSocket();
    }

    public void initSocket(){
        String url="ws://192.168.1.100:8000/socketcluster/";
        socket = new Socket(url);
        socket.setListener(new BasicListener() {

            public void onConnected(Socket socket,Map<String, List<String>> headers) {
                System.out.println("Connected to endpoint");
                subscribeChannel();
            }

            public void onDisconnected(Socket socket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) {
                System.out.println("Disconnected from end-point");
            }

            public void onConnectError(Socket socket,WebSocketException exception) {
                System.out.println("Got connect error "+ exception);
            }

            public void onSetAuthToken(String token, Socket socket) {
                System.out.println("Token is "+ token);
            }

            public void onAuthentication(Socket socket,Boolean status) {
                if (status) {
                    System.out.println("socket is authenticated");
                } else {
                    System.out.println("Authentication is required (optional)");
                }
            }
        });

        socket.on("msg_received", new Emitter.Listener() {
            public void call(String eventName,Object object) {

                // Cast object to its proper datatype
                System.out.println("Got message for :"+eventName+" data is :"+object.toString());
            }
        });
    }

    public void subscribeChannel(){
        channel = socket.createChannel("user_999");
        channel.subscribe(new Ack() {
            public void call(String channelName, Object error, Object data) {
                if (error == null) {
                    System.out.println("Subscribed to channel "+channelName+" successfully");
                }
            }
        });

        channel.onMessage(new Emitter.Listener() {
            public void call(String channelName , Object object) {

                System.out.println("Got message for channel "+channelName+" data is "+object.toString());

            }
        });
    }



    public void connectSocket(View v){
//This will send websocket handshake request to socketcluster-server
        //socket.connect();

        //This will set automatic-reconnection to server with delay of 2 seconds and repeating it for 30 times
        socket.setReconnection(new ReconnectStrategy().setDelay(2000).setMaxAttempts(30));
        socket.connect();
        //socket.disableLogging();

    }

    public void disconnectSocket(View v){

    }

    public void sendMessage(View v) {
//        socket.emit(eventname, message, new Ack() {
//            public void call(String eventName,Object error, Object data) {
//                //If error and data is String
//                System.out.println("Got message for :"+eventName+" error is :"+error+" data is :"+data);
//            }
//        });

        JSONObject jo=new JSONObject();
        try {
            jo.put("type","message");
            jo.put("to","user_1");
            jo.put("message","Hello Worls");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        channel.publish( jo, new Ack() {
            public void call(String channelName,Object error, Object data) {
                if (error == null) {
                    System.out.println("Published message to channel "+channelName+" successfully");
                }
            }
        });


    }

    public void sendAck(){
//        socket.emit(eventname, message, new Ack() {
//            public void call(String eventName,Object error, Object data) {
//                //If error and data is String
//                System.out.println("Got message for :"+eventName+" error is :"+error+" data is :"+data);
//            }
//        });
    }

    public void sendToSocket(View v){
        socket.emit("chat","Hi");

        socket.emit("chat", "Hi", new Ack() {
            @Override
            public void call(String name, Object error, Object data) {
                System.out.println("Event ::"+name+" Error ::"+error+" data ::"+data);
            }
        });

    }





}
