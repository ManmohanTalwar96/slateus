package in.evergreen.socialapp.Utility;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import in.evergreen.socialapp.R;

/**
 * Created by mgod on 5/27/15.
 *
 * Simple custom view example to show how to get selected events from the token
 * view. See ContactsCompletionView and contact_token.xml for usage
 */
public class ACTokenTextView extends android.support.v7.widget.AppCompatTextView {

    public ACTokenTextView(Context context) {
        super(context);
    }

    public ACTokenTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        setCompoundDrawablesWithIntrinsicBounds(0, 0, selected ? R.drawable.ic_close_12dp : 0, 0);
    }
}
