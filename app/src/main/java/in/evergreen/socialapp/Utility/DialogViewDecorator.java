package in.evergreen.socialapp.Utility;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.annotation.ColorRes;
import android.view.View;

public class DialogViewDecorator {

    private static final
    @ColorRes
    int DEFAULT_TITLE_DIVIDER_COLOR = android.R.color.holo_orange_light;

    public static void decorate(Dialog dialog) {
        decorate(dialog, DEFAULT_TITLE_DIVIDER_COLOR);
    }

    /**
     * Sets the title divider color when the view is shown by setting DialogInterface.OnShowListener on the dialog.
     * <p/>
     * If you want to do other things onShow be sure to extend OnDecoratedDialogShownListener(call super.show(...)!)
     * and call {@link #decorate(Dialog, int, OnDecoratedDialogShownListener)}.
     *
     * @param dialog
     * @param titleDividerColor
     */
    public static void decorate(Dialog dialog, final int titleDividerColor) {
        decorate(dialog, titleDividerColor, new OnDecoratedDialogShownListener(titleDividerColor));
    }

    /**
     * Method for setting a extended implementation of OnDecoratedDialogShownListener. Don't forget to call super
     * or the titleDividerColor wont be applied!
     *
     * @param dialog
     * @param titleDividerColor
     * @param OnShowListener
     * @param <T>
     */
    public static <T extends OnDecoratedDialogShownListener> void decorate(Dialog dialog, int titleDividerColor, T OnShowListener) {
        if (dialog == null || titleDividerColor <= 0) { return; }

        if (dialog.isShowing()) {
            setTitleDividerColor(dialog, titleDividerColor);
        } else {
            dialog.setOnShowListener(OnShowListener);
        }
    }

    private static void setTitleDividerColor(DialogInterface dialogInterface, int titleDividerColor) {
        try {
            Dialog dialog = (Dialog) dialogInterface;
            int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
            View divider = dialog.findViewById(dividerId);
            if (divider != null) {
                divider.setBackgroundColor(dialog.getContext().getResources().getColor(titleDividerColor));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static class OnDecoratedDialogShownListener implements DialogInterface.OnShowListener {
        private int titleDividerColor;

        public OnDecoratedDialogShownListener() {
            this.titleDividerColor = DEFAULT_TITLE_DIVIDER_COLOR;
        }

        public OnDecoratedDialogShownListener(int titleDividerColor) {
            this.titleDividerColor = titleDividerColor;
        }

        @Override
        public void onShow(DialogInterface dialogInterface) {
            setTitleDividerColor(dialogInterface, titleDividerColor);
        }
    }}