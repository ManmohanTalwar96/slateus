package in.evergreen.socialapp.Utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Sheetal on 6/24/18.
 */

public class DateFormatter {
    public String convertDate(String dateCreated) throws ParseException {

        dateCreated = dateCreated.replace("T"," ");
            Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(dateCreated);
            DateFormat dateFormatNeeded = new SimpleDateFormat("dd MMM, yyyy");
            DateFormat dateFormatNeeded2 = new SimpleDateFormat("hh:mm a");
            String crdate1 = dateFormatNeeded.format(date)+" at "+dateFormatNeeded2.format(date);

            // Date Calculation
            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");


            // get current date time with Calendar()
            Calendar cal = Calendar.getInstance();
            String currenttime = dateFormat.format(cal.getTime());

            Date CreatedAt = date;
            Date current = null;
            try {
                current = dateFormat.parse(currenttime);
            } catch (java.text.ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            // Get msec from each, and subtract.
            long diff = current.getTime() - CreatedAt.getTime();
            long diffSeconds = diff / 1000;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            String time = null;
            if (diffDays > 0) {
//                if (diffDays == 1) {
//                    time = diffDays + "day ago ";
//                } else {
//                    time = diffDays + "days ago ";
//                }
                time = crdate1;
            } else {
                if (diffHours > 0) {
                    if (diffHours == 1) {
                        time = diffHours + " hour";
                    } else {
                        time = diffHours + " hours";
                    }
                } else {
                    if (diffMinutes > 0) {
                        if (diffMinutes == 1) {
                            time = diffMinutes + "min ago";
                        } else {
                            time = diffMinutes + "mins ago";
                        }
                    } else {
                        if (diffSeconds > 0) {
                            time = diffSeconds + "secs ago";
                        }
                    }

                }

            }
            return time;
        }
  
}
