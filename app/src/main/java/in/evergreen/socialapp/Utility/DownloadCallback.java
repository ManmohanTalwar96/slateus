package in.evergreen.socialapp.Utility;

public interface DownloadCallback {
    void downloadComplete(String filepath);
    void downloadStart();
    void downloadFailed();
    void downloadProgress(int progress);
}