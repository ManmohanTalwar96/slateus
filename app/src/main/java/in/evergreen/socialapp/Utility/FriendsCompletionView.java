package in.evergreen.socialapp.Utility;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tokenautocomplete.TokenCompleteTextView;

import in.evergreen.socialapp.R;

/**
 * Sample token completion view for basic contact info
 *
 * Created on 10/06/17.
 * @author Sheetal Kumar
 */
public class FriendsCompletionView extends TokenCompleteTextView<Person> {

    public FriendsCompletionView(Context context) {
        super(context);
    }

    public FriendsCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FriendsCompletionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected View getViewForObject(Person person) {
        LayoutInflater l = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        ACTokenTextView token = (ACTokenTextView) l.inflate(R.layout.list_friend_token, (ViewGroup) getParent(), false);
        token.setText(person.getName()+"  ");
        return token;
    }

    @Override
    protected Person defaultObject(String completionText) {
        //Stupid simple example of guessing if we have an email or not
        int index = completionText.indexOf('@');
        if (index == -1) {
            return new Person(completionText, completionText.replace(" ", "") + "@example.com","");
        } else {
            return new Person(completionText.substring(0, index), completionText,"");
        }
    }
}
