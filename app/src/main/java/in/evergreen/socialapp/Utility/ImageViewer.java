package in.evergreen.socialapp.Utility;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.yanzhenjie.album.AlbumFile;

import java.util.ArrayList;

import in.evergreen.socialapp.AddPostActivity;
import in.evergreen.socialapp.R;

/**
 * Created by Sheetal on 6/16/18.
 */

public class ImageViewer {
    ArrayList<AddPostActivity.MediaPath> checkedImage=new ArrayList<>();
    ViewGroup v;
    Context mContext;

    public ImageViewer(Context mContext,  ViewGroup v){

        this.mContext=mContext;
        this.v = v;
    }

    public void showImages(ArrayList<AddPostActivity.MediaPath> checkedImage){
        this.checkedImage = checkedImage;
        v.removeAllViews();
        if(this.checkedImage.size()==1){
            createSingleImage();
        }
        if(this.checkedImage.size()==2){
            createDoubleImage();
        }
        if(this.checkedImage.size()==3){
            createTripleImage();
        }
        if(this.checkedImage.size()==4){
            createFourImage();
        }
        if(this.checkedImage.size()==5){
            createFiveImage(false);
        }
        if(this.checkedImage.size()>5){
            createFiveImage(true);
        }
    }

    public void createSingleImage(){
        LayoutInflater li = LayoutInflater.from(mContext);
        View view = li.inflate(R.layout.image_single, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
        Glide.with(mContext).load(checkedImage.get(0).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv);
        Log.e("PATH 1",checkedImage.get(0).path_thumb);
        v.addView(view);
    }
    public void createDoubleImage(){
        LayoutInflater li = LayoutInflater.from(mContext);
        View view = li.inflate(R.layout.image_double, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
        Glide.with(mContext).load(checkedImage.get(0).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv);
        ImageView iv2 = (ImageView) view.findViewById(R.id.imageView2);
        Glide.with(mContext).load(checkedImage.get(1).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv2);
        v.addView(view);
    }

    public void createTripleImage(){
        LayoutInflater li = LayoutInflater.from(mContext);
        View view = li.inflate(R.layout.image_triple, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
        Glide.with(mContext).load(checkedImage.get(0).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv);
        ImageView iv2 = (ImageView) view.findViewById(R.id.imageView2);
        Glide.with(mContext).load(checkedImage.get(1).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv2);
        ImageView iv3 = (ImageView) view.findViewById(R.id.imageView3);
        Glide.with(mContext).load(checkedImage.get(2).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv3);
        v.addView(view);
    }

    public void createFourImage(){
        LayoutInflater li = LayoutInflater.from(mContext);
        View view = li.inflate(R.layout.image_four, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
        Glide.with(mContext).load(checkedImage.get(0).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv);
        ImageView iv2 = (ImageView) view.findViewById(R.id.imageView2);
        Glide.with(mContext).load(checkedImage.get(1).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv2);
        ImageView iv3 = (ImageView) view.findViewById(R.id.imageView3);
        Glide.with(mContext).load(checkedImage.get(2).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv3);
        ImageView iv4 = (ImageView) view.findViewById(R.id.imageView4);
        Glide.with(mContext).load(checkedImage.get(3).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv4);
        v.addView(view);
    }

    public void createFiveImage(boolean showNum){
        LayoutInflater li = LayoutInflater.from(mContext);
        View view = li.inflate(R.layout.image_five, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
        Glide.with(mContext).load(checkedImage.get(0).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv);
        ImageView iv2 = (ImageView) view.findViewById(R.id.imageView2);
        Glide.with(mContext).load(checkedImage.get(1).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv2);
        ImageView iv3 = (ImageView) view.findViewById(R.id.imageView3);
        Glide.with(mContext).load(checkedImage.get(2).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv3);
        ImageView iv4 = (ImageView) view.findViewById(R.id.imageView4);
        Glide.with(mContext).load(checkedImage.get(3).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv4);
        ImageView iv5 = (ImageView) view.findViewById(R.id.imageView5);
        Glide.with(mContext).load(checkedImage.get(4).path_thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv5);
        if(showNum==true){
            view.findViewById(R.id.txtNumber).setVisibility(View.VISIBLE);
            ((TextView)view.findViewById(R.id.txtNumber)).setText("+"+(checkedImage.size()-5)+" more");
        }else{
            view.findViewById(R.id.txtNumber).setVisibility(View.GONE);
        }
        v.addView(view);
    }



}
