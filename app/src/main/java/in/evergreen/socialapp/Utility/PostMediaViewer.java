package in.evergreen.socialapp.Utility;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;

import Models.MediaModel;
import cn.jzvd.JZVideoPlayerStandard;
import in.evergreen.socialapp.AddPostActivity;
import in.evergreen.socialapp.DashboardActivity;
import in.evergreen.socialapp.GalleryViewActivity;
import in.evergreen.socialapp.R;


/**
 * Created by Sheetal on 6/16/18.
 */

public class PostMediaViewer {
    ArrayList<MediaModel> checkedImage=new ArrayList<>();
    ViewGroup v;
    Context mContext;
    ExoPlayer player;
    int post_index;
    private  DataSource.Factory manifestDataSourceFactory;
    private  DataSource.Factory mediaDataSourceFactory;


    public PostMediaViewer(Context mContext, ViewGroup v, int post_index){
        this.post_index = post_index;
        this.mContext=mContext;
        this.v = v;
        mediaDataSourceFactory = new DefaultDataSourceFactory(
                mContext,
                Util.getUserAgent(mContext, mContext.getString(R.string.app_name)),
                new DefaultBandwidthMeter());
        manifestDataSourceFactory =
                new DefaultDataSourceFactory(
                        mContext, Util.getUserAgent(mContext, mContext.getString(R.string.app_name)));
    }

    public void showImages(ArrayList<MediaModel> checkedImage){
        Log.e("POST LIST SIZE", checkedImage.size()+"czcz");
        this.checkedImage = checkedImage;
        v.removeAllViews();
        if(this.checkedImage.size()==1){
            createSingleImage();
        }
        if(this.checkedImage.size()==2){
            createDoubleImage();
        }
        if(this.checkedImage.size()==3){
            createTripleImage();
        }
        if(this.checkedImage.size()==4){
            createFourImage();
        }
        if(this.checkedImage.size()==5){
            createFiveImage(false);
        }
        if(this.checkedImage.size()>5){
            createFiveImage(true);
        }
    }

    public void createSingleImage(){
        LayoutInflater li = LayoutInflater.from(mContext);
        View view;

        Log.e("FILE TYPE",checkedImage.get(0).file_type+"--"+checkedImage.get(0)._id);
        if(checkedImage.get(0).file_type.equalsIgnoreCase("image")){
            view = li.inflate(R.layout.image_single, null);
            ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
            Glide.with(mContext).load(checkedImage.get(0).thumb).apply(new RequestOptions().override(500, 500)
                    .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv);
            initClickEvent(iv,1);
        }else if(checkedImage.get(0).file_type.equalsIgnoreCase("audio")){
            view = li.inflate(R.layout.single_audio, null);
        }else if(checkedImage.get(0).file_type.equalsIgnoreCase("video")){
            view = li.inflate(R.layout.single_video, null);
        }else{
            view = new View(mContext);
        }
        if(checkedImage.get(0).file_type.equalsIgnoreCase("audio")){
//            PlayerView playerView = view.findViewById(R.id.video_view);
//            playerView.setShutterBackgroundColor(mContext.getResources().getColor(R.color.white));
//            Uri uri = Uri.parse(checkedImage.get(0).path);
//            MediaSource mediaSource = buildMediaSource(uri);
//            player = ExoPlayerFactory.newSimpleInstance(
//                    new DefaultRenderersFactory(mContext),
//                    new DefaultTrackSelector(), new DefaultLoadControl());
//            playerView.setPlayer(player);
//            player.setPlayWhenReady(false);
//            player.prepare(mediaSource, true, false);

//            ViewGroup mPlayerContainer =  (ViewGroup) view.findViewById(R.id.player_layout);
//
//            AudioPlayer.getInstance().init(mContext, checkedImage.get(0).path).useDefaultUi(mPlayerContainer, li);
//
//            AudioPlayer.getInstance().addOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//
//                @Override
//                public void onCompletion(MediaPlayer mp) {
//                    Toast.makeText(mContext, "Completed", Toast.LENGTH_SHORT).show();
//                    // do you stuff.
//                }
//            });
//
//            AudioPlayer.getInstance().addOnPlayClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    Toast.makeText(mContext, "Play", Toast.LENGTH_SHORT).show();
//                    // get-set-go. Lets dance.
//                }
//            });
//
//            AudioPlayer.getInstance().addOnPauseClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    Toast.makeText(mContext, "Pause", Toast.LENGTH_SHORT).show();
//                    // Your on audio pause stuff.
//                }
//            });




            View btnPlayer = view.findViewById(R.id.btnPlayAudio);
            btnPlayer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, GalleryViewActivity.class);
                    intent.putExtra("post_index",post_index);
                    intent.putExtra("position",1);
                    mContext.startActivity(intent);
                }
            });


        }
        if( checkedImage.get(0).file_type.equalsIgnoreCase("video")){
            final JZVideoPlayerStandard jzVideoPlayerStandard = (JZVideoPlayerStandard)view.findViewById(R.id.videoplayer);
            jzVideoPlayerStandard.setUp(checkedImage.get(0).path,
                    JZVideoPlayerStandard.SCREEN_WINDOW_LIST,
                    "");
          //  Glide.with(mContext).load(VideoConstant.videoThumbs[0][position]).into(holder.jzVideoPlayer.thumbImageView);
           Glide.with(mContext).load(checkedImage.get(0).thumb)
                    .into(jzVideoPlayerStandard.thumbImageView);


        }
        v.addView(view);
    }
    public void createDoubleImage(){
        LayoutInflater li = LayoutInflater.from(mContext);
        View view = li.inflate(R.layout.image_double, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
        Glide.with(mContext).load(checkedImage.get(0).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv);
        ImageView iv2 = (ImageView) view.findViewById(R.id.imageView2);
        Glide.with(mContext).load(checkedImage.get(1).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv2);
        v.addView(view);
        initClickEvent(iv,1);
        initClickEvent(iv2,2);
    }

    public void createTripleImage(){
        LayoutInflater li = LayoutInflater.from(mContext);
        View view = li.inflate(R.layout.image_triple, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
        Glide.with(mContext).load(checkedImage.get(0).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv);
        ImageView iv2 = (ImageView) view.findViewById(R.id.imageView2);
        Glide.with(mContext).load(checkedImage.get(1).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv2);
        ImageView iv3 = (ImageView) view.findViewById(R.id.imageView3);
        Glide.with(mContext).load(checkedImage.get(2).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv3);
        v.addView(view);
        initClickEvent(iv,1);
        initClickEvent(iv2,2);
        initClickEvent(iv3,3);
    }

    public void createFourImage(){
        LayoutInflater li = LayoutInflater.from(mContext);
        View view = li.inflate(R.layout.image_four, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
        Glide.with(mContext).load(checkedImage.get(0).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv);
        ImageView iv2 = (ImageView) view.findViewById(R.id.imageView2);
        Glide.with(mContext).load(checkedImage.get(1).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv2);
        ImageView iv3 = (ImageView) view.findViewById(R.id.imageView3);
        Glide.with(mContext).load(checkedImage.get(2).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv3);
        ImageView iv4 = (ImageView) view.findViewById(R.id.imageView4);
        Glide.with(mContext).load(checkedImage.get(3).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv4);
        v.addView(view);
        initClickEvent(iv,1);
        initClickEvent(iv2,2);
        initClickEvent(iv3,3);
        initClickEvent(iv4,4);
    }

    public void createFiveImage(boolean showNum){
        LayoutInflater li = LayoutInflater.from(mContext);
        View view = li.inflate(R.layout.image_five, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
        Glide.with(mContext).load(checkedImage.get(0).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv);
        ImageView iv2 = (ImageView) view.findViewById(R.id.imageView2);
        Glide.with(mContext).load(checkedImage.get(1).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv2);
        ImageView iv3 = (ImageView) view.findViewById(R.id.imageView3);
        Glide.with(mContext).load(checkedImage.get(2).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv3);
        ImageView iv4 = (ImageView) view.findViewById(R.id.imageView4);
        Glide.with(mContext).load(checkedImage.get(3).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv4);
        ImageView iv5 = (ImageView) view.findViewById(R.id.imageView5);
        Glide.with(mContext).load(checkedImage.get(4).thumb).apply(new RequestOptions().override(500, 500)
                .placeholder(R.drawable.inage_placeholder).error(R.drawable.inage_placeholder)).into(iv5);
        if(showNum==true){
            view.findViewById(R.id.txtNumber).setVisibility(View.VISIBLE);
            ((TextView)view.findViewById(R.id.txtNumber)).setText("+"+(checkedImage.size()-5)+" more");
        }else{
            view.findViewById(R.id.txtNumber).setVisibility(View.GONE);
        }
        initClickEvent(iv,1);
        initClickEvent(iv2,2);
        initClickEvent(iv3,3);
        initClickEvent(iv4,4);
        initClickEvent(iv5,5);
        v.addView(view);
    }

    private void initClickEvent(ImageView view, final int position){
        view.setClickable(true);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, GalleryViewActivity.class);
                intent.putExtra("post_index",post_index);
                intent.putExtra("position",position);
                if(mContext instanceof DashboardActivity){
                    intent.putExtra("source","dashboard");
                }else{
                    intent.putExtra("source","profile");
                }
                mContext.startActivity(intent);
            }
        });
    }

    private MediaSource buildMediaSource(Uri uri) {
        @C.ContentType int type = Util.inferContentType(uri);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(
                        new DefaultDashChunkSource.Factory(mediaDataSourceFactory),
                        manifestDataSourceFactory)
                        .createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(
                        new DefaultSsChunkSource.Factory(mediaDataSourceFactory), manifestDataSourceFactory)
                        .createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }

    public void releasePlayer() {
        if (player != null) {
//            playbackPosition = player.getCurrentPosition();
//            currentWindow = player.getCurrentWindowIndex();
            player.release();
            player = null;
        }
    }




}
