package in.evergreen.socialapp.Utility;

import java.io.Serializable;

/**
 * Simple container object for contact data
 *
 * Created by Seetal on 02/07/17.
 * @author Sheetal
 */
public class Person implements Serializable {
    private String name;
    private String image;
    private String id;

    public Person(String name, String id, String image) {
        this.name = name;
        this.id = id;
        this.image = image;
    }

    public String getName() { return name; }
    public String getId() { return id; }
    public String getImage() { return image; }

    @Override
    public String toString() { return name; }
}
