package in.evergreen.socialapp.Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;


public class TLStorage {
	SharedPreferences pref;
	Editor editor;
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// LogCat tag
	private static String TAG = TLStorage.class.getSimpleName();

	// Shared preferences file name
	private static final String PREF_NAME = "parivartan";

	public TLStorage(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	public void setValueString(String key, String str) {
		editor.putString(key, str);
		editor.commit();
		Log.d(TAG, key + ":" + str);
	}

	public String getValueString(String key) {
		return pref.getString(key, "");
	}
	
	public void setValueBoolean(String key, Boolean str) {
		editor.putBoolean(key, str);
		editor.commit();
		Log.d(TAG, key + ":" + str);
	}
public void clearStorageData(){
	pref.edit().clear().commit();
}

	public Boolean getValueBoolean(String key) {
		Boolean bdata=pref.getBoolean(key, false);
		Log.d(TAG, key + ":" + bdata);
		return bdata;
	}

	

}
