package in.evergreen.socialapp.Utility;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;

import in.evergreen.socialapp.R;


public class TLHelper {
	private ProgressDialog pDialog;
	private Context mContext;
	public TLHelper(Context context) {
        this.mContext = context;   
        pDialog=new ProgressDialog(context);
    }

	public interface MyCallback {
		void callbackCall();
	}

	public void showAlert(String title, String msg){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext, R.style.confirmdialog);
		// Setting Dialog Title
		alertDialog.setTitle(title);
		// Setting Dialog Message
		alertDialog.setMessage(msg);
		// On pressing Settings button
		alertDialog.setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alertDialog.show();
	}
	
	public void showToast(String msg){
		Toast.makeText(this.mContext, msg, Toast.LENGTH_SHORT).show();
	}

	public float dpFromPx(final float px) {
		return px / mContext.getResources().getDisplayMetrics().density;
	}

	public float pxFromDp(final float dp) {
		return dp * mContext.getResources().getDisplayMetrics().density;
	}
	public void showLoader(String str){
		viewLoader(str, false);
	}
	
	public void showLoader(String str, Boolean cancelleble){
		viewLoader(str, cancelleble);
	}
	
	private void viewLoader(String str, Boolean cancelleble){
		try{			
			pDialog.setCancelable(cancelleble);
			pDialog.setMessage(str);
			if (pDialog.isShowing())
				pDialog.dismiss();
			pDialog.show();
		}catch (Exception e){
			
		}		
	}
	public double setRound(double d, int c)
	{
		int temp = (int)(d * Math.pow(10 , c));
		return ((double)temp)/ Math.pow(10 , c);
	}



	private String removeZeros(String s){
		s = s.indexOf(".") < 0 ? s : s.replaceAll("0*$", "").replaceAll("\\.$", "");
		return s;

	}
	public String makeRound(String val, String prec){
		String str= String.format("%."+prec+"f", Double.parseDouble(val));
		return str;
	}

	public void hideLoader(){
		try{
			if (pDialog.isShowing())
				pDialog.dismiss();
		}catch (Exception e){
			
		}
		
	}

	public String getDeviceID(){
		String android_id = Secure.getString(mContext.getContentResolver(),
				Secure.ANDROID_ID);
		return android_id;
	}
	
	public void noConnection(){
		showToast("No Internet Connection");
	}
	
	public boolean isSimSupport(){
        TelephonyManager tm = (TelephonyManager) this.mContext.getSystemService(Context.TELEPHONY_SERVICE);  //gets the current TelephonyManager
        if (tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT){
            return false;
        } else {
            return true;
        }
    }
	
	public int getRandomInteger(int aStart, int aEnd){
		Random aRandom = new Random();
	    if (aStart > aEnd) {
	    	throw new IllegalArgumentException("Start cannot exceed End.");
	    }
	    //get the range, casting to long to avoid overflow problems
	    long range = (long)aEnd - (long)aStart + 1;
	    // compute a fraction of the range, 0 <= frac < range
	    long fraction = (long)(range * aRandom.nextDouble());
	    int randomNumber =  (int)(fraction + aStart);    
	    return randomNumber;
	 }
	public void startSMSReceive() {
		TLStorage tls=new TLStorage(this.mContext);
		tls.setValueBoolean("SMS_RECEIVE", true);
	}

	public void stopSMSReceive() {
		TLStorage tls=new TLStorage(this.mContext);
		tls.setValueBoolean("SMS_RECEIVE", false);
	}
	
	public String mapToString(Map<String, String> params){
		String data="";
		for (Map.Entry<String, String> e : params.entrySet()) {
			data=data+e.getKey()+":"+ e.getValue()+"\n";		   
		}
		return data;
	}
	public String getMonthName(int mon){
		String[] monarr={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
		return monarr[mon];
	}
	public String ymdtodmy(String dmy){
		String ret="";
		String parts[]= dmy.split("-");
		if(parts.length == 3){
			String monthname=getMonthName((Integer.parseInt(parts[1])-1));
			ret=(parts[2]+" "+monthname+", "+parts[0]);
		}
		return ret;
	}
	
	public  boolean isValidEmail(String email) {
	    if (email == null) {
	        return false;
	    } else {
	        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	    }
	}
	public String getDuration(String Secs)
	{
		String time="00:00:00";
		long longVal= Long.parseLong(Secs);
		int hours = (int) longVal / 3600;
		time=(hours>9)? String.valueOf(hours)+":":"0"+ String.valueOf(hours)+":";
		int remainder = (int) longVal - hours * 3600;
		int mins = remainder / 60;
		time+=(mins>9)? String.valueOf(mins)+":":"0"+ String.valueOf(mins)+":";
		remainder = remainder - mins * 60;
		int secs = remainder;
		time+=(secs>9)? String.valueOf(secs):"0"+ String.valueOf(secs);
		return time;
	}
	public String getRealPathFromURI(Uri contentUri) {
		  Cursor cursor = null;
		  try { 
		    String[] proj = { MediaStore.Images.Media.DATA };
		    cursor = mContext.getContentResolver().query(contentUri,  proj, null, null, null);
		    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		    cursor.moveToFirst();
		    return cursor.getString(column_index);
		  } finally {
		    if (cursor != null) {
		      cursor.close();
		    }
		  }
		}
	public String ImageToString(Bitmap photo){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] imageBytes = baos.toByteArray();
		String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
		return encodedImage;
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final View mProgress, final View mForm, final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = mContext.getResources().getInteger(android.R.integer.config_shortAnimTime);

			mForm.setVisibility(show ? View.GONE : View.VISIBLE);
			mForm.animate().setDuration(shortAnimTime).alpha(
					show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mForm.setVisibility(show ? View.GONE : View.VISIBLE);
				}
			});

			mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
			mProgress.animate().setDuration(shortAnimTime).alpha(
					show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
				}
			});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
			mForm.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}
	public String ByteToString(byte[] bdata){
		return Base64.encodeToString(bdata, 0);
	}


	/***************Start code by zafar*************/
	public String cmToInch(String cm) {
		if (!cm.isEmpty()) {
			double inch_val = Double.parseDouble(cm) * 0.393701;
			inch_val= Double.parseDouble(String.valueOf(inch_val));
			return String.valueOf(Math.round(inch_val));

		}else{
			return "";
		}
	}
	public String inchToCm(String inch){
		if(!inch.isEmpty()) {
			double mile_val = Double.parseDouble(inch) * 2.54;
			return String.valueOf(Math.round(mile_val));
		}else{
			return "";
		}
	}
	public String[] cmToFeetInch(String cm){
		String[] redata= new String[2];
		if (!cm.isEmpty()) {
			double inch_val = Double.parseDouble(cm) * 0.393701;
			double feet= Math.round(inch_val / 12);
			double inch=inch_val-(feet*12);
			if(inch<0){ feet=feet-1; inch=12+inch;}
			redata[0]= String.valueOf(Math.round(feet));
			redata[1]= String.valueOf(Math.round(inch));
			return redata;
		}else{
			redata[0]="";
			redata[1]="";
			return redata;
		}
	}
	public String feetInchToCm(String[] feetInch){
		double inch_val= (!feetInch[0].isEmpty())? Double.parseDouble(feetInch[0]) * 12:0;
		double inch_val1= (!feetInch[1].isEmpty())? Double.parseDouble(feetInch[1]) :0;
		return String.valueOf(Math.round(Double.parseDouble(inchToCm(String.valueOf(inch_val + inch_val1)))));
	}

	public void openAppSetting(){
		Context context=mContext;
		if (context == null) {
			return;
		}
		final Intent i = new Intent();
		i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
		i.addCategory(Intent.CATEGORY_DEFAULT);
		i.setData(Uri.parse("package:" + context.getPackageName()));
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		context.startActivity(i);
	}

	public String getShortName(String name){
		Log.e("DISANME","---"+name);
		StringBuilder s= new StringBuilder();
		String[] myName = name.split(" ");
		for (int i = 0; i < myName.length; i++) {
			if(myName[i].length()>0)
				s.append(myName[i].toUpperCase().charAt(0));
			if(i==1)
				break;
		}
		return s.toString();
	}

	public Date getDate(String date){
		if(date.isEmpty()){
			return new Date();
		}else{
			String[] parts = date.split("T");
			return new Date();
		}
	}

	public SpannableString getSpan(String titleText){
		SpannableString ss1=  new SpannableString(titleText);
		ss1.setSpan(new RelativeSizeSpan(.8f), 0,titleText.length(), 0); // set size
		ss1.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.light_grey)), 0, titleText.length(), 0);// set color
		return ss1;
	}

    public String formatToYesterdayOrToday(String date) throws ParseException {
        date = date.replace("T"," ");
        Date dateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(date);
        //2018-05-20T19:28:03.000Z
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        DateFormat timeFormatter = new SimpleDateFormat("hh:mm a");
        DateFormat dateFormatter = new SimpleDateFormat("dd MMM, yyyy");


        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return mContext.getResources().getString(R.string.title_today_at) +" " + timeFormatter.format(dateTime);
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return mContext.getResources().getString(R.string.title_yesterday_at) +" " + timeFormatter.format(dateTime);
        } else {
            return dateFormatter.format(dateTime)+" AT " + timeFormatter.format(dateTime);
        }
    }

    public String label(String key){
		try{
			int id = mContext.getResources().getIdentifier(key, "string", mContext.getPackageName());
			return mContext.getResources().getString(id);
		}catch (Exception e){

		}
		return "";
	}

	public String label(int id){
		try{
			return mContext.getResources().getString(id);
		}catch (Exception e){

		}
		return "";
	}

	public void showError(int code){
		switch (code) {
			case 404:
				Toast.makeText(mContext, "Not Found", Toast.LENGTH_SHORT).show();
				break;
			case 500:
				Toast.makeText(mContext, "Server Broken. Try again after some time.", Toast.LENGTH_SHORT).show();
				break;
			default:
				Toast.makeText(mContext, "Unknown error. Try again after some time.", Toast.LENGTH_SHORT).show();
				break;
		}
	}

	public String getCapsSentences(String tagName) {
		Log.e("NAME:", tagName+"---");
		String[] splits = tagName.toLowerCase().split(" ");
		StringBuilder sb = new StringBuilder();
		Log.e("INSEX LENGTH",splits.length+"");
		for (int i = 0; i < splits.length; i++) {
			String eachWord = splits[i];
			if (i > 0 && eachWord.length() > 0) {
				sb.append(" ");
			}
			if(eachWord.length()>0){
				String cap = eachWord.substring(0, 1).toUpperCase()
						+ eachWord.substring(1);
				sb.append(cap);
			}

		}
		return sb.toString();
	}

	public void confirmDialog(String title, String content, String actionLabel, final MyCallback callback){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext,R.style.confirmdialog);
		alertDialog.setTitle(title);
		alertDialog.setMessage(content);
		alertDialog.setPositiveButton(actionLabel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						callback.callbackCall();
					}
				});
		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		// Showing Alert Message
		alertDialog.show();
	}

	public String formatToYesterdayOrToday2(String date) throws ParseException {
		date = date.replace("T"," ");
		Date dateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(date);
		//2018-05-20T19:28:03.000Z
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateTime);
		Calendar today = Calendar.getInstance();
		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DATE, -1);
		DateFormat timeFormatter = new SimpleDateFormat("hh:mm a");
		DateFormat dateFormatter = new SimpleDateFormat("dd MMM, yyyy");


		if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
			return mContext.getResources().getString(R.string.title_today_at) +" " + timeFormatter.format(dateTime);
		} else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
			return mContext.getResources().getString(R.string.title_yesterday_at) +" " + timeFormatter.format(dateTime);
		} else {
			//return dateFormatter.format(dateTime)+" AT " + timeFormatter.format(dateTime);
			return dateFormatter.format(dateTime);
		}
	}

}
