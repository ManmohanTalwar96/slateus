package in.evergreen.socialapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;


import java.util.Calendar;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;


public class WindowActivityDP extends AppCompatActivity {

    TLHelper hlp;
    TLStorage sto;
    int width;
    int height;
    String user_id;
    String user_name;
    int position;
    public String datetext="";
    public Calendar scheduleCalender;
    public  int year;
    public  int month;
    public  int day;
    public int hour;
    public int minute;
    public ImageView scheduledImage;
    public TextView txtPostDateTime;
    public AlertDialog dialog;
    public int last_request=0;
    Uri mImageCaptureUri;
    String mImageCaptureURL;
    private static final int PICK_FROM_CAMERA = 6;
    private static final int PICK_FROM_FILE = 7;
    public static final int REQUEST_ID_WRITE_PERMISSIONS=2;
    String edate;
    Uri resultUri;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams params = this.getWindow().getAttributes();
        params.alpha = 1.0f;
        params.dimAmount = 0.7f;
        this.getWindow().setAttributes((WindowManager.LayoutParams) params);
        // This sets the window size, while working around the IllegalStateException thrown by ActionBarView

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
        }
        width = size.x;
        width=(int) (width * 0.9);
        height = size.y;
        height=(int) (height * 0.8);
        this.getWindow().setLayout(width, height);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_window_dp);
        hlp = new TLHelper(WindowActivityDP.this);
        sto = new TLStorage(getApplicationContext());

        scheduleCalender = Calendar.getInstance();
        scheduledImage = (ImageView) findViewById(R.id.imgThubmnail);
        txtPostDateTime = (TextView) findViewById(R.id.txtChangePostTime);
        Calendar c = Calendar.getInstance();
        c.add( Calendar.DATE, 1 );
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DATE);
        hour = 9;
        minute = 0;
        String edate =  day + " " + hlp.getMonthName(month) + ", " + year+" "+"09:"+minute+" AM";
        txtPostDateTime.setText("Post at: "+edate);
        datetext = String.valueOf(year) + "-" + String.valueOf((month + 1)) + "-" + String.valueOf(day)+" "+hour+":"+minute;


    }

    private com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener scheduleDatePickerListener = new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int myear, int monthOfYear, int dayOfMonth) {

            String edate =  dayOfMonth + " " + hlp.getMonthName(monthOfYear) + ", " + myear;
            year = myear;
            month = monthOfYear;
            day = dayOfMonth;
            datetext = String.valueOf(myear) + "-" + String.valueOf((monthOfYear + 1)) + "-" + String.valueOf(dayOfMonth);
            txtPostDateTime.setText("Post at: "+edate);
            TimePickerDialog dpd = TimePickerDialog.newInstance(scheduleTimePickerListener,hour,minute,false);
            dpd.setThemeDark(true);
            dpd.show(getFragmentManager(), "Timepickerdialog");
        }
    };

    private TimePickerDialog.OnTimeSetListener scheduleTimePickerListener = new TimePickerDialog.OnTimeSetListener(){

        @Override
        public void onTimeSet(RadialPickerLayout view, int hourOfDay, int m) {
            hour = hourOfDay;
            minute = m;
            String ampm = "AM";
            int hourdisplay = hour;
            if(hour>12){
                ampm = "PM";
                hourdisplay = hour-12;
            }
            edate =  day + " " + hlp.getMonthName(month) + ", " + year+" "+hourdisplay+":"+minute+" "+ampm;

            Log.e("Callback Date",edate);
            datetext = String.valueOf(year) + "-" + String.valueOf((month + 1)) + "-" + String.valueOf(day)+" "+hour+":"+minute;
            txtPostDateTime.setText("Post at: "+edate);
        }
    };


    public void changePostTime(View v){
        DatePickerDialog dpd = DatePickerDialog.newInstance(scheduleDatePickerListener,year,month,day);
        dpd.setThemeDark(true);
        dpd.setMinDate(scheduleCalender);
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    public void closeActivity(View v) {

        finish();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_out_up, R.anim.slide_in_up);
        finish();
        overridePendingTransition(R.anim.slide_out_up, R.anim.slide_in_up);
    }

    public void sendSchedule(View v) {

        Intent intent2 = new Intent();

        intent2.putExtra("schedule_at", edate);
        intent2.putExtra("picture", resultUri);

        sendBroadcast(intent2);
        finish();//finishing activity

    }

    public void changeDP(View v){

        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setFlipHorizontally(false)
                .setFlipHorizontally(false)
                .setAutoZoomEnabled(true)
                .setShowCropOverlay(true)
                .setActivityTitle("Image")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setFixAspectRatio(true)
                .setCropMenuCropButtonTitle("Done")
                .setRequestedSize(500, 500)
                .start(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                scheduledImage.setImageURI(result.getUri());
                resultUri = result.getUri();


//                uploadImage(result.getUri());

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        CropImage.ActivityResult result = CropImage.getActivityResult(data);
    }

//    public void saveImage(String output){
//        ImageView userimage=(ImageView) findViewById(R.id.imgThubmnail);
//        Glide.with(WindowActivityDP.this).load(output).into(userimage);
//        imagepath=output;
//        findViewById(R.id.txtAddDP).setVisibility(View.GONE);
//    }

}
