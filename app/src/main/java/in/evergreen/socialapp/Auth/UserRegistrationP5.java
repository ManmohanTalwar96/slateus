package in.evergreen.socialapp.Auth;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.TLStorage;

public class UserRegistrationP5 extends AppCompatActivity {

    TLStorage sto;
    String password;
    ImageView validator;
    EditText txtpassword;
    Pattern pattern;
    Matcher matcher;
    final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*+=])(?=\\S+$).{6,20}$";

    final String PASSWORD_PATTERN2 = "^(?=.*[a-z])(?=.*[A-Z]).{6,20}$";
    final String PASSWORD_PATTERN3 = "^(?=.*[a-z]).{6,20}$";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_registration_p5);
        sto = new TLStorage(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        txtpassword = (EditText) findViewById(R.id.reg_password);
        validator = (ImageView) findViewById(R.id.password_validator);

//        private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";

        txtpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!isValidPassword(txtpassword.getText().toString().trim())) {
                    validator.setVisibility(View.INVISIBLE);
//                    txtpassword.setError("Invalid Password");
                } else {
                    validator.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    public boolean isValidPassword(final String password) {

        pattern = Pattern.compile(PASSWORD_PATTERN3);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public boolean ValidateForm(){
        boolean error = false;


        password = txtpassword.getText().toString().trim();

        if(password.isEmpty()){
            error = true;
            txtpassword.setError("Please Enter your Password");
        }else {
            txtpassword.setError(null);
        }

        return error;
    }

    public void actionRegister5(View view) {

        if(ValidateForm()==false && isValidPassword(password) == true) {


            sto.setValueString("password",password);

            Intent i = new Intent(getApplicationContext(),UserRegistrationFinal.class);
            startActivity(i);
            }
            else {
            validator.setVisibility(View.INVISIBLE);
            txtpassword.setError("Please enter a Valid Password");
        }
        }
    }

