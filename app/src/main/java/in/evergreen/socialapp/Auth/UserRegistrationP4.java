package in.evergreen.socialapp.Auth;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.Objects;

import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.TLStorage;

public class UserRegistrationP4 extends AppCompatActivity {

    TLStorage sto;
    String email;
    EditText txtemail;
    ImageView validator;
//    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_registration_p4);
        sto = new TLStorage(this);

        validator = findViewById(R.id.email_validator);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        txtemail = (EditText) findViewById(R.id.reg_email);

        txtemail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!Patterns.EMAIL_ADDRESS.matcher(txtemail.getText().toString()).matches()) {
                    validator.setVisibility(View.INVISIBLE);
//                    txtemail.setError("Invalid email");
                } else {
                    validator.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    public boolean ValidateForm(){
        boolean error = false;

        email = txtemail.getText().toString().trim();

        if(email.isEmpty()){
            error = true;
            txtemail.setError("Please Enter your Email ID");
        }else {
            txtemail.setError(null);
        }

        return error;
    }

    public void actionRegister4(View view) {

        if(ValidateForm()==true) {
            return;}

            sto.setValueString("email",email);

            Intent i = new Intent(getApplicationContext(),UserRegistrationP5.class);
            startActivity(i);
        }
    }

