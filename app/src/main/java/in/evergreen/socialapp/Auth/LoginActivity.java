package in.evergreen.socialapp.Auth;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import Apis.ApiClient;
import Apis.AuthInterface;
import in.evergreen.socialapp.DashboardActivity;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;
import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    String mobile, password;
    EditText textMobile, textPassword;
    ImageView imgCheck;
    AuthInterface apiService;
    JsonParser jsonParser;
    JsonObject jsonObject;
    TLStorage sto;
    TLHelper hlp;
    String TAG = LoginActivity.class.getName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.new_auth_login);
        imgCheck = findViewById(R.id.email_validator);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        textMobile = (EditText) findViewById(R.id.login_emailMobile);
        textPassword = (EditText) findViewById(R.id.login_password);

        mobile = textMobile.getText().toString().trim();
        password = textPassword.getText().toString().trim();

        textMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!Patterns.EMAIL_ADDRESS.matcher(textMobile.getText().toString()).matches()) {
                    imgCheck.setVisibility(View.INVISIBLE);
//                    textMobile.setError("Invalid email");
                } else {
                    imgCheck.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        hideKeyboard();
        apiService = ApiClient.getClient().create(AuthInterface.class);
        jsonParser = new JsonParser();
        jsonObject = new JsonObject();
        sto = new TLStorage(this);
        hlp = new TLHelper(this);
    }

    public boolean ValidateForm() {
        boolean error = false;

        textMobile = (EditText) findViewById(R.id.login_emailMobile);
        textPassword = (EditText) findViewById(R.id.login_password);

        mobile = textMobile.getText().toString().trim();
        password = textPassword.getText().toString().trim();

        if (mobile.isEmpty() && password.isEmpty()) {
            error = true;
            textMobile.setError("Please Enter your Email-Id");
            textPassword.setError("Please Enter your Password");

        } else {
            textMobile.setError(null);
            textPassword.setError(null);
        }
        if (mobile.isEmpty()) {
            error = true;
            textMobile.setError("Please Enter your Email-Id");
            hideKeyboard();
        } else {
            textMobile.setError(null);
        }
        if (password.isEmpty()) {
            error = true;
            textPassword.setError("Please Enter your Password");
            hideKeyboard();
        } else {
            textPassword.setError(null);
        }
        return error;
    }

    public void actionLogin(View v) {

        if (ValidateForm() == true) {
            return;
        }
        Log.e(TAG, "Calling AOP");
        JSONObject jo = new JSONObject();
        try {
            jo.put("email", mobile);
            jo.put("password", password);

            jsonObject = (JsonObject) jsonParser.parse(jo.toString());
            hlp.showLoader("Please wait ...");
            Call<JsonElement> call = apiService.login(jsonObject);
            call.enqueue(new Callback<JsonElement>() {

                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hlp.hideLoader();
                    JsonElement jsonElement = response.body();
                    String body = jsonElement.toString();
                    Log.e(TAG, "entering try");

                    try {
                        JSONObject res = new JSONObject(body);
                        Log.e(TAG, "entering if");
                        if (res.getString("status").equalsIgnoreCase("success")) {
                            JSONObject data = res.getJSONObject("data");
                            Log.e(TAG, "Setting value String");

                            Log.e("_id is", String.valueOf(res));
                            sto.setValueString("_id", data.getString("_id"));
                            sto.setValueString("display_name", data.getString("display_name"));
                            sto.setValueString("email", data.getString("email"));
                            sto.setValueString("mobile", data.getString("mobile"));
                            sto.setValueString("token", res.getString("_token"));

                            sto.setValueString("image", data.getString("user_image_thumb"));
                            Log.e(TAG, "Next is intent to dashboard");
                            Intent i = new Intent(LoginActivity.this, DashboardActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();

                        } else if (res.getString("status").equalsIgnoreCase("failed")) {
                            ((TextView) findViewById(R.id.error)).setText(res.getString("message").toString());
                            findViewById(R.id.error).setVisibility(View.VISIBLE);
                            hideKeyboard();
                        } else if (res.getString("status").equalsIgnoreCase("Verification Pending")) {

                            JSONObject id = res.getJSONObject("data");

                            final int user_id = id.getInt("_id");
//
                            final PrettyDialog pDialog = new PrettyDialog(LoginActivity.this);
                            pDialog
                                    .setIcon(
                                            R.drawable.pdlg_icon_info,
                                            R.color.warning,
                                            new PrettyDialogCallback() {
                                                @Override
                                                public void onClick() {

                                                }
                                            }
                                    )
                                    .setTitle("Verification")
                                    .setMessage(res.getString("message").toString())
                                    .addButton(
                                            "RESEND",     // button text
                                            R.color.pdlg_color_white,  // button text color
                                            R.color.dark_blue,  // button background color
                                            new PrettyDialogCallback() {  // button OnClick listener
                                                @Override
                                                public void onClick() {
                                                    resendLink(user_id);
                                                    pDialog.dismiss();
                                                    Toast.makeText(LoginActivity.this,"Verification Sent",Toast.LENGTH_LONG).show();;

                                                }
                                            }
                                    )
                                    .addButton(
                                            "Cancel",
                                            R.color.dark_grey,
                                            R.color.background_feed,
                                            new PrettyDialogCallback() {
                                                @Override
                                                public void onClick() {
                                                    pDialog.dismiss();
                                                }
                                            }
                                    )
                                    .setAnimationEnabled(true)
                                    .show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hlp.hideLoader();
                    hlp.noConnection();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void resendLink(int user_id){

        Call<JsonElement> call = apiService.resendLink(user_id);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                hlp.hideLoader();
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });

    }
    public void openRegistration(View v) {
        Intent i = new Intent(this, UserRegistrationP1.class);
        startActivity(i);
    }

    public void resetPassword(View view) {
        Intent i = new Intent(this, ForgetPassword.class);
        startActivity(i);
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}
