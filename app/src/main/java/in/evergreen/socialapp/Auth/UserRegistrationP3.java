package in.evergreen.socialapp.Auth;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.TLStorage;

public class UserRegistrationP3 extends AppCompatActivity {

    TLStorage sto;
    EditText phone;
    ImageView validator;
    String txtphone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_registration_p3);


        phone = (EditText) findViewById(R.id.reg_mobile);
        validator = (ImageView) findViewById(R.id.number_validator);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        txtphone = phone.getText().toString().trim();

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!isValidPhoneNumber(phone.getText().toString())) {
                    validator.setVisibility(View.INVISIBLE);
//                    phone.setError("Invalid Number");
                } else {
                    validator.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        sto = new TLStorage(this);
    }

    private boolean isValidPhoneNumber(String number) {
        if (number != null && number.length() > 9) {
            return true;
        }
        return false;
    }

    public boolean ValidateForm(){
        boolean error = false;

        txtphone = phone.getText().toString().trim();

        if (txtphone.isEmpty()) {
            error = true;
            ((TextView) findViewById(R.id.error)).setText("Please Enter your Mobile Number");
            findViewById(R.id.error).setVisibility(View.VISIBLE);

        } else {
            findViewById(R.id.error).setVisibility(View.INVISIBLE);
        }
        return error;
    }


    public void actionRegister3(View view) {
        if(ValidateForm()==true) {
            return;}

            sto.setValueString("phone", txtphone);

            Intent i = new Intent(getApplicationContext(),UserRegistrationP4.class);
            startActivity(i);
        }
    }

