package in.evergreen.socialapp.Auth;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import Apis.ApiClient;
import Apis.AuthInterface;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPassword extends AppCompatActivity {

    EditText txtemail;
    String email;
    ImageView validator;
    AuthInterface apiService;
    JsonParser jsonParser;
    JsonObject jsonObject;
    TLHelper hlp;
    TLStorage sto;


    String TAG = ForgetPassword.class.getName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_forget_password);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        apiService = ApiClient.getClient().create(AuthInterface.class);
        jsonParser = new JsonParser();
        jsonObject = new JsonObject();
        sto = new TLStorage(this);
        hlp = new TLHelper(this);

        validator = findViewById(R.id.email_validator);

        txtemail = (EditText) findViewById(R.id.forget_email);

        email = txtemail.getText().toString().trim();

        txtemail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!Patterns.EMAIL_ADDRESS.matcher(txtemail.getText().toString()).matches()) {
                    validator.setVisibility(View.INVISIBLE);
//                    txtemail.setError("Invalid email");
                } else {
                    validator.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    public boolean ValidateForm(){
        boolean error = false;

        email = txtemail.getText().toString().trim();

        if(email.isEmpty()){
            error = true;
            txtemail.setError("Please Enter your Email ID");
        }else {
            txtemail.setError(null);
        }

        return error;

    }

    public void actionResetPassword(View view) {


        findViewById(R.id.error).setVisibility(View.INVISIBLE);
        if(ValidateForm()==true) {

            return;
        }
            Log.e(TAG,"entering action");

            JSONObject jo = new JSONObject();
            try{
                jo.put("email",email);

                jsonObject = (JsonObject)jsonParser.parse(jo.toString());
                hlp.showLoader("Please wait ...");
                Call<JsonElement> call = apiService.reset(jsonObject);
                call.enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        hlp.hideLoader();
                        JsonElement jsonElement = response.body();
                        String body = jsonElement.toString();
                        Log.e(TAG,"entering response try");

                        try{
                            JSONObject res = new JSONObject(body);
                            if(res.getString("status").equalsIgnoreCase("success")){
                                Log.e(TAG,"checking response");
                                AlertDialog alertDialog = new AlertDialog.Builder(
                                        ForgetPassword.this).create();

                                final PrettyDialog pDialog = new PrettyDialog(ForgetPassword.this);
                                pDialog
                                        .setIcon(
                                        R.drawable.pdlg_icon_success,     // icon resource
                                        R.color.dark_blue,
                                                new PrettyDialogCallback() {
                                                    @Override
                                                    public void onClick() {

                                                    }
                                                }
                                        )
                                        .setTitle("Success")
                                        .setMessage("Password Reset Link sent to Email!")
                                        .addButton(
                                                "Continue to Login!",     // button text
                                                R.color.pdlg_color_white,  // button text color
                                                R.color.dark_blue,  // button background color
                                                new PrettyDialogCallback() {  // button OnClick listener
                                                    @Override
                                                    public void onClick() {
                                                        Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        startActivity(i);
                                                        finish();
                                                        pDialog.dismiss();
                                                    }
                                                }
                                        )
                                        .setAnimationEnabled(true)
                                        .show();
                            }else if (res.getString("status").equalsIgnoreCase("failed")){
                                ((TextView) findViewById(R.id.error)).setText(res.getString("message").toString());
                                findViewById(R.id.error).setVisibility(View.VISIBLE);

                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {

                        hlp.hideLoader();
                        hlp.noConnection();
                    }
                });

            }catch (JSONException e){
                e.printStackTrace();
            }
        }
}

