package in.evergreen.socialapp.Auth;

import android.content.Intent;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;

public class UserRegistrationP2 extends AppCompatActivity {

    String dob;
    TLStorage sto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration_p2);

        sto = new TLStorage(this);

        DatePicker datePicker = (DatePicker) findViewById(R.id.reg_datepicker);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        dob = (new SimpleDateFormat("yyyy-MM-dd", Locale.US)).format(new Date());
        Log.e("mnth", String.valueOf(dob));
        datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                Log.d("Date", "Year=" + year + " Month=" + (month + 1) + " day=" + dayOfMonth);
                dob = datePicker.getYear() +
                        "-" + (datePicker.getMonth()+1) +
                        "-" + datePicker.getDayOfMonth();

            }
        });
    }

//    public boolean ValidateForm(){
//        boolean error = false;
//
//        if(dob.isEmpty()){
//            error = true;
//            findViewById(R.id.reg_errorDob).setVisibility(View.VISIBLE);
//        }else {
//            findViewById(R.id.reg_errorDob).setVisibility(View.INVISIBLE);
//        }
//
//        return error;
//    }

    public void actionRegister2(View view) {

            Log.e("sto dob", dob);
            sto.setValueString("DOB",dob);

            Intent i = new Intent(getApplicationContext(),UserRegistrationP3.class);
            startActivity(i);
        }
    }

