package in.evergreen.socialapp.Auth;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import Apis.ApiClient;
import Apis.AuthInterface;

import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;
import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRegistrationFinal extends AppCompatActivity {

    String firstName, lastName, email, password,dob,  phone;
    String gender = "male";
    final Context context = this;
    LinearLayout male, female, other;
    AuthInterface apiService;
    JsonParser jsonParser;
    JsonObject jsonObject;
    TLStorage sto;
    TLHelper hlp;
    String TAG = UserRegistrationFinal.class.getName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_registration_final);


        apiService = ApiClient.getClient().create(AuthInterface.class);
        jsonParser = new JsonParser();
        jsonObject = new JsonObject();
        sto = new TLStorage(this);
        hlp = new TLHelper(this);
    }

    public boolean ValidateForm(){
        boolean error = false;

        firstName = sto.getValueString("firstName");
        lastName = sto.getValueString("lastName");
        email = sto.getValueString("email");
        password = sto.getValueString("password");
        dob = sto.getValueString("DOB");
        phone = sto.getValueString("phone");
        if(gender.isEmpty()){
            error = true;
            findViewById(R.id.error).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.error).setVisibility(View.INVISIBLE);
        }
        return error;
    }

    public void actionGender(View view) {
        gender = view.getTag().toString().trim();

        switchGenderIcon(gender);
    }


    public void switchGenderIcon(String tag){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ((RelativeLayout) findViewById(R.id.male_circle)).setBackground(getDrawable(R.drawable.gender_unselected));
            ((RelativeLayout) findViewById(R.id.female_circle)).setBackground(getDrawable(R.drawable.gender_unselected));
            ((RelativeLayout) findViewById(R.id.other_circle)).setBackground(getDrawable(R.drawable.gender_unselected));
        }

        switch (tag){
            case "Male":
                Log.e("male case",gender);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((RelativeLayout) findViewById(R.id.male_circle)).setBackground(getDrawable(R.drawable.gender_selected));
                }
                break;
            case "Female":
                Log.e("female case",gender);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((RelativeLayout) findViewById(R.id.female_circle)).setBackground(getDrawable(R.drawable.gender_selected));
                }

                break;
            case "Other":
                Log.e("other case",gender);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((RelativeLayout) findViewById(R.id.other_circle)).setBackground(getDrawable(R.drawable.gender_selected));
                }
                break;

        }

    }


    public void actionRegisterFinal(View view) {

        if(ValidateForm()==true) {
            findViewById(R.id.error).setVisibility(View.INVISIBLE);
            return;
        }

        Log.e(TAG, "Calling AOP");
        JSONObject jo = new JSONObject();
        try {
            jo.put("email", email);
            jo.put("password", password);
            jo.put("first_name", firstName);
            jo.put("last_name", lastName);
            jo.put("dob", dob);
            jo.put("gender", gender);
            jo.put("mobile", phone);

            jsonObject = (JsonObject) jsonParser.parse(jo.toString());
            hlp.showLoader("Please wait ...");
            Call<JsonElement> call = apiService.register(jsonObject);
            call.enqueue(new Callback<JsonElement>() {

                @Override
                public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                    hlp.hideLoader();
                    JsonElement jsonElement = response.body();
                    String body = jsonElement.toString();
                    Log.e(TAG, "entering try");

                    try {
                        JSONObject res = new JSONObject(body);
                        if(res.getString("status").equalsIgnoreCase("success")){
                            Log.e(TAG, "Setting value String");

                            final PrettyDialog pDialog = new PrettyDialog(UserRegistrationFinal.this);
                            pDialog
                                    .setIcon(
                                            R.drawable.pdlg_icon_success,     // icon resource
                                            R.color.dark_blue,
                                            new PrettyDialogCallback() {
                                                @Override
                                                public void onClick() {

                                                }
                                            }
                                    )
                                    .setTitle("Success")
                                    .setMessage("Your Registration is Successful, please verify your email and continue!")
                                    .addButton(
                                            "Continue to Login!",     // button text
                                            R.color.pdlg_color_white,  // button text color
                                            R.color.dark_blue,  // button background color
                                            new PrettyDialogCallback() {  // button OnClick listener
                                                @Override
                                                public void onClick() {
                                                    Intent i = new Intent(UserRegistrationFinal.this, LoginActivity.class);
                                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(i);
                                                    finish();
                                                    pDialog.dismiss();
                                                }
                                            }
                                    )
                                    .setAnimationEnabled(true)
                                    .show();

                        }else if(res.getString("status").equalsIgnoreCase("failed")){
                            ((TextView) findViewById(R.id.error)).setText(res.getString("message").toString());
                            findViewById(R.id.error).setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hlp.hideLoader();
                    hlp.noConnection();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
