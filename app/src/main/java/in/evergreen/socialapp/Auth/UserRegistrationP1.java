package in.evergreen.socialapp.Auth;

import android.content.Intent;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import in.evergreen.socialapp.Auth.LoginActivity;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.TLStorage;

public class UserRegistrationP1 extends AppCompatActivity {

    String firstName, lastName;
    EditText textFirstName, textLastName;
    TLStorage sto;

    String TAG = UserRegistrationP1.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_registration_p1);

        sto = new TLStorage(this);
    }

    public boolean ValidateForm(){
        boolean error = false;

        textFirstName = (EditText) findViewById(R.id.reg_firstName);
        textLastName = (EditText) findViewById(R.id.reg_lastName);

        firstName = textFirstName.getText().toString().trim();
        lastName = textLastName.getText().toString().trim();


        if(firstName.isEmpty()){
            error = true;
            textFirstName.setError("Please Enter your First Name");
        }else {
            textFirstName.setError(null);
        }
        if(lastName.isEmpty()){
            error = true;
            textLastName.setError("Please Enter Last Name");
        }else {
            textLastName.setError(null);
        }
        Log.e(TAG,firstName);
        Log.e(TAG,lastName);
        return error;
    }

    public void actionRegister1(View view) {

        if(ValidateForm()==true) {
            return;}

            sto.setValueString("firstName", firstName);
            sto.setValueString("lastName", lastName);

            Intent i = new Intent(getApplicationContext(), UserRegistrationP2.class);
            startActivity(i);

    }


}
