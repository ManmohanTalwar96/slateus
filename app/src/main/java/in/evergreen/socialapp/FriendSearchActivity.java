package in.evergreen.socialapp;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Apis.ApiClient;
import Apis.UserInterface;
import Models.FriendsSearchModel;
import in.evergreen.socialapp.Adapters.SearchAdapter;
import in.evergreen.socialapp.Auth.LoginActivity;
import in.evergreen.socialapp.Utility.EndlessRecyclerOnScrollListener;
import in.evergreen.socialapp.Utility.SearchSuggestionProvider;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FriendSearchActivity extends AppCompatActivity {

    public TextView noRecord;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<FriendsSearchModel> item = new ArrayList<FriendsSearchModel>();
    private SearchAdapter adapter;
    private UserInterface apiInterface;
    TLHelper hlp;
    TLStorage sto;
    View  reloader;
    public int page=1;
    public boolean pageend =false;
    boolean is_loading =false;
    EndlessRecyclerOnScrollListener esl;


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_search);
        handleIntent(getIntent());

        noRecord = (TextView) findViewById(R.id.search_txtNoRecord);
        reloader = findViewById(R.id.search_buttonReload);
        apiInterface = ApiClient.getClient().create(UserInterface.class);
        reloader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchResult("");
            }
        });
        sto = new TLStorage(this);
        hlp = new TLHelper(this);


//        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        recyclerView = findViewById(R.id.friendSearchRecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new SearchAdapter(item, FriendSearchActivity.this);
        recyclerView.setAdapter(adapter);

        esl = new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                Log.e("SCROLL ", pageend+"==="+is_loading);
                if(pageend==false && is_loading == false){
                    page++;
                    searchResult("");
                }
            }
        };

        recyclerView.addOnScrollListener(esl);
        searchResult("");
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            searchResult(query);

            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    SearchSuggestionProvider.AUTHORITY, SearchSuggestionProvider.MODE);
            suggestions.saveRecentQuery(query, null);
        }
    }


    public void searchResult(String q){


        Call<JsonElement> call = apiInterface.getUser(sto.getValueString("token"), q, 1);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();
                Log.e("RESPONSE", body);

                try {

                    JSONObject res = new JSONObject(body);

                    if(res.getString("status").equalsIgnoreCase("success")){


                        JSONArray data = res.getJSONArray("data");


                        createListing(data);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.hideLoader();
                hlp.noConnection();
            }
        });
    }


    public void createListing(JSONArray data) throws JSONException{
        item.clear();
        for (int i=0; i<data.length(); i++) {
            try {
                item.add(new FriendsSearchModel(data.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
                }
        }

        adapter.notifyDataSetChanged();
        if(item.size()==0){
            noRecord.setVisibility(View.VISIBLE);
        }
        else {
            noRecord.setVisibility(View.INVISIBLE);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.friend_search_menu,menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();



        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName())
        );
        searchView.setSubmitButtonEnabled(true);
        searchView.setQueryRefinementEnabled(true);
        searchView.setIconifiedByDefault(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                searchResult(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchResult(newText);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
