package in.evergreen.socialapp;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import cn.jzvd.JZVideoPlayer;
import com.crashlytics.android.Crashlytics;
import in.evergreen.socialapp.Utility.FontsOverride;
import in.evergreen.socialapp.Utility.TLStorage;
import io.fabric.sdk.android.Fabric;


public class MyApplication extends Application {

    public static String FOLDER = "slateus";
    public static int _id = 0;
    public static String display_name = "";
    public static String token = "";
    public static String image = "";
    public static String email = "";


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/lato-300.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/lato-400.ttf");
        //FontsOverride.setDefaultFont(this, "SERIF", "fonts/lato-700.ttf");
        //FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/lato-700.ttf");
        //FontsOverride.setDefaultFont(this, "LIGHT", "fonts/lato-300.ttf");
        JZVideoPlayer.WIFI_TIP_DIALOG_SHOWED = true;
    }



    public static void initUserSession(Context ctx){
        TLStorage sto = new TLStorage(ctx);
        Log.e("_id",sto.getValueString("_id"));
        _id = Integer.parseInt(sto.getValueString("_id"));
        display_name = sto.getValueString("display_name");
        token = sto.getValueString("token");
        image = sto.getValueString("image");
        email = sto.getValueString("email");
    }
}
