package in.evergreen.socialapp.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Apis.ApiClient;
import Apis.UserInterface;
import Models.FriendsSearchModel;
import Models.ProfileFriendsModel;
import in.evergreen.socialapp.Adapters.ProfileFriendsAdapter;
import in.evergreen.socialapp.Adapters.SearchAdapter;
import in.evergreen.socialapp.FriendSearchActivity;
import in.evergreen.socialapp.MyApplication;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.EndlessRecyclerOnScrollListener;
import in.evergreen.socialapp.Utility.TLHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileFriendsFragment extends Fragment {

    private ArrayList<ProfileFriendsModel> items = new ArrayList<ProfileFriendsModel>();
    private Context context;
    private TLHelper hlp;
    private UserInterface userInterface;
    private ProfileFriendsAdapter adapter;
    private int user_id;
    public int page=1;
    public boolean pageend =false;
    boolean is_loading =false;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    EndlessRecyclerOnScrollListener esl;


    public ProfileFriendsFragment() {
    }

    @SuppressLint("ValidFragment")
    public ProfileFriendsFragment(ArrayList<ProfileFriendsModel> items, Context context, int user_id) {
        this.items = items;
        this.context = context;
        this.user_id = user_id;
        hlp = new TLHelper(context);
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile_friends, container,false);



        esl = new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                Log.e("SCROLL ", pageend+"==="+is_loading);
                if(pageend==false && is_loading == false){
                    page++;
                    actionFriend(user_id);
                }
            }
        };

        userInterface = ApiClient.getClient().create(UserInterface.class);

        recyclerView = view.findViewById(R.id.profileFriendsRecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ProfileFriendsAdapter(items, context);
        recyclerView.setAdapter(adapter);
        actionFriend(user_id);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter = new ProfileFriendsAdapter(items, context);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(esl);
        return  view;
    }


    public void actionFriend( int user_id) {
        if (page == 1) {
            Call<JsonElement> call = userInterface.getUserFriend(MyApplication.token, user_id, page);

            call.enqueue(new Callback<JsonElement>() {

                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hlp.hideLoader();
                    JsonElement jsonElement = response.body();
                    String body = jsonElement.toString();

                    try {

                        JSONObject res = new JSONObject(body);

                        if (res.getString("status").equalsIgnoreCase("success")) {
                            JSONArray data = res.getJSONArray("data");

                            Log.e("Response_data", String.valueOf(data));
                            createListing(data);

                            Log.e("listenDOne", "next is adapter");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hlp.hideLoader();
                    hlp.noConnection();
                }
            });
        }
    }

    public void createListing(JSONArray data) throws JSONException{
//        items.clear();
        if(page==1){
            items.clear();
        }
        for (int i=0; i<data.length(); i++) {
            try {

                items.add(new ProfileFriendsModel(data.getJSONObject(i)));
                Log.e("modelItem", String.valueOf(items));

            } catch (JSONException e) {
                e.printStackTrace();

            }
        }
        if(data.length()==0){
            pageend = true;
        }
        adapter.notifyDataSetChanged();
//        if(items.size()==0){
//            noRecord.setVisibility(View.VISIBLE);
//        }
//        else {
//            noRecord.setVisibility(View.INVISIBLE);
//        }
    }


}

