package in.evergreen.socialapp.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Apis.ApiClient;
import Apis.PostInterface;
import Apis.UserInterface;
import Models.FeedsModel;
import Models.MediaModel;
import Models.ProfileFriendsModel;
import cn.jzvd.JZMediaManager;
import cn.jzvd.JZUtils;
import cn.jzvd.JZVideoPlayer;
import cn.jzvd.JZVideoPlayerManager;
import in.evergreen.socialapp.Adapters.FeedsHomeAdapter;
import in.evergreen.socialapp.Adapters.ProfileFriendsAdapter;
import in.evergreen.socialapp.MyApplication;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.EndlessRecyclerOnScrollListener;
import in.evergreen.socialapp.Utility.TLHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class ProfilePostFragment extends Fragment {

    TLHelper hlp;
    View v;
    int user_id;
    Context context;


    private FeedsHomeAdapter adapter;
    public static ArrayList<FeedsModel> itemiList=new ArrayList<FeedsModel>();
    private RecyclerView mRecyclerView;
    View  reloader;
    PostInterface postInterface;
    SwipeRefreshLayout swipeRefreshLayout;
    public int page=1;
    public boolean pageend =false;
    boolean is_loading =false;
    public TextView noRecord;
    EndlessRecyclerOnScrollListener esl;

    public static ProfilePostFragment fragment;

    public static ProfilePostFragment getInstance(Context context, Activity activity, int user_id){

        fragment = new ProfilePostFragment(context,user_id);
        return fragment;
    }


    @SuppressLint("ValidFragment")
    public ProfilePostFragment(Context context, int user_id) {
        this.context = context;
        this.user_id = user_id;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile_post, container,false);
        v=view;
        hlp = new TLHelper(context);

        noRecord = (TextView) v.findViewById(R.id.txtNoRecord);
        reloader = v.findViewById(R.id.buttonReload);
        postInterface = ApiClient.getClient().create(PostInterface.class);
        reloader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchRecords();
            }
        });
        esl = new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                Log.e("SCROLL ", pageend+"==="+is_loading);
                if(pageend==false && is_loading == false){
                    page++;
                    fetchRecords();
                }
            }
        };
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.dark_blue, R.color.dark_grey, R.color.light_grey);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                itemiList.clear();
                page=1;
                pageend = false;
                fetchRecords();
                esl.reset();
            }
        });
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        LinearLayoutManager lm = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new FeedsHomeAdapter(context, itemiList);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(View view) {

            }

            @Override
            public void onChildViewDetachedFromWindow(View view) {
                JZVideoPlayer jzvd = view.findViewById(R.id.videoplayer);
                if (jzvd != null && JZUtils.dataSourceObjectsContainsUri(jzvd.dataSourceObjects, JZMediaManager.getCurrentDataSource())) {
                    JZVideoPlayer currentJzvd = JZVideoPlayerManager.getCurrentJzvd();
                    if (currentJzvd != null && currentJzvd.currentScreen != JZVideoPlayer.SCREEN_WINDOW_FULLSCREEN) {
                        JZVideoPlayer.releaseAllVideos();
                    }
                }
            }
        });

        mRecyclerView.addOnScrollListener(esl);


        fetchRecords();
        return  view;
    }







    public void fetchRecords(){
        noRecord.setVisibility(View.GONE);
        reloader.setVisibility(View.GONE);
        if(page==1){
            swipeRefreshLayout.setRefreshing(true);
        }

        is_loading = true;
        Call<JsonElement> call = postInterface.getUserFeed(MyApplication.token,user_id,page,20);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                hlp.hideLoader();
                is_loading = false;
                swipeRefreshLayout.setRefreshing(false);
                if(response.isSuccessful()){
                    JsonElement jsonElement = response.body();
                    String body = jsonElement.toString();
                    Log.e("RESPONSE", body);
                    try {
                        JSONObject res = new JSONObject(body);
                        JSONArray data = res.getJSONArray("data");
                        createListing(data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    hlp.showError(response.code());
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.hideLoader();
                is_loading=false;
                swipeRefreshLayout.setRefreshing(false);
                if(page==1){
                    reloader.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    public void createListing(JSONArray data) throws JSONException{
        if(page==1){
            itemiList.clear();
        }
        int last_size =itemiList.size();



        for (int i=0; i<data.length(); i++) {
            JSONObject item;
            try {
                item = data.getJSONObject(i);
                itemiList.add(new FeedsModel(item));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(data.length()==0){
            pageend = true;
        }
        adapter.updateItems(itemiList);
        if(itemiList.size()==0){
            noRecord.setVisibility(View.VISIBLE);
        }
    }


    public static ArrayList<MediaModel> getPostMedia(int post_index){
        if(itemiList.size()>post_index){
            return itemiList.get(post_index).media;
        }else {
            return new ArrayList<MediaModel>();
        }
    }

}
