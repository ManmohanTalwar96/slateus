package in.evergreen.socialapp.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import Apis.ApiClient;
import Apis.UserInterface;
import in.evergreen.socialapp.MyApplication;
import in.evergreen.socialapp.R;
import in.evergreen.socialapp.Utility.TLHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class ProfileProfileFragment extends Fragment {

    private UserInterface userInterface;
    private TLHelper hlp;
    private Context context;
    private Activity activity;
    private  int user_id;
    public View view;
    JSONObject data;

    public static ProfileProfileFragment fragment;

    public static ProfileProfileFragment getInstance(Context context, Activity activity, int user_id){

            fragment = new ProfileProfileFragment(context,user_id);
            return fragment;
    }


    @SuppressLint("ValidFragment")
    public ProfileProfileFragment(Context context, int user_id) {
        this.context = context;
        this.user_id = user_id;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile_profile, container, false);

        context = container.getContext();

        hlp = new TLHelper(context);

        userInterface = ApiClient.getClient().create(UserInterface.class);
        this.view = view;

        if(data!=null)
            setProfileFields(data);
        return view;
    }

    public void setProfileFields(JSONObject data){

        if(view==null){
            this.data = data;
            return;
        }

        try {

            ((TextView) view.findViewById(R.id.user_aboutme)).setText(data.getString("about"));
            ((TextView) view.findViewById(R.id.user_email)).setText(data.getString("email"));
            ((TextView) view.findViewById(R.id.user_gender)).setText(data.getString("gender"));
            ((TextView) view.findViewById(R.id.user_state)).setText(data.getString("state_id"));
            ((TextView) view.findViewById(R.id.user_city)).setText(data.getString("city_id"));
            ((TextView) view.findViewById(R.id.user_relationship_status)).setText(data.getString("relationship"));
            ((TextView) view.findViewById(R.id.user_dob)).setText(data.getString("dob"));
            ((TextView) view.findViewById(R.id.user_interest)).setText(data.getString("interest"));
            ((TextView) view.findViewById(R.id.user_profession)).setText(data.getString("profession"));
            ((TextView) view.findViewById(R.id.user_company)).setText(data.getString("company"));
            ((TextView) view.findViewById(R.id.user_hobbies)).setText(data.getString("hobbies"));
            ((TextView) view.findViewById(R.id.user_website)).setText(data.getString("website"));
            ((TextView) view.findViewById(R.id.user_nickname)).setText(data.getString("nickname"));
            ((TextView) view.findViewById(R.id.user_goal)).setText(data.getString("goal"));
            ((TextView) view.findViewById(R.id.user_technology)).setText(data.getString("technology"));
            ((TextView) view.findViewById(R.id.user_college)).setText(data.getString("college"));
            ((TextView) view.findViewById(R.id.user_life)).setText(data.getString("life"));
            ((TextView) view.findViewById(R.id.user_profession)).setText(data.getString("profession"));
            ((TextView) view.findViewById(R.id.user_believe)).setText(data.getString("believe"));
            ((TextView) view.findViewById(R.id.user_book)).setText(data.getString("book"));
            ((TextView) view.findViewById(R.id.user_tagline)).setText(data.getString("tagline"));
            ((TextView) view.findViewById(R.id.user_idol)).setText(data.getString("idol"));
            ((TextView) view.findViewById(R.id.user_actor)).setText(data.getString("actor"));
            ((TextView) view.findViewById(R.id.user_movie)).setText(data.getString("movie"));
            ((TextView) view.findViewById(R.id.user_music)).setText(data.getString("music"));
            ((TextView) view.findViewById(R.id.user_place)).setText(data.getString("place"));

        } catch (JSONException e) {
            e.printStackTrace();
        }



    }


}


