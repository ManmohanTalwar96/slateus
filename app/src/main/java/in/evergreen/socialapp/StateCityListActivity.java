package in.evergreen.socialapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Apis.ApiClient;
import Apis.UserInterface;
import Models.FriendsSearchModel;
import Models.StateModel;
import in.evergreen.socialapp.Adapters.StateListAdapter;
import in.evergreen.socialapp.Utility.TLHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StateCityListActivity extends AppCompatActivity {

    private Context context;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<StateModel> items = new ArrayList<>();
    private StateListAdapter adapter;
    private UserInterface userInterface;
    private TLHelper hlp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_city_list);

        hlp = new TLHelper(this);
        userInterface = ApiClient.getClient().create(UserInterface.class);

        recyclerView = (RecyclerView) findViewById(R.id.stateRecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new StateListAdapter(items, StateCityListActivity.this);
        recyclerView.setAdapter(adapter);

        stateList();
    }

    public void stateList(){

        Call<JsonElement> call = userInterface.stateList(MyApplication.token);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();

                try {
                    JSONObject res = new JSONObject(body);

                    if (res.getString("status").equalsIgnoreCase("success")){

                        JSONArray data = res.getJSONArray("data");

                        createListing(data);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                hlp.noConnection();
            }
        });

    }

    public void createListing(JSONArray data) throws JSONException{
        items.clear();
        for (int i=0; i<data.length(); i++) {
            try {
                items.add(new StateModel(data.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        adapter.notifyDataSetChanged();

    }

    public void viewclose(View view) {

        Intent close = new Intent(StateCityListActivity.this ,EditProfile.class);
        startActivity(close);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode ==11 && resultCode == RESULT_OK){

            int state_id = data.getIntExtra("state_id",0);
            int city_id = data.getIntExtra("city_id",0);
            Log.e("city-id",String.valueOf(city_id));
            Log.e("state-id",String.valueOf(state_id));
            Intent i = new Intent() ;
            i.putExtra("state_id", state_id);
            i.putExtra("city_id", city_id);
            setResult(Activity.RESULT_OK, i);
            finish();
        }
    }
}
