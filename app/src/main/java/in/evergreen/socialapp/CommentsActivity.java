package in.evergreen.socialapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Apis.ApiClient;
import Apis.PostInterface;
import Apis.UserInterface;
import Models.FeedCommentModel;
import Models.FeedsModel;
import Models.MediaModel;
import cn.jzvd.JZMediaManager;
import cn.jzvd.JZUtils;
import cn.jzvd.JZVideoPlayer;
import cn.jzvd.JZVideoPlayerManager;
import in.evergreen.socialapp.Adapters.CommentAdapter;
import in.evergreen.socialapp.Adapters.FeedsHomeAdapter;
import in.evergreen.socialapp.Auth.LoginActivity;
import in.evergreen.socialapp.Utility.EndlessRecyclerOnScrollListener;
import in.evergreen.socialapp.Utility.TLHelper;
import in.evergreen.socialapp.Utility.TLStorage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentsActivity extends AppCompatActivity  {

    UserInterface userService;
    JsonParser jsonParser;
    JsonObject jsonObject;
    TLStorage sto;
    TLHelper hlp;
    ImageView userimage;
    private RecyclerView mRecyclerView;
    View reloader;
    PostInterface postInterface;
    SwipeRefreshLayout swipeRefreshLayout;
    int page=1;
    boolean pageend =false;
    boolean is_loading =false;
    public TextView noRecord;
    EndlessRecyclerOnScrollListener esl;
    CommentAdapter adapter;
    ArrayList<FeedCommentModel> itemiList = new ArrayList<>();
    int post_id;


    String TAG = CommentsActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        getSupportActionBar().setElevation(0);
        sto = new TLStorage(this);
        hlp = new TLHelper(this);
        jsonParser = new JsonParser();
        jsonObject = new JsonObject();
        post_id = getIntent().getExtras().getInt("post_id");


        noRecord = (TextView) findViewById(R.id.txtNoRecord);
        reloader = findViewById(R.id.buttonReload);
        postInterface = ApiClient.getClient().create(PostInterface.class);
        reloader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchRecords();
            }
        });
        esl = new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                Log.e("SCROLL ", pageend+"==="+is_loading);
                if(pageend==false && is_loading == false){
                    page++;
                    fetchRecords();
                }
            }
        };
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.backgroundAuth, R.color.dark_grey, R.color.light_grey);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                itemiList.clear();
                page=1;
                pageend = false;
                fetchRecords();
                esl.reset();
            }
        });
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CommentAdapter(this, itemiList);
        mRecyclerView.setAdapter(adapter);

        mRecyclerView.addOnScrollListener(esl);








        fetchRecords();
        showSelfImage();
    }


    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void fetchRecords(){
        noRecord.setVisibility(View.GONE);
        reloader.setVisibility(View.GONE);
        if(page==1){
            swipeRefreshLayout.setRefreshing(true);
        }

        is_loading = true;
        Call<JsonElement> call = postInterface.getComments(MyApplication.token,post_id,page);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                is_loading = false;
                swipeRefreshLayout.setRefreshing(false);
                if(response.isSuccessful()){
                    JsonElement jsonElement = response.body();
                    String body = jsonElement.toString();
                    Log.e("RESPONSE", body);
                    try {
                        JSONObject res = new JSONObject(body);
                        JSONArray data = res.getJSONArray("data");
                        createListing(data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    hlp.showError(response.code());
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                is_loading=false;
                swipeRefreshLayout.setRefreshing(false);
                if(page==1){
                    reloader.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    public void createListing(JSONArray data) throws JSONException{
        for (int i=0; i<data.length(); i++) {
            JSONObject item;
            try {
                item = data.getJSONObject(i);
                if(page==1)
                    itemiList.add(new FeedCommentModel(item));
                else
                    itemiList.add(0,new FeedCommentModel(item));
                if(page!=1)
                    adapter.notifyItemInserted(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(data.length()==0){
            pageend = true;
        }
        if(page==1){
            adapter.notifyDataSetChanged();
            mRecyclerView.scrollToPosition(itemiList.size());
        }

        if(itemiList.size()==0){
            noRecord.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

    }


    public void postComment(View v){
        final EditText txtComment = (EditText) findViewById(R.id.txtPostComment);
        final String comment = txtComment.getText().toString().trim();
        if(comment.isEmpty())
            return;
        txtComment.setText("");
        JSONObject jo = new JSONObject();
        try {
            jo.put("comment", comment);
            jo.put("post_id", post_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = (JsonObject) jsonParser.parse(jo.toString());
        Call<JsonElement>  call = postInterface.createComment(MyApplication.token,jsonObject);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement>call, Response<JsonElement> response) {
                JsonElement jsonElement = response.body();
                String body = jsonElement.toString();
                Log.e("RESPONSE", body);
                try {
                    JSONObject res = new JSONObject(body);
                    if(res.getString("status").equalsIgnoreCase("success")){
                        JSONObject data = res.getJSONObject("data");
                        data.put("allow_delete","1");
                        itemiList.add(new FeedCommentModel(data));
                        adapter.notifyItemInserted(itemiList.size());
                        noRecord.setVisibility(View.GONE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mRecyclerView.scrollToPosition(mRecyclerView.getAdapter().getItemCount()-1);
                            }
                        },1);
                    }else{

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                txtComment.setText(comment);
                hlp.noConnection();
            }
        });
    }

    public void showSelfImage() {
        ImageView userimage = (ImageView) findViewById(R.id.userImageComment);
        TextView txtAvatarText = (TextView) findViewById(R.id.txtNameCharComment);
        if (MyApplication.image.isEmpty() == false) {
            userimage.setVisibility(View.VISIBLE);
            txtAvatarText.setVisibility(View.GONE);
            Glide.with(this).load(MyApplication.image).into(userimage);
        } else {
            userimage.setVisibility(View.GONE);
            txtAvatarText.setVisibility(View.VISIBLE);
            txtAvatarText.setText(hlp.getShortName(MyApplication.display_name));
        }
    }

}