package Apis;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface AuthInterface {

    @POST("auth/login")
    Call<JsonElement> login(@Body JsonObject jsonBody);

    @POST("auth/register")
    Call<JsonElement> register(@Body JsonObject jsonBody);

    @POST("auth/resetlink")
    Call<JsonElement> reset(@Body JsonObject jsonBody);

    @GET("auth/resendlink/{user_id}")
    Call<JsonElement> resendLink(@Path("user_id") int user_id);



 }
