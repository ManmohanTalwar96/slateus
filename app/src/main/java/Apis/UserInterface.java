package Apis;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Sheetal on 5/16/18.
 */

public interface UserInterface {

    @GET("auth/logout")
    Call<JsonElement> logout(@Header("Authorization") String authorization);

    @GET("user/search")
    Call<JsonElement> getUser(@Header("Authorization") String authorization, @Query("q") String q, @Query("page") int page);

    @GET("user/profile/{user_id}")
    Call<JsonElement> getUserProfile(@Header("Authorization") String authorization, @Path("user_id") int user_id) ;

    @GET("friend/{user_id}")
    Call<JsonElement> getUserFriend(@Header("Authorization") String authorization, @Path("user_id") int user_id, @Query("page") int page) ;

    @POST("user/update")
    Call<JsonElement> updateProfile(@Header("Authorization") String authorization, @Body JsonObject jsonBody);

    @GET("state/101")
    Call<JsonElement> stateList(@Header("Authorization") String authorization);

    @GET("profession")
    Call<JsonElement> profession(@Header("Authorization") String authorization);

    @GET("relationship")
    Call<JsonElement> relationship(@Header("Authorization") String authorization);

    @GET("city/{state_id}")
    Call<JsonElement> cityList(@Header("Authorization") String authorization, @Path("state_id") int state_id);

    @POST("user/changepassword")
    Call<JsonElement> changePassword(@Header("Authorization") String authorization, @Body JsonObject jsonBody);

    @POST("user/changemobile")
    Call<JsonElement> changeMobile(@Header("Authorization") String authorization, @Body JsonObject jsonBody);

    @Multipart
    @POST("user/pic")
    Call<JsonElement> updateProfileImage(@Header("Authorization") String authorization, @Part MultipartBody.Part image, @Part("name") RequestBody name);


    @GET("friend/request/{user_id}")
    Call<JsonElement> sendFriendRequest(@Header("Authorization") String authorization, @Path("user_id") int user_id );

    @GET("friend/accept/{user_id}")
    Call<JsonElement> acceptFriendRequest(@Header("Authorization") String authorization, @Path("user_id") int user_id );

    @GET("friend/deny/{user_id}")
    Call<JsonElement> denyFriendRequest(@Header("Authorization") String authorization, @Path("user_id") int user_id );

    @GET("friend/cancel/{user_id}")
    Call<JsonElement> cancelFriendRequest(@Header("Authorization") String authorization, @Path("user_id") int user_id );

    @GET("friend/block/{user_id}")
    Call<JsonElement> blockFriend(@Header("Authorization") String authorization, @Path("user_id") int user_id );

    @GET("friend/unfriend/{user_id}")
    Call<JsonElement> unfriendFriend(@Header("Authorization") String authorization, @Path("user_id") int user_id );

    @GET("user/getallfriendlist")
    Call<JsonElement> allFriendList(@Header("Authorization") String authorization);

}
