package Apis;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Sheetal on 5/16/18.
 */

public interface PostInterface {

    @Multipart
    @POST("upload/file")
    Call<JsonElement> uploadFile(@Header("Authorization") String authorization, @Part MultipartBody.Part file);

    @Multipart
    @POST("upload/image/post/1000")
    Call<JsonElement> uploadImage(@Header("Authorization") String authorization, @Part MultipartBody.Part file);

    @POST("post/create")
    Call<JsonElement> createPost(@Header("Authorization") String authorization, @Body JsonObject jsonBody);

    @GET("post/feeds")
    Call<JsonElement> getFeed(@Header("Authorization") String authorization, @Query("page") int page, @Query("perpage") int perpage);

    @GET("unlike/{id}")
    Call<JsonElement> setUnlike(@Header("Authorization") String authorization, @Path("id") int id , @Query("type") String type);

    @GET("like/{id}")
    Call<JsonElement> setLike(@Header("Authorization") String authorization, @Path("id") int id ,@Query("type") String type);

    @POST("comment/add")
    Call<JsonElement> createComment(@Header("Authorization") String authorization, @Body JsonObject jsonBody);

    @GET("comment/{post_id}")
    Call<JsonElement> getComments(@Header("Authorization") String authorization, @Path("post_id") int post_id, @Query("page") int page);

    @GET("likes/{post_id}")
    Call<JsonElement> getLikes(@Header("Authorization") String authorization, @Path("post_id") int post_id, @Query("page") int page);

    @GET("post/share/{post_id}")
    Call<JsonElement> sharePost(@Header("Authorization") String authorization, @Path("post_id") int post_id);

    @DELETE("post/delete/{post_id}")
    Call<JsonElement> deletePost(@Header("Authorization") String authorization, @Path("post_id") int post_id);

    @DELETE("comment/delete/{comment_id}")
    Call<JsonElement> deleteComment(@Header("Authorization") String authorization, @Path("comment_id") int comment_id);

    @GET("post/feeds/{user_id}")
    Call<JsonElement> getUserFeed(@Header("Authorization") String authorization, @Path("user_id") int user_id , @Query("page") int page, @Query("perpage") int perpage);

    @GET("post/{post_id}")
    Call<JsonElement> getSinglePost(@Header("Authorization") String authorization, @Path("post_id") int post_id);

    @POST("comment/update/{comment_id}")
    Call<JsonElement> updateComment(@Header("Authorization") String authorization, @Path("comment_id") int comment_id, @Body JsonObject jsonBody);

}
