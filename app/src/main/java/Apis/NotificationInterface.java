package Apis;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Sheetal on 5/16/18.
 */

public interface NotificationInterface {

    @GET("notification")
    Call<JsonElement> getNotification(@Header("Authorization") String authorization, @Query("page") int page, @Query("perpage") int perpage);



}
